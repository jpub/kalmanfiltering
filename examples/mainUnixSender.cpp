#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>

#include "../inc/__GenStructs.h"

#define LOCAL_SOCK_PATH "/tmp/__uds_tst"
#define REMOTE_SOCK_PATH "/tmp/__uds_trk"

#pragma pack(push,1)
struct TEST
{
    MSG_HEADER msgHdr;
    AP_UPDATE apUpdate;
};
#pragma pack(pop)
  
void* recvdata(void* usocket)
{
    printf( "receving thread started\n" );
    int s = *(int*)usocket;
    int counter = 0;
    char str[100];
    while ( true )
    {
        int n = recv( s, str, 100, 0 );
        ++counter;
        printf( "%d - recv %d bytes\n", counter, n );
    }
}

int main( int argc, char** argv )
{
    printf( "Local socket path:%s\n", LOCAL_SOCK_PATH );
    printf( "Remote socket path:%s\n", REMOTE_SOCK_PATH );

    if ( argc != 3 )
    {
        printf( "usage: ./mainUnixSender <MESSAGE> <DELAY>\n" );
        return -1;
    }
    
    int s, t, len;
    int testS;
    struct sockaddr_un local, remote;
    char str[100];
    int delay = atoi( argv[2] );

    if ((s = socket(AF_UNIX, SOCK_DGRAM, 0)) == -1) {
        perror("socket");
        exit(1);
    }

    if ((testS = socket(AF_UNIX, SOCK_DGRAM, 0)) == -1) {
        perror("socket");
        exit(1);
    }
    
    local.sun_family = AF_UNIX;
    strcpy(local.sun_path, LOCAL_SOCK_PATH);

    remote.sun_family = AF_UNIX;
    strcpy(remote.sun_path, REMOTE_SOCK_PATH);
    
    len = strlen(local.sun_path) + sizeof(local.sun_family);
    unlink(local.sun_path);

    if (bind(s, (struct sockaddr *)&local, len) == -1) {
        perror("bind");
        exit(1);
    }


    pthread_t rcvthread;
    pthread_create(&rcvthread, NULL, recvdata, &s);
    pthread_detach(rcvthread);
        
    int counter = 0;
    TEST test;
    test.msgHdr.messageCode = 1003;
    test.msgHdr.messageLength = sizeof(TEST);
  
    while ( true )
    {
        ++counter;
        //int n = sendto( testS, argv[1], strlen(argv[1]), 0, (struct sockaddr*)&remote, sizeof(sockaddr_un) );
        int n = sendto( testS, &test, sizeof(TEST), 0, (struct sockaddr*)&remote, sizeof(sockaddr_un) );
        printf( "%d - send %d bytes\n", counter, n );
        usleep( delay );
    }
    
    close(s);

    return 0;
}



/*
int main(void)
{
    int s, t, len;
    struct sockaddr_un remote;
    char str[100];

    if ((s = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
        perror("socket");
        exit(1);
    }

    printf("Trying to connect...\n");

    remote.sun_family = AF_UNIX;
    strcpy(remote.sun_path, SOCK_PATH);
    len = strlen(remote.sun_path) + sizeof(remote.sun_family);
    if (connect(s, (struct sockaddr *)&remote, len) == -1) {
        perror("connect");
        exit(1);
    }

    printf("Connected.\n");

    while(printf("> "), fgets(str, 100, stdin), !feof(stdin)) {
        if (send(s, str, strlen(str), 0) == -1) {
            perror("send");
            exit(1);
        }

        if ((t=recv(s, str, 100, 0)) > 0) {
            str[t] = '\0';
            printf("echo> %s", str);
        } else {
            if (t < 0) perror("recv");
            else printf("Server closed connection\n");
            exit(1);
        }
    }

    close(s);

    return 0;
}
*/
