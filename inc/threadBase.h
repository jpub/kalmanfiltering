#ifndef __THREAD_BASE_H
#define __THREAD_BASE_H

#include <pthread.h>
#include "msgBase.h"
#include "msgQueue.h"



class ThreadBase
{
public:
    ThreadBase( MsgQueue<QItem>* msgQueue );
    virtual ~ThreadBase();
    
    int start();
    int join();
    int detach();
    pthread_t self();
    
    virtual void* run( void* arg ) = 0;

protected:
    MsgQueue<QItem>* mQueue;
    
private:
    pthread_t mThread;
    int       mRunning;
    int       mDetached;
};

#endif
