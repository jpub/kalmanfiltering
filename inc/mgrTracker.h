#ifndef __MGR_TRACKER_H
#define __MGR_TRACKER_H

#include "trackerAngleAgreement.h"
#include "factoryMsg.h"
#include "msgTrackedLocation.h"
#include "msgDeviceLocation.h"

using std::string;

class MgrTracker
{
public:
	static MgrTracker& getInstance();
	void init(
		bool activateAngleTracker,
		bool activateDistanceTracker,
		double maxAngleAgreement,
		double humanWalkSpeed,
		int trackAgeTime );

	bool update( MsgDeviceLocation& msgDeviceLocation );
	bool maintainance();

	
private:
	MgrTracker();
	~MgrTracker();

private:
	static MgrTracker* msInstance;


private:
	TrackerAngleAgreement* mTracker;
	FactoryMsg mFactoryMsg;

	bool mActivateAngleTracker;
	bool mActivateDistanceTracker;
	unsigned int mTrackAgeTime;
};

#endif