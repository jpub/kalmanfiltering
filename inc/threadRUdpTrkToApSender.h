#ifndef __THREAD_RUDP_TRK_TO_AP_SEDNER_H
#define __THREAD_RUDP_TRK_TO_AP_SEDNER_H

#include <netdb.h>
#include "factoryMsg.h"
#include "threadRUdpBaseSender.h"
#include "udt.h"


class ThreadRUdpTrkToApSender : public ThreadRUdpBaseSender
{
public:
    ThreadRUdpTrkToApSender( MsgQueue<QItem>* outgoingQueue,
    						 int destinationId,
    						 const char* destIp,
    						 int destPort,
    						 const char* localBindIp,
    						 int localBindPort,
    						 int ttl );

    virtual void handleDisconnection();
    virtual void handleSuccessfulSending( MsgBase& msg );
};

#endif
