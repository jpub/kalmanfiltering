#include "byteBuffer.h"
#include "msgBase.h"
#include "msgLinkRttRequest.h"
#include "logger.h"
#include <stdio.h>


QItem MsgLinkRttRequest::clone()
{
    MsgLinkRttRequest* msg = new MsgLinkRttRequest();
    if ( this->mRawBuffer )
        msg->mRawBuffer = new ByteBuffer( *this->mRawBuffer );
    msg->mHeader = this->mHeader;
    msg->mData = this->mData;

    return QItem( msg );
}

MsgLinkRttRequest::MsgLinkRttRequest()
{
    memset( &mData, 0, sizeof(mData) );
    mHeader.messageCode = ID_LINK_RTT_REQUEST;
    mHeader.messageLength = getSize();
}

MsgLinkRttRequest::MsgLinkRttRequest( ByteBuffer* inBuffer )
    :MsgBase( inBuffer )
{
    int bytePosition = 0;
    unpack( bytePosition );
}

MsgLinkRttRequest::MsgLinkRttRequest( const MsgLinkRttRequest& rhs )
    :MsgBase( rhs )
{
    mData = rhs.mData;
}

MsgLinkRttRequest& MsgLinkRttRequest::operator=( const MsgLinkRttRequest& rhs )
{
    MsgBase::operator = (rhs);
    mData = rhs.mData;
    return *this;
}

int MsgLinkRttRequest::getSize()
{
    int totalSize = 0;
    totalSize += MsgBase::getSize();
    totalSize += sizeof( mData );
    return totalSize;
}

ByteBuffer* MsgLinkRttRequest::pack( int& bytePosition )
{
    MsgBase::pack( bytePosition );
    mRawBuffer->appendByteArray( (char*)&mData, sizeof(mData) );
    return mRawBuffer;
}

void MsgLinkRttRequest::unpack( int& bytePosition )
{
    MsgBase::unpack( bytePosition );
    mRawBuffer->popByteArray( (char*)&mData, sizeof(mData) );
}

int MsgLinkRttRequest::print()
{
    int charWritten = MsgBase::print();

    charWritten = snprintf( mPrintBuffer + charWritten, PRINT_BUFFER_SIZE,
        "Key:%d",
        mData.key );

    LOG(DEBUG) << mPrintBuffer;
    return charWritten;
}