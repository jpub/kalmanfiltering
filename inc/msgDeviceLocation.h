#ifndef __MSG_DEVICE_LOCATION_H
#define __MSG_DEVICE_LOCATION_H

#include "msgBase.h"

class ByteBuffer;

class MsgDeviceLocation : public MsgBase
{
public:
    ENABLE_EVENT_DISPATCH();

public:
    virtual QItem clone();

public:
    MsgDeviceLocation();
    MsgDeviceLocation( ByteBuffer* inBuffer );
    MsgDeviceLocation( const MsgDeviceLocation& rhs );
    MsgDeviceLocation& operator=( const MsgDeviceLocation& rhs );

    virtual int getSize();
    virtual ByteBuffer* pack( int& bytePosition );
    virtual void unpack( int& bytePosition );
    virtual int print();

    DEVICE_LOCATION* data()
    {
        return &mData;
    }
    
private:
    DEVICE_LOCATION mData;
};

#endif
