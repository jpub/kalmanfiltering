#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>

#define LOCAL_SOCK_PATH "/tmp/__uds_trk"
#define REMOTE_SOCK_PATH "/tmp/__uds_tst"


int main(void)
{
    printf( "Local socket path:%s\n", LOCAL_SOCK_PATH );
    printf( "Remote socket path:%s\n", REMOTE_SOCK_PATH );
    
    int s, s2, len;
    int testS;
    socklen_t t;
    struct sockaddr_un local, remote;
    char str[100];

    if ((s = socket(AF_UNIX, SOCK_DGRAM, 0)) == -1) {
        perror("socket");
        exit(1);
    }

    if ((testS = socket(AF_UNIX, SOCK_DGRAM, 0)) == -1) {
        perror("socket");
        exit(1);
    }
    
    local.sun_family = AF_UNIX;
    strcpy(local.sun_path, LOCAL_SOCK_PATH);

    remote.sun_family = AF_UNIX;
    strcpy(remote.sun_path, REMOTE_SOCK_PATH);

    len = strlen(local.sun_path) + sizeof(local.sun_family);
    unlink(local.sun_path);

    if (bind(s, (struct sockaddr *)&local, len) == -1) {
        perror("bind");
        exit(1);
    }

    int counter = 0;
    while ( true )
    {
        int n = recv( s, str, 100, 0 );
        ++counter;
        printf( "%d - recv %d bytes\n", counter, n );
        for ( int i=0; i<n; ++i )
            printf( "%d ", str[i] );
        printf( "\n" );
        
        if ( counter % 5 == 0 )
        {
            char fbStr[] = "this is my feedback";
            int fblen = sendto( testS, fbStr, strlen(fbStr), 0, (struct sockaddr*)&remote, sizeof(sockaddr_un) );
            printf( "fb to sender %d bytes\n", fblen );
        }
    }

    close( s );
    unlink(local.sun_path);
    return 0;
}

/*

int main(void)
{
    int s, s2, len;
    socklen_t t;
    struct sockaddr_un local, remote;
    char str[100];

    if ((s = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
        perror("socket");
        exit(1);
    }

    local.sun_family = AF_UNIX;
    strcpy(local.sun_path, SOCK_PATH);
    unlink(local.sun_path);
    len = strlen(local.sun_path) + sizeof(local.sun_family);
    if (bind(s, (struct sockaddr *)&local, len) == -1) {
        perror("bind");
        exit(1);
    }

    if (listen(s, 5) == -1) {
        perror("listen");
        exit(1);
    }

    for(;;) {
        int done, n;
        printf("Waiting for a connection...\n");
        t = sizeof(remote);
        if ((s2 = accept(s, (struct sockaddr *)&remote, &t)) == -1) {
            perror("accept");
            exit(1);
        }

        printf("Connected.\n");

        done = 0;
        do {
            n = recv(s2, str, 100, 0);
            if (n <= 0) {
                if (n < 0) perror("recv");
                done = 1;
            }

            if (!done) 
                if (send(s2, str, n, 0) < 0) {
                    perror("send");
                    done = 1;
                }
        } while (!done);

        close(s2);
    }

    return 0;
}
*/
