#include "byteBuffer.h"
#include "msgBase.h"
#include "msgSensorUpdate.h"
#include <stdio.h>
#include <stdlib.h>


QItem MsgSensorUpdate::clone()
{
    MsgSensorUpdate* msg = new MsgSensorUpdate();
    if ( this->mRawBuffer )
        msg->mRawBuffer = new ByteBuffer( *this->mRawBuffer );
    msg->mHeader = this->mHeader;
    msg->mData = this->mData;

    return QItem( msg );
}

MsgSensorUpdate::MsgSensorUpdate()
{
    memset( &mData, 0, sizeof(mData) );
    mHeader.messageCode = ID_SENSOR_UPDATE;
    mHeader.messageLength = getSize();
}

MsgSensorUpdate::MsgSensorUpdate( ByteBuffer* inBuffer )
    :MsgBase( inBuffer )
{
    int bytePosition = 0;
    unpack( bytePosition );
}

MsgSensorUpdate::MsgSensorUpdate( const MsgSensorUpdate& rhs )
    :MsgBase( rhs )
{
    mData = rhs.mData;
}

MsgSensorUpdate& MsgSensorUpdate::operator=( const MsgSensorUpdate& rhs )
{
    MsgBase::operator = (rhs);
    mData = rhs.mData;
    return *this;
}

int MsgSensorUpdate::getSize()
{
    int totalSize = 0;
    totalSize += MsgBase::getSize();
    totalSize += sizeof( mData );
    return totalSize;
}

ByteBuffer* MsgSensorUpdate::pack( int& bytePosition )
{
    MsgBase::pack( bytePosition );
    mRawBuffer->appendByteArray( (char*)&mData, sizeof(mData) );
    return mRawBuffer;
}

void MsgSensorUpdate::unpack( int& bytePosition )
{
    MsgBase::unpack( bytePosition );
    mRawBuffer->popByteArray( (char*)&mData, sizeof(mData) );
}

int MsgSensorUpdate::print()
{
    int charWritten = MsgBase::print();
    charWritten = snprintf( mPrintBuffer + charWritten, PRINT_BUFFER_SIZE,
        "TgtMac:%02x%02x%02x%02x%02x%02x TgtRssi:%d",
        mData.tgtMac[0],
        mData.tgtMac[1],
        mData.tgtMac[2],
        mData.tgtMac[3],
        mData.tgtMac[4],
        mData.tgtMac[5],
        mData.tgtRssi );

    LOG(DEBUG) << mPrintBuffer;
    return charWritten;
}
