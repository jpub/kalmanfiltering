#include "logger.h"
#include "threadBaseReceiver.h"
#include "byteBuffer.h"

#include "__DefineVectorSize.h"
#include "__GenStructs.h"

ThreadBaseReceiver::ThreadBaseReceiver( MsgQueue<QItem>* outgoingQueue )
    : ThreadBase( outgoingQueue ),
      mRingBuffer(RING_BUFFER_SIZE)
{
    mPeekData = new char[PEEK_BUFFER_SIZE];
}

void ThreadBaseReceiver::processRingBuffer()
{
    if ( !mRingBuffer.peek(mPeekData, PEEK_BUFFER_SIZE) )
        return;

    MSG_HEADER* hdrPtr = 0;
    hdrPtr = (MSG_HEADER*)mPeekData;

    static int counter = 0;
    // clear the buffer as much as possible
    while ( mRingBuffer.size() >= hdrPtr->messageLength )
    {
        ++counter;
        //LOG(DEBUG) << "UDP received " << counter << ":" << hdrPtr->messageLength;
        
        ByteBuffer* bb = new ByteBuffer( hdrPtr->messageLength );
        char* bbPtr = bb->getDataPtr();
        mRingBuffer.read( bbPtr, hdrPtr->messageLength );

        QItem qitem = mFactoryMsg.get( hdrPtr->messageCode, bb );
        mQueue->add( qitem );
    }
}
