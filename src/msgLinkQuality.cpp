#include "byteBuffer.h"
#include "msgBase.h"
#include "msgLinkQuality.h"
#include <stdio.h>


QItem MsgLinkQuality::clone()
{
    MsgLinkQuality* msg = new MsgLinkQuality();
    if ( this->mRawBuffer )
        msg->mRawBuffer = new ByteBuffer( *this->mRawBuffer );
    msg->mHeader = this->mHeader;
    msg->mData = this->mData;

    return QItem( msg );
}

MsgLinkQuality::MsgLinkQuality()
{
    memset( &mData, 0, sizeof(mData) );
    mHeader.messageCode = ID_LINK_QUALITY;
    mHeader.messageLength = getSize();
}

MsgLinkQuality::MsgLinkQuality( ByteBuffer* inBuffer )
    :MsgBase( inBuffer )
{
    int bytePosition = 0;
    unpack( bytePosition );
}

MsgLinkQuality::MsgLinkQuality( const MsgLinkQuality& rhs )
    :MsgBase( rhs )
{
    mData = rhs.mData;
}

MsgLinkQuality& MsgLinkQuality::operator=( const MsgLinkQuality& rhs )
{
    MsgBase::operator = (rhs);
    mData = rhs.mData;
    return *this;
}

int MsgLinkQuality::getSize()
{
    int totalSize = 0;
    totalSize += MsgBase::getSize();
    totalSize += sizeof( mData );
    return totalSize;
}

ByteBuffer* MsgLinkQuality::pack( int& bytePosition )
{
    MsgBase::pack( bytePosition );
    mRawBuffer->appendByteArray( (char*)&mData, sizeof(mData) );
    return mRawBuffer;
}

void MsgLinkQuality::unpack( int& bytePosition )
{
    MsgBase::unpack( bytePosition );
    mRawBuffer->popByteArray( (char*)&mData, sizeof(mData) );
}

int MsgLinkQuality::print()
{
    int charWritten = MsgBase::print();

    charWritten = snprintf( mPrintBuffer + charWritten, PRINT_BUFFER_SIZE,
        "ClientId:%d RecvPerctage:%d TotalRecv:%d TotalSend:%d",
        mData.clientId,
        mData.receivedPercentage,
        mData.totalReceived,
        mData.totalSend );

    LOG(DEBUG) << mPrintBuffer;
    return charWritten;
}
