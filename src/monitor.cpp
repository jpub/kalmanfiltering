#include <stdio.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pcap.h>
#include <time.h>
#include "__DefineVectorSize.h"
#include "__EventIds.h"
#include "__GenStructs.h"
#include "logger.h"
#include "threadMasterReceiver.h"
#include "threadUdpReceiver.h"
#include "threadUdpSender.h"
#include "threadUnixDsSender.h"
#include "threadPcap.h"
#include "threadRUdpServer.h"
#include "threadRUdpReceiver.h"
#include "eventHandlerMonitor.h"
#include "mgrConfigStatic.h"
#include "mgrConfigDynamic.h"
#include "mgrReporter.h"
#include "factoryMsg.h"
#include "msgPeriodicTimer.h"
#include "tinyxml2.h"

#include "versionInfo.h"

INITIALIZE_EASYLOGGINGPP

// so that all incoming Mgr processing is single threaded
void maintenance( MsgQueue<QItem>& msgQueue )
{
    FactoryMsg factoryMsg;
    unsigned int counter = 0;
    MsgPeriodicTimer* msg = 0;
    int timerPeriod = MgrConfigStatic::getInstance().getMaintenanceTimer();
    
    while ( true )
    {
        ++counter;

        QItem qitem = factoryMsg.get( ID_PERIODIC_TIMER );
        msg = (MsgPeriodicTimer*)(qitem.getDataPointer());
        msg->data()->timerCounter = counter;
        msgQueue.add( qitem );

        usleep( timerPeriod );
    }
}

int main( int argc, char** argv )
{
    el::Configurations conf( "logMonitor.conf" );    
    el::Loggers::reconfigureAllLoggers( conf );

    if ( argc != 4 )
    {
        LOG(INFO) << "usage: sudo ./sniffer <SRC_AID> <SRC_ADDRESS> <BC_ADDRESS>";
        return -1;
    }

    LOG(DEBUG) << "Command line arg: SRC_AID:" << argv[1];
    LOG(DEBUG) << "Command line arg: SRC_MAC:" << argv[2];
    LOG(DEBUG) << "Command line arg: BC_ADDRESS:" << argv[3];

    MgrConfigStatic::getInstance().init( "configStatic.xml" );
    MgrConfigStatic::getInstance().setSrcAid( atoi(argv[1]) );
    MgrConfigStatic::getInstance().setSrcAddress( argv[2] );
    MgrConfigStatic::getInstance().setBcAddress( argv[3] );

    MgrConfigDynamic::getInstance().init( "configDynamic.xml" );

    int broadcastPort = MgrConfigStatic::getInstance().getBcPort();
    int monitorPort = MgrConfigStatic::getInstance().getMonitorPort();
    string udsExfDbg = MgrConfigStatic::getInstance().getUdsExfDbg();
    const char* broadcastIp = argv[3];


    MsgQueue<QItem> incomingQueue;
    MsgQueue<QItem> exfDbgOutgoingQueue;  
    MsgQueue<QItem> apBroadcastQueue;

    EventHandlerMonitor eventHandlerMonitor;

    MgrReporter::getInstance().init( &apBroadcastQueue, NULL, &exfDbgOutgoingQueue, &incomingQueue );

    ThreadMasterReceiver* masterReceiverThread = new ThreadMasterReceiver(
        &incomingQueue,
        &eventHandlerMonitor );


    ThreadUdpSender* apBroadcaster = new ThreadUdpSender(
        &apBroadcastQueue,
        SYSTEM_BROADCAST,
        broadcastIp,
        broadcastPort,
        INADDR_ANY,
        0 );

    ThreadRUdpServer* rudpMonServer = new ThreadRUdpServer(
        &incomingQueue,
        monitorPort,
        INADDR_ANY );

    ThreadUnixDsSender* unixDsSenderThread = new ThreadUnixDsSender(
        &exfDbgOutgoingQueue,
        SYSTEM_EXF,
        udsExfDbg );

    masterReceiverThread->start();
    apBroadcaster->start();
    rudpMonServer->start();
    unixDsSenderThread->start();

    maintenance( incomingQueue );
    return 0;    
}
