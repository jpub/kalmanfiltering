#include "byteBuffer.h"
#include "msgBase.h"
#include "msgApUpdateAck.h"
#include <stdio.h>


QItem MsgApUpdateAck::clone()
{
    MsgApUpdateAck* msg = new MsgApUpdateAck();
    if ( this->mRawBuffer )
        msg->mRawBuffer = new ByteBuffer( *this->mRawBuffer );
    msg->mHeader = this->mHeader;
    msg->mData = this->mData;

    return QItem( msg );
}

MsgApUpdateAck::MsgApUpdateAck()
{
    memset( &mData, 0, sizeof(mData) );
    mHeader.messageCode = ID_AP_UPDATE_ACK;
    mHeader.messageLength = getSize();
}

MsgApUpdateAck::MsgApUpdateAck( ByteBuffer* inBuffer )
    :MsgBase( inBuffer )
{
    int bytePosition = 0;
    unpack( bytePosition );
}

MsgApUpdateAck::MsgApUpdateAck( const MsgApUpdateAck& rhs )
    :MsgBase( rhs )
{
    mData = rhs.mData;
}

MsgApUpdateAck& MsgApUpdateAck::operator=( const MsgApUpdateAck& rhs )
{
    MsgBase::operator = (rhs);
    mData = rhs.mData;
    return *this;
}

int MsgApUpdateAck::getSize()
{
    int totalSize = 0;
    totalSize += MsgBase::getSize();
    totalSize += sizeof( mData );
    return totalSize;
}

ByteBuffer* MsgApUpdateAck::pack( int& bytePosition )
{
    MsgBase::pack( bytePosition );
    mRawBuffer->appendByteArray( (char*)&mData, sizeof(mData) );
    return mRawBuffer;
}

void MsgApUpdateAck::unpack( int& bytePosition )
{
    MsgBase::unpack( bytePosition );
    mRawBuffer->popByteArray( (char*)&mData, sizeof(mData) );
}

int MsgApUpdateAck::print()
{
    int charWritten = MsgBase::print();

    charWritten = snprintf( mPrintBuffer + charWritten, PRINT_BUFFER_SIZE,
        "ApId:%d Bitmask:%d SrThr:%d MrThr:%d LrThr:%d R0:%d D0:%d PosX:%d PosY:%d PosZ:%d",
        mData.apId,
        mData.bitmask,
        mData.apRssiThresholdSr,
        mData.apRssiThresholdMr,
        mData.apRssiLimit,
        mData.r0,
        mData.d0,
        mData.positionX,
        mData.positionY,
        mData.positionZ );

    LOG(DEBUG) << mPrintBuffer;
    return charWritten;
}
