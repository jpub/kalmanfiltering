#ifndef __MSG_AP_UPDATE_ACK_H
#define __MSG_AP_UPDATE_ACK_H

#include "msgBase.h"

class ByteBuffer;

class MsgApUpdateAck : public MsgBase
{
public:
    ENABLE_EVENT_DISPATCH();

public:
    virtual QItem clone();

public:
    MsgApUpdateAck();
    MsgApUpdateAck( ByteBuffer* inBuffer );
    MsgApUpdateAck( const MsgApUpdateAck& rhs );
    MsgApUpdateAck& operator=( const MsgApUpdateAck& rhs );

    virtual int getSize();
    virtual ByteBuffer* pack( int& bytePosition );
    virtual void unpack( int& bytePosition );
    virtual int print();

    AP_UPDATE_ACK* data()
    {
        return &mData;
    }
    
private:
    AP_UPDATE_ACK mData;
};

#endif
