#include <stdio.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pcap.h>
#include <time.h>
#include "__DefineVectorSize.h"
#include "__EventIds.h"
#include "__GenStructs.h"
#include "logger.h"
#include "threadMasterReceiver.h"
#include "threadUdpReceiver.h"
#include "threadUdpSender.h"
#include "threadPcap.h"
#include "threadRUdpServer.h"
#include "threadRUdpReceiver.h"
#include "threadUnixDsReceiver.h"
#include "threadUnixDsSender.h"
#include "eventHandlerSniffer.h"
#include "mgrConfigStatic.h"
#include "mgrConfigDynamic.h"
#include "mgrReporter.h"
#include "mgrPeriodic.h"
#include "factoryMsg.h"
#include "msgDiscoveryAp.h"
#include "tinyxml2.h"

#include "versionInfo.h"

INITIALIZE_EASYLOGGINGPP

// so that all incoming Mgr processing is single threaded
void maintenance( MsgQueue<QItem>& msgQueue )
{
    FactoryMsg factoryMsg;
    unsigned int counter = 0;
    MsgPeriodicTimer* msg = 0;
    int timerPeriod = MgrConfigStatic::getInstance().getMaintenanceTimer();
    
    while ( true )
    {
        ++counter;

        QItem qitem = factoryMsg.get( ID_PERIODIC_TIMER );
        msg = (MsgPeriodicTimer*)(qitem.getDataPointer());
        msg->data()->timerCounter = counter;
        msgQueue.add( qitem );

        usleep( timerPeriod );
    }
}

int main( int argc, char** argv )
{
    el::Configurations conf( "logSniffer.conf" );    
    el::Loggers::reconfigureAllLoggers( conf );

    if ( argc != 6 )
    {
        LOG(INFO) << "usage: sudo ./sniffer <SRC_AID> <SRC_MAC> <SRC_ADDRESS> <BC_ADDRESS> <SNIFF_INTERFACE>";
        return -1;
    }

    LOG(DEBUG) << "Command line arg: SRC_AID:" << argv[1];
    LOG(DEBUG) << "Command line arg: SRC_MAC:" << argv[2];
    LOG(DEBUG) << "Command line arg: SRC_ADDRESS:" << argv[3];
    LOG(DEBUG) << "Command line arg: BC_ADDRESS:" << argv[4];
    LOG(DEBUG) << "Command line arg: SNIFF_INTERFACE:" << argv[5];

    MgrConfigStatic::getInstance().init( "configStatic.xml" );
    MgrConfigStatic::getInstance().setSrcAid( atoi(argv[1]) );
    MgrConfigStatic::getInstance().setSrcMac( argv[2] );
    MgrConfigStatic::getInstance().setSrcAddress( argv[3] );
    MgrConfigStatic::getInstance().setBcAddress( argv[4] );

    MgrConfigDynamic::getInstance().init( "configDynamic.xml" );

    int broadcastPort = MgrConfigStatic::getInstance().getBcPort();
    int apApRudpPort = MgrConfigStatic::getInstance().getApApRudpPort();
    int srcAid = MgrConfigStatic::getInstance().getSrcAid();
    int sensorRegulatorSec = MgrConfigStatic::getInstance().getSensorRegulatorSec();
    string udsSnf = MgrConfigStatic::getInstance().getUdsSnf();
    string udsMlt = MgrConfigStatic::getInstance().getUdsMlt();
    const char* broadcastIp = argv[4];
    const char* srcMac = argv[2];
    const char* snifferInterface = argv[5];    

    MsgQueue<QItem> incomingQueue;
    MsgQueue<QItem> mltOutgoingQueue;
    MsgQueue<QItem> apBroadcastQueue;

    EventHandlerSniffer eventHandlerSniffer;

    MgrReporter::getInstance().init( &apBroadcastQueue, &mltOutgoingQueue, NULL, &incomingQueue );
    MgrReporter::getInstance().updateFilterList( MgrConfigStatic::getInstance().getFilterCodeVec() );

    ThreadMasterReceiver* masterReceiverThread = new ThreadMasterReceiver(
        &incomingQueue,
        &eventHandlerSniffer );

    ThreadUdpReceiver* broadcastReceiverThread = new ThreadUdpReceiver(
        &incomingQueue,
        broadcastPort,
        INADDR_ANY );

    ThreadUdpSender* apBroadcaster = new ThreadUdpSender(
        &apBroadcastQueue,
        SYSTEM_BROADCAST,
        broadcastIp,
        broadcastPort,
        INADDR_ANY,
        0 );

    ThreadRUdpServer* rudpApServer = new ThreadRUdpServer(
        &incomingQueue,
        apApRudpPort,
        INADDR_ANY );

    ThreadUnixDsReceiver* unixDsReceiverThread = new ThreadUnixDsReceiver(
        &incomingQueue,
        udsSnf );

    ThreadUnixDsSender* unixDsSenderThread = new ThreadUnixDsSender(
        &mltOutgoingQueue,
        SYSTEM_MLT,
        udsMlt );

    ThreadPcap* pcapThread = new ThreadPcap(
       &apBroadcastQueue,
       snifferInterface,
       srcMac,
       srcAid,
       MgrConfigStatic::getInstance().getIgnoreMap(),
       sensorRegulatorSec );

    masterReceiverThread->start();
    broadcastReceiverThread->start();
    apBroadcaster->start();
    rudpApServer->start();
    unixDsReceiverThread->start();
    unixDsSenderThread->start();
    pcapThread->start();

    maintenance( incomingQueue );
    return 0;    
}
