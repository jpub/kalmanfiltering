#ifndef __MSG_PERIODIC_TIMER_H
#define __MSG_PERIODIC_TIMER_H

#include "msgBase.h"

class ByteBuffer;

class MsgPeriodicTimer : public MsgBase
{
public:
    ENABLE_EVENT_DISPATCH();

public:
    virtual QItem clone();

public:
    MsgPeriodicTimer();
    MsgPeriodicTimer( ByteBuffer* inBuffer );
    MsgPeriodicTimer( const MsgPeriodicTimer& rhs );
    MsgPeriodicTimer& operator=( const MsgPeriodicTimer& rhs );

    virtual int getSize();
    virtual ByteBuffer* pack( int& bytePosition );
    virtual void unpack( int& bytePosition );
    virtual int print();

    PERIODIC_TIMER* data()
    {
        return &mData;
    }
    
private:
    PERIODIC_TIMER mData;
};

#endif
