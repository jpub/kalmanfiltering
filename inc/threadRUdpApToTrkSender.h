#ifndef __THREAD_RUDP_AP_TO_TRK_SEDNER_H
#define __THREAD_RUDP_AP_TO_TRK_SEDNER_H

#include <netdb.h>
#include "factoryMsg.h"
#include "threadRUdpBaseSender.h"
#include "udt.h"


class ThreadRUdpApToTrkSender : public ThreadRUdpBaseSender
{
public:
    ThreadRUdpApToTrkSender( MsgQueue<QItem>* outgoingQueue,
    						 int destinationId,
    						 const char* destIp,
    						 int destPort,
    						 const char* localBindIp,
    						 int localBindPort,
    						 int ttl );

    virtual void handleDisconnection();
    virtual void handleSuccessfulSending( MsgBase& msg );
};

#endif
