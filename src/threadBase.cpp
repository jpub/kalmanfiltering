#include "threadBase.h"


static void* runThread(void* arg)
{
    return ((ThreadBase*)arg)->run( arg );
}

ThreadBase::ThreadBase( MsgQueue<QItem>* msgQueue )
    : mQueue(msgQueue),
      mThread(0),
      mRunning(0),
      mDetached(0)
{
}

ThreadBase::~ThreadBase()
{
    if ( mRunning == 1 && mDetached == 0)
    {
        pthread_detach(mThread);
    }
    if ( mRunning == 1 )
    {
        pthread_cancel(mThread);
    }
}

int ThreadBase::start()
{
    int result = pthread_create(&mThread, NULL, runThread, this);
    
    if ( result == 0 )
    {
        mRunning = 1;
    }
    return result;
}

int ThreadBase::join()
{
    int result = -1;
    if ( mRunning == 1)
    {
        result = pthread_join(mThread,NULL);

        if ( result == 0 )
        {
            mDetached = 0;
        }
    }
    return result;
}

int ThreadBase::detach()
{
    int result = -1;

    if ( mRunning == 1 && mDetached == 0 )
    {
        result = pthread_detach( mThread );
        
        if ( result == 0 )
        {
            mDetached = 1;
        }
    }

    return result;
}

pthread_t ThreadBase::self()
{
    return mThread;
}

