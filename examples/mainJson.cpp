#include <stdio.h>
#include <string.h>
#include "json.hpp"

using json = nlohmann::json;
using std::string;

int main( void )
{
    json j;
    float piValue = 3.14;
    j["pi"] = piValue;
    printf( "%s\n", j.dump(4).c_str() );

    string inJson = "{ \"hello\":4.56}";
    json inJ = json::parse( inJson );
    float xxx = inJ["hello"];
    // will crash
    //int yyy = inJ["DoesNotExist"];
    printf( "hello=%f\n", xxx );
    //printf( "DoesNotExist=%d\n", yyy );
    return 0;
}
