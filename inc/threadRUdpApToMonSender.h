#ifndef __THREAD_RUDP_AP_TO_MON_SEDNER_H
#define __THREAD_RUDP_AP_TO_MON_SEDNER_H

#include <netdb.h>
#include "factoryMsg.h"
#include "threadRUdpBaseSender.h"
#include "udt.h"


class ThreadRUdpApToMonSender : public ThreadRUdpBaseSender
{
public:
    ThreadRUdpApToMonSender( MsgQueue<QItem>* outgoingQueue,
    						 int destinationId,
    						 const char* destIp,
    						 int destPort,
    						 const char* localBindIp,
    						 int localBindPort,
    						 int ttl );

    virtual void handleDisconnection();
};

#endif
