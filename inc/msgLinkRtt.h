#ifndef __MSG_LINK_RTT_H
#define __MSG_LINK_RTT_H

#include "msgBase.h"

class ByteBuffer;

class MsgLinkRtt : public MsgBase
{
public:
    ENABLE_EVENT_DISPATCH();

public:
    virtual QItem clone();

public:
    MsgLinkRtt();
    MsgLinkRtt( ByteBuffer* inBuffer );
    MsgLinkRtt( const MsgLinkRtt& rhs );
    MsgLinkRtt& operator=( const MsgLinkRtt& rhs );

    virtual int getSize();
    virtual ByteBuffer* pack( int& bytePosition );
    virtual void unpack( int& bytePosition );
    virtual int print();

    LINK_RTT* data()
    {
        return &mData;
    }
    
private:
    LINK_RTT mData;
};

#endif
