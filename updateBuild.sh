#!/bin/bash

VERSION=`grep -m 1 version changeLog.txt`
BUILD=`date +"%F %T"`

echo "#ifndef __VERSION_INFO" > inc/versionInfo.h
echo "#define __VERSION_INFO" >> inc/versionInfo.h
echo "" >> inc/versionInfo.h
echo "const char* VERSION_INFO = \"@@@@@@@@ v"$VERSION" "$BUILD"\";" >> inc/versionInfo.h
echo "" >> inc/versionInfo.h
echo "#endif" >> inc/versionInfo.h

