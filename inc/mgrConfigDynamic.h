#ifndef __MGR_CONFIG_DYNAMIC_H
#define __MGR_CONFIG_DYNAMIC_H

#include <string>
#include <vector>
#include "tinyxml2.h"

using std::string;
using std::vector;

class MgrConfigDynamic
{
public:
	static MgrConfigDynamic& getInstance();
	bool init( const char* filename );
	void writeToFile();
	
	int getThresholdSr();
	int getThresholdMr();
	int getLrCutoff();
	int getR0();
	int getD0();
	int getApPosX();
	int getApPosY();
	int getApPosZ();


public: // parameters that can be change from incoming messages
	void setThresholdSr( int thresholdSr );
	void setThresholdMr( int thresholdMr );
	void setLrCutoff( int lrCutoff );
	void setR0( int r0 );
	void setD0( int d0 );
	void setApPosX( int posX );
	void setApPosY( int posY );
	void setApPosZ( int posZ );


private:
	MgrConfigDynamic();
	const char* getElementText( const char* elementName );

private:
	static MgrConfigDynamic* msInstance;
	string mFilename;
	tinyxml2::XMLDocument mXmlDoc;
	tinyxml2::XMLElement* mRoot;
	tinyxml2::XMLElement* mElement;

private:	
	int mThresholdSr;
	int mThresholdMr;
	int mLrCutoff;
	int mR0;
	int mD0;
	int mApPosX;
	int mApPosY;
	int mApPosZ;
};

#endif