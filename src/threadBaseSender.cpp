#include "timeMachine.h"
#include "logger.h"
#include "threadBaseSender.h"
#include "mgrConfigStatic.h"

ThreadBaseSender::ThreadBaseSender( MsgQueue<QItem>* outgoingQueue, int destinationId )
    : ThreadBase( outgoingQueue ),
      mMessageNumber( 0 ),
      mDestinationId( destinationId )
{
	mSourceId = MgrConfigStatic::getInstance().getSrcAid();
}

void* ThreadBaseSender::run( void* arg )
{    
    char* nullArg = 0;

    while ( true )
    {
        QItem qitem = mQueue->remove();

        MsgBase* msg = (MsgBase*)(qitem.getDataPointer());  

        // only fill in source if you are the originator
        if ( msg->header()->orgSourceId == 0 )
        {
            msg->header()->orgSourceId = mSourceId;
        }
        // so that monitor can receive exactly what is being sent
        if ( mDestinationId != SYSTEM_MONITOR )
        {
            msg->header()->messageTime = TimeMachine::getTimeNow();
            msg->header()->messageNumber = ++mMessageNumber;
            msg->header()->sourceId = mSourceId;
            msg->header()->destinationId = mDestinationId;
        }

        if ( ! sendMessage(*qitem) )
        {
            LOG(WARNING) << "Sending thread error. Will exit thread";
            break;
        }

        handleSuccessfulSending( *qitem );
    }
    
    return nullArg;
}
