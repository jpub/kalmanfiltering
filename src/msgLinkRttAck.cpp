#include "byteBuffer.h"
#include "msgBase.h"
#include "msgLinkRttAck.h"
#include "logger.h"
#include <stdio.h>


QItem MsgLinkRttAck::clone()
{
    MsgLinkRttAck* msg = new MsgLinkRttAck();
    if ( this->mRawBuffer )
        msg->mRawBuffer = new ByteBuffer( *this->mRawBuffer );
    msg->mHeader = this->mHeader;
    msg->mData = this->mData;

    return QItem( msg );
}

MsgLinkRttAck::MsgLinkRttAck()
{
    memset( &mData, 0, sizeof(mData) );
    mHeader.messageCode = ID_LINK_RTT_ACK;
    mHeader.messageLength = getSize();
}

MsgLinkRttAck::MsgLinkRttAck( ByteBuffer* inBuffer )
    :MsgBase( inBuffer )
{
    int bytePosition = 0;
    unpack( bytePosition );
}

MsgLinkRttAck::MsgLinkRttAck( const MsgLinkRttAck& rhs )
    :MsgBase( rhs )
{
    mData = rhs.mData;
}

MsgLinkRttAck& MsgLinkRttAck::operator=( const MsgLinkRttAck& rhs )
{
    MsgBase::operator = (rhs);
    mData = rhs.mData;
    return *this;
}

int MsgLinkRttAck::getSize()
{
    int totalSize = 0;
    totalSize += MsgBase::getSize();
    totalSize += sizeof( mData );
    return totalSize;
}

ByteBuffer* MsgLinkRttAck::pack( int& bytePosition )
{
    MsgBase::pack( bytePosition );
    mRawBuffer->appendByteArray( (char*)&mData, sizeof(mData) );
    return mRawBuffer;
}

void MsgLinkRttAck::unpack( int& bytePosition )
{
    MsgBase::unpack( bytePosition );
    mRawBuffer->popByteArray( (char*)&mData, sizeof(mData) );
}

int MsgLinkRttAck::print()
{
    int charWritten = MsgBase::print();

    charWritten = snprintf( mPrintBuffer + charWritten, PRINT_BUFFER_SIZE,
        "Key:%d",
        mData.key );

    LOG(DEBUG) << mPrintBuffer;
    return charWritten;
}