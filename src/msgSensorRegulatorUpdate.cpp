#include "byteBuffer.h"
#include "msgBase.h"
#include "msgSensorRegulatorUpdate.h"
#include <stdio.h>


QItem MsgSensorRegulatorUpdate::clone()
{
    MsgSensorRegulatorUpdate* msg = new MsgSensorRegulatorUpdate();
    if ( this->mRawBuffer )
        msg->mRawBuffer = new ByteBuffer( *this->mRawBuffer );
    msg->mHeader = this->mHeader;
    msg->mData = this->mData;

    return QItem( msg );
}

MsgSensorRegulatorUpdate::MsgSensorRegulatorUpdate()
{
    memset( &mData, 0, sizeof(mData) );
    mHeader.messageCode = ID_SENSOR_REGULATOR_UPDATE;
    mHeader.messageLength = getSize();
}

MsgSensorRegulatorUpdate::MsgSensorRegulatorUpdate( ByteBuffer* inBuffer )
    :MsgBase( inBuffer )
{
    int bytePosition = 0;
    unpack( bytePosition );
}

MsgSensorRegulatorUpdate::MsgSensorRegulatorUpdate( const MsgSensorRegulatorUpdate& rhs )
    :MsgBase( rhs )
{
    mData = rhs.mData;
}

MsgSensorRegulatorUpdate& MsgSensorRegulatorUpdate::operator=( const MsgSensorRegulatorUpdate& rhs )
{
    MsgBase::operator = (rhs);
    mData = rhs.mData;
    return *this;
}

int MsgSensorRegulatorUpdate::getSize()
{
    int totalSize = 0;
    totalSize += MsgBase::getSize();
    totalSize += sizeof( mData );
    return totalSize;
}

ByteBuffer* MsgSensorRegulatorUpdate::pack( int& bytePosition )
{
    MsgBase::pack( bytePosition );
    mRawBuffer->appendByteArray( (char*)&mData, sizeof(mData) );
    return mRawBuffer;
}

void MsgSensorRegulatorUpdate::unpack( int& bytePosition )
{
    MsgBase::unpack( bytePosition );
    mRawBuffer->popByteArray( (char*)&mData, sizeof(mData) );
}

int MsgSensorRegulatorUpdate::print()
{
    int charWritten = MsgBase::print();

    charWritten = snprintf( mPrintBuffer + charWritten, PRINT_BUFFER_SIZE,
        "RegulatorSec:%d",
        mData.regulatorSeconds );

    LOG(DEBUG) << mPrintBuffer;
    return charWritten;
}
