#ifndef __MUTEX_LOCKER_H
#define __MUTEX_LOCKER_H


#include <pthread.h>

class MutexLocker
{
public:
    
    MutexLocker(pthread_mutex_t* aMutex)
    {
        pthread_mutex_lock(aMutex);
        mMutex = aMutex;
    }
    
    ~MutexLocker()
    {
        pthread_mutex_unlock(mMutex);
    }
    
private:
    pthread_mutex_t* mMutex;
};


#endif
