#ifndef __THREAD_RUDP_SERVER_H
#define __THREAD_RUDP_SERVER_H

#include <arpa/inet.h>
#include <netdb.h>
#include "threadBase.h"
#include "udt.h"
#include "factoryMsg.h"


class ThreadRUdpServer : public ThreadBase
{
public:
    ThreadRUdpServer( MsgQueue<QItem>* incomingQueue,
                      int incomingPort,
                      const char* localBindIp );
    
    virtual void* run( void* arg );

    
private:
    FactoryMsg mFactoryMsg;
    
    int mIncomingPort;
    const char* mLocalBindIp;

    UDTSOCKET mSockfd;
    struct addrinfo mHints;
    struct addrinfo* mOwnAddr;
    struct sockaddr mDestAddr;
    socklen_t mSlen;
    char* mPeekData;
};

#endif
