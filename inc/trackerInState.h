#ifndef __TRACKER_IN_STATE_H
#define __TRACKER_IN_STATE_H

#include <string>

using std::string;


class TrackerInState
{
public:
	string getDeviceId();
	double getCurrentTrackerLongitude();
	double getCurrentTrackerLatitude();
	double getDetectedLongitude();
	double getDetectedLatitude();
	unsigned int getLastUpdateTime();
	unsigned int getSecondsSinceTrackerLastUpdate();

	void setDeviceId( string deviceId );
	void setCurrentTrackerLongitude( double longitude );
	void setCurrentTrackerLatitude( double latitude );
	void setDetectedLongitude( double detectedLongitude );
	void setDetectedLatitude( double detectedLatitude );
	void setLastUpdateTime( unsigned int lastUpdateTime );
	void setSecondsSinceTrackerLastUpdate( unsigned int secondsSinceTrackerLastUpdate );

private:
	string mDeviceId;
	double mLongitude;
	double mLatitude;
	double mDetectedLongitude;
	double mDetectedLatitude;
	unsigned int mLastUpdateTime;
	unsigned int mSecondsSinceTrackerLastUpdate;
};

#endif