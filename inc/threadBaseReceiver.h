#ifndef __THREAD_BASE_RECEIVER_H
#define __THREAD_BASE_RECEIVER_H

#include "threadBase.h"
#include "ringBuffer.h"
#include "factoryMsg.h"

class ThreadBaseReceiver : public ThreadBase
{
public:
    ThreadBaseReceiver( MsgQueue<QItem>* outgoingQueue );

protected:
	virtual void processRingBuffer();

protected:
	FactoryMsg mFactoryMsg;
	RingBuffer mRingBuffer;
	char* mPeekData;
};
    
#endif
