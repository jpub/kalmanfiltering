#include "byteBuffer.h"
#include "msgBase.h"
#include "msgDbgDeviceUpdate.h"
#include <stdio.h>


QItem MsgDbgDeviceUpdate::clone()
{
    MsgDbgDeviceUpdate* msg = new MsgDbgDeviceUpdate();
    if ( this->mRawBuffer )
        msg->mRawBuffer = new ByteBuffer( *this->mRawBuffer );
    msg->mHeader = this->mHeader;
    msg->mData = this->mData;

    return QItem( msg );
}

MsgDbgDeviceUpdate::MsgDbgDeviceUpdate()
{
    memset( &mData, 0, sizeof(mData) );
    mHeader.messageCode = ID_DBG_DEVICE_UPDATE;
    mHeader.messageLength = getSize();
}

MsgDbgDeviceUpdate::MsgDbgDeviceUpdate( ByteBuffer* inBuffer )
    :MsgBase( inBuffer )
{
    int bytePosition = 0;
    unpack( bytePosition );
}

MsgDbgDeviceUpdate::MsgDbgDeviceUpdate( const MsgDbgDeviceUpdate& rhs )
    :MsgBase( rhs )
{
    mData = rhs.mData;
}

MsgDbgDeviceUpdate& MsgDbgDeviceUpdate::operator=( const MsgDbgDeviceUpdate& rhs )
{
    MsgBase::operator = (rhs);
    mData = rhs.mData;
    return *this;
}

int MsgDbgDeviceUpdate::getSize()
{
    int totalSize = 0;
    totalSize += MsgBase::getSize();

    int numberOfUnusedApInfo = __ARRAY_20_LENGTH - mData.numberOfApInfo;
    totalSize += ( sizeof(mData) - numberOfUnusedApInfo * sizeof(AP_INFO) );

    return totalSize;
}

ByteBuffer* MsgDbgDeviceUpdate::pack( int& bytePosition )
{
    MsgBase::pack( bytePosition );
    int variableSize = getSize() - sizeof(MSG_HEADER);
    mRawBuffer->appendByteArray( (char*)&mData, variableSize );
    return mRawBuffer;
}

void MsgDbgDeviceUpdate::unpack( int& bytePosition )
{
    MsgBase::unpack( bytePosition );
    int variableSize = mRawBuffer->getSize() - sizeof(MSG_HEADER);
    mRawBuffer->popByteArray( (char*)&mData, variableSize );
}

int MsgDbgDeviceUpdate::print()
{
    int charWritten = MsgBase::print();

    charWritten = snprintf( mPrintBuffer + charWritten, PRINT_BUFFER_SIZE,
        "DevMac:%02x%02x%02x%02x%02x%02x PosX:%d PosY:%d PosZ:%d MultiLatType:%d SrName:%d MrName1:%d MrName2:%d MrName3:%d NumOfApInfo:%d",
        mData.deviceMac[0],
        mData.deviceMac[1],
        mData.deviceMac[2],
        mData.deviceMac[3],
        mData.deviceMac[4],
        mData.deviceMac[5],
        mData.positionX,
        mData.positionY,
        mData.positionZ,
        mData.multilatType,
        mData.srApId,
        mData.mrApId1,
        mData.mrApId2,
        mData.mrApId3,
        mData.numberOfApInfo );

    LOG(DEBUG) << mPrintBuffer;
    return charWritten;
}
