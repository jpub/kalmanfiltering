// -*- C++ -*-
#ifndef __MSG_QUEUE_H
#define __MSG_QUEUE_H

#include "mutexLocker.h"
#include <list>

template<typename T> class MsgQueue
{
public:
    MsgQueue()
    {
        pthread_mutex_init( &mMutex, NULL );
        pthread_cond_init( &mCond, NULL );
    }
    
    ~MsgQueue()
    {
        pthread_mutex_destroy( &mMutex );
        pthread_cond_destroy( &mCond );
    }
    
    void add(T item)
    {
        MutexLocker mutexLock( &mMutex ); 

        mQueue.push_back(item);
        pthread_cond_signal(&mCond);
    }
    
    T remove()
    {
        MutexLocker mutexLock( &mMutex ); 
        
        while (mQueue.size() == 0)
        {
            pthread_cond_wait( &mCond, &mMutex );
        }
        T item = mQueue.front();
        mQueue.pop_front();
        return item;
    }
    
    int size()
    {
        MutexLocker mutexLock( &mMutex ); 
        
        int size = mQueue.size();
        return size;
    }
    
private:
    std::list<T> mQueue;
    pthread_mutex_t mMutex;
    pthread_cond_t mCond;
    
};

#endif
