#ifndef __EVENT_HANDLER_BASE_H
#define __EVENT_HANDLER_BASE_H



/*
 *	This is a parent class for all the Concrete Event Handlers.
 *  So that we can have a generic storage for all the
 *  Event Handlers in one container
 *
 *  All Concrete Event Handlers will multiple inherit from this 
 *  EventHandlerBase  and also the templated EventHandler<>
 */
class EventHandlerBase 
{
public:
	EventHandlerBase( void ) {}
	virtual ~EventHandlerBase( void ) {}
};


/*
 *	Concrete Event Handler will multiple inherit from EventHandlerBase 
 *  and this EventHandler<> providing the concrete Event that they are
 *  going to handle as the template argument
 */
template < typename EventType >
class EventHandler
{
public:
	EventHandler( void ) {}
	virtual ~EventHandler( void ) {}

public:
	virtual bool handle( EventType& event ) = 0;
};

#endif
