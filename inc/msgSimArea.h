#ifndef __MSG_SIM_AREA_H
#define __MSG_SIM_AREA_H

#include "msgBase.h"

class ByteBuffer;

class MsgSimArea : public MsgBase
{
public:
    ENABLE_EVENT_DISPATCH();

public:
    virtual QItem clone();

public:
    MsgSimArea();
    MsgSimArea( ByteBuffer* inBuffer );
    MsgSimArea( const MsgSimArea& rhs );
    MsgSimArea& operator=( const MsgSimArea& rhs );

    virtual int getSize();
    virtual ByteBuffer* pack( int& bytePosition );
    virtual void unpack( int& bytePosition );
    virtual int print();

    SIM_AREA* data()
    {
        return &mData;
    }
    
private:
    SIM_AREA mData;
};

#endif
