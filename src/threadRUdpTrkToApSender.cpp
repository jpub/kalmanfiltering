#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "mgrReporter.h"
#include "threadRUdpTrkToApSender.h"
#include "logger.h"


ThreadRUdpTrkToApSender::ThreadRUdpTrkToApSender(
    MsgQueue<QItem>* outgoingQueue,
    int destinationId,
    const char* destIp,
    int destPort,
    const char* localBindIp,
    int localBindPort,
    int ttl )
    : ThreadRUdpBaseSender(outgoingQueue, destinationId, destIp, destPort, localBindIp, localBindPort, ttl)
{
}

void ThreadRUdpTrkToApSender::handleDisconnection()
{
    LOG(WARNING) << "Connection with peer AP " << mDestIp << " lost:" << UDT::getlasterror().getErrorMessage();
    MgrReporter::getInstance().removeRdupApSendingQueue( mDestIp );
}

void ThreadRUdpTrkToApSender::handleSuccessfulSending( MsgBase& msg )
{
    if ( MgrReporter::getInstance().isMonRudpConnected() )
        MgrReporter::getInstance().rudpUnicastToMon( msg.clone() );
}
