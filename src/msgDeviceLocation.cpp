#include "byteBuffer.h"
#include "msgBase.h"
#include "msgDeviceLocation.h"
#include <stdio.h>


QItem MsgDeviceLocation::clone()
{
    MsgDeviceLocation* msg = new MsgDeviceLocation();
    if ( this->mRawBuffer )
        msg->mRawBuffer = new ByteBuffer( *this->mRawBuffer );
    msg->mHeader = this->mHeader;
    msg->mData = this->mData;

    return QItem( msg );
}

MsgDeviceLocation::MsgDeviceLocation()
{
    memset( &mData, 0, sizeof(mData) );
    mHeader.messageCode = ID_DEVICE_LOCATION;
    mHeader.messageLength = getSize();
}

MsgDeviceLocation::MsgDeviceLocation( ByteBuffer* inBuffer )
    :MsgBase( inBuffer )
{
    int bytePosition = 0;
    unpack( bytePosition );
}

MsgDeviceLocation::MsgDeviceLocation( const MsgDeviceLocation& rhs )
    :MsgBase( rhs )
{
    mData = rhs.mData;
}

MsgDeviceLocation& MsgDeviceLocation::operator=( const MsgDeviceLocation& rhs )
{
    MsgBase::operator = (rhs);
    mData = rhs.mData;
    return *this;
}

int MsgDeviceLocation::getSize()
{
    int totalSize = 0;
    totalSize += MsgBase::getSize();
    totalSize += sizeof( mData );
    return totalSize;
}

ByteBuffer* MsgDeviceLocation::pack( int& bytePosition )
{
    MsgBase::pack( bytePosition );
    mRawBuffer->appendByteArray( (char*)&mData, sizeof(mData) );
    return mRawBuffer;
}

void MsgDeviceLocation::unpack( int& bytePosition )
{
    MsgBase::unpack( bytePosition );
    mRawBuffer->popByteArray( (char*)&mData, sizeof(mData) );
}

int MsgDeviceLocation::print()
{
    int charWritten = MsgBase::print();

    charWritten = snprintf( mPrintBuffer + charWritten, PRINT_BUFFER_SIZE,
        "MacAdd:%02x%02x%02x%02x%02x%02x PosX:%d PosY:%d PosZ:%d",
        mData.macAddress[0],
        mData.macAddress[1],
        mData.macAddress[2],
        mData.macAddress[3],
        mData.macAddress[4],
        mData.macAddress[5],
        mData.positionX,
        mData.positionY,
        mData.positionZ );

    LOG(DEBUG) << mPrintBuffer;
    return charWritten;
}
