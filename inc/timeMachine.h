#ifndef __TIME_MACHINE_H
#define __TIME_MACHINE_H

class TimeMachine
{
public:
	static unsigned int getTimeNow();
	static long long unsigned int getMilliSinceEpoch();
};

#endif