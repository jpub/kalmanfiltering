#ifndef __MSG_G_UPDATE_ACK_H
#define __MSG_G_UPDATE_ACK_H

#include "msgBase.h"

class ByteBuffer;

class MsgGUpdateAck : public MsgBase
{
public:
    ENABLE_EVENT_DISPATCH();

public:
    virtual QItem clone();

public:
    MsgGUpdateAck();
    MsgGUpdateAck( ByteBuffer* inBuffer );
    MsgGUpdateAck( const MsgGUpdateAck& rhs );
    MsgGUpdateAck& operator=( const MsgGUpdateAck& rhs );

    virtual int getSize();
    virtual ByteBuffer* pack( int& bytePosition );
    virtual void unpack( int& bytePosition );
    virtual int print();

    G_UPDATE_ACK* data()
    {
        return &mData;
    }
    
private:
    G_UPDATE_ACK mData;
};

#endif
