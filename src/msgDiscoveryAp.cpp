#include "byteBuffer.h"
#include "msgBase.h"
#include "msgDiscoveryAp.h"
#include "logger.h"
#include <stdio.h>


QItem MsgDiscoveryAp::clone()
{
    MsgDiscoveryAp* msg = new MsgDiscoveryAp();
    if ( this->mRawBuffer )
        msg->mRawBuffer = new ByteBuffer( *this->mRawBuffer );
    msg->mHeader = this->mHeader;
    msg->mData = this->mData;

    return QItem( msg );
}

MsgDiscoveryAp::MsgDiscoveryAp()
{
    memset( &mData, 0, sizeof(mData) );
    mHeader.messageCode = ID_DISCOVERY_AP;
    mHeader.messageLength = getSize();
}

MsgDiscoveryAp::MsgDiscoveryAp( ByteBuffer* inBuffer )
    :MsgBase( inBuffer )
{
    int bytePosition = 0;
    unpack( bytePosition );
}

MsgDiscoveryAp::MsgDiscoveryAp( const MsgDiscoveryAp& rhs )
    :MsgBase( rhs )
{
    mData = rhs.mData;
}

MsgDiscoveryAp& MsgDiscoveryAp::operator=( const MsgDiscoveryAp& rhs )
{
    MsgBase::operator = (rhs);
    mData = rhs.mData;
    return *this;
}

int MsgDiscoveryAp::getSize()
{
    int totalSize = 0;
    totalSize += MsgBase::getSize();
    totalSize += sizeof( mData );
    return totalSize;
}

ByteBuffer* MsgDiscoveryAp::pack( int& bytePosition )
{
    MsgBase::pack( bytePosition );
    mRawBuffer->appendByteArray( (char*)&mData, sizeof(mData) );
    return mRawBuffer;
}

void MsgDiscoveryAp::unpack( int& bytePosition )
{
    MsgBase::unpack( bytePosition );
    mRawBuffer->popByteArray( (char*)&mData, sizeof(mData) );
}

int MsgDiscoveryAp::print()
{
    int charWritten = MsgBase::print();

    charWritten = snprintf( mPrintBuffer + charWritten, PRINT_BUFFER_SIZE,
        "SrcIP:%.*s TrackerId:%d TrackerIP:%.*s MonitorId:%d MonitorIP:%.*s",
        __ARRAY_20_LENGTH, mData.srcAddress,
        mData.trackerId,
        __ARRAY_20_LENGTH, mData.trackerAddress,
        mData.monitorId,
        __ARRAY_20_LENGTH, mData.monitorAddress );

    LOG(DEBUG) << mPrintBuffer;
    return charWritten;
}