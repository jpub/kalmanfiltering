#include <stdio.h>
#include <string>
#include "__EventIds.h"
#include "__GenStructs.h"
#include "mgrReporter.h"
#include "logger.h"
#include "threadPcap.h"
#include "factoryMsg.h"
#include "msgSensorUpdate.h"

using namespace std;

pthread_mutex_t ThreadPcap::msMutex;
bool ThreadPcap::msSimulation = false;
int ThreadPcap::msSensorRegulatorSec = 0;
map<string, string> ThreadPcap::msIgnoreMap; 
map<string, time_t> ThreadPcap::msRegulatorMap;
map<string, time_t>::iterator ThreadPcap::msIt;

void incomingPackets(
    unsigned char* args,
    const struct pcap_pkthdr* pkthdr,
    const unsigned char* packet )
{
    ThreadPcap::PcapCallbackIncomingPackets( args, pkthdr, packet );
}


ThreadPcap::ThreadPcap( MsgQueue<QItem>* outgoingQueue, const char* dev, const char* srcMac, int srcAid, map<string,string> ignoreMap, int sensorRegulatorSec )
    : ThreadBase( outgoingQueue ),
      mDev( dev ),
      mSrcAid( srcAid )
{
    msIgnoreMap = ignoreMap;

    pthread_mutex_init( &msMutex, NULL );
    msSensorRegulatorSec = sensorRegulatorSec;
    memcpy( mSrcMac, srcMac, __ARRAY_12_LENGTH );
}

ThreadPcap::~ThreadPcap()
{
    pthread_mutex_destroy( &msMutex );
}

void* ThreadPcap::run( void* arg )
{
    char* nullArg = 0;
    char errbuf[PCAP_ERRBUF_SIZE];
    pcap_if_t* alldevs;
    
    
    if ( pcap_findalldevs(&alldevs, errbuf) == -1 )
    {
        LOG(FATAL) << "Error getting " << errbuf;
        return nullArg;
    }
    
    int i=0;
    for ( pcap_if_t* d=alldevs; d != NULL; d=d->next )
    {
        ++i;
        if ( d->name )
        	LOG(INFO) << i << ": " << d->name;
        else
        	LOG(INFO) << i << ": no description";
    }
    LOG(INFO) << "Total interface: " << i;
    LOG(INFO) << "Sniffing on: " << mDev;

    mHandler = pcap_create( mDev, errbuf );
    if ( mHandler == NULL )
    {
    	LOG(FATAL) << "pcap create fail:" << errbuf;
        return nullArg;
    }

    std::string filterExp = "";

    // lan testing only..not real AP
    std::string devStr = mDev;
    if ( devStr.find("eth") != std::string::npos )
    {
        LOG(INFO) << "Pcap thread running in testing mode. Capture ICMP only";
        filterExp = "icmp";
        msSimulation = true;
    }
    else
    {
        filterExp = "wlan subtype probe-req";
        if ( pcap_set_rfmon(mHandler, 1) == 0 )
        {
            LOG(INFO) << "monitor mode enabled";
        }
        else
        {
            LOG(FATAL) << "Fail to set monitor mode";
            return nullArg;
        }      
    }
    
    int status1 = pcap_set_promisc( mHandler, 1 );
    LOG(DEBUG) << "set promisc status:" << status1;

    int status2 = pcap_set_timeout( mHandler, 1000 );
    LOG(DEBUG) << "set timeout status:" << status2;
    
    int status3 = pcap_activate( mHandler );
    LOG(DEBUG) << "activate sniff status:" << status3;

    struct bpf_program fp;
    bpf_u_int32 pNet = 0;

    int status4 = pcap_compile( mHandler, &fp, filterExp.c_str(), 0, pNet );
    LOG(DEBUG) << "compile filter status:" << status4;

    int status5 = pcap_setfilter( mHandler, &fp );
    LOG(DEBUG) << "set filter status:" << status5;

	int finalStatus =
		status1 |
		status2 |
		status3 |
		status4 |
		status5;

	if ( finalStatus != 0 )
	{
		LOG(FATAL) << "Pcap unable to setup correctly:" << finalStatus;
		return nullArg;
	}

    pcap_loop( mHandler, -1, incomingPackets, NULL );
    return nullArg;
}

void ThreadPcap::PcapCallbackIncomingPackets(
    unsigned char* args,
    const struct pcap_pkthdr* pkthdr,
    const unsigned char* packet )
{
    static FactoryMsg factoryMsg;
    RADIO_WIFI* radioWifi = (RADIO_WIFI*)packet;
    QItem qitem = factoryMsg.get( ID_SENSOR_UPDATE );
    
    MsgSensorUpdate* msg = (MsgSensorUpdate*)(qitem.getDataPointer());
    
    if ( ! ThreadPcap::msSimulation )
    {
        msg->data()->tgtRssi = radioWifi->rssi;
        memcpy( msg->data()->tgtMac, radioWifi->srcMac, __ARRAY_6_LENGTH );
    }

    //LOG(DEBUG) << "sniff something";
    //outgoingQueue->add( qitem );
    char tmpMacBuffer[20] = {0};
    snprintf( tmpMacBuffer, 13, "%02x%02x%02x%02x%02x%02x",
              msg->data()->tgtMac[0],
              msg->data()->tgtMac[1],
              msg->data()->tgtMac[2],
              msg->data()->tgtMac[3],
              msg->data()->tgtMac[4],
              msg->data()->tgtMac[5] );
    string deviceId( tmpMacBuffer );

    if ( CheckIgnoreMap(deviceId) )
    {
        LOG(DEBUG) << "skipped " << deviceId;
        return;
    }

    if ( CheckReportingTime(deviceId, pkthdr->ts.tv_sec) )
        MgrReporter::getInstance().rudpBroadcastToAp( qitem );
}

// need to protect the map
void ThreadPcap::Maintainance()
{
    MutexLocker mutexLock( &msMutex ); 
    // just clear away all entries instead of checking individual if outdated for better performance
    LOG(DEBUG) << "CLEARING!!!!!!!!!!!!!!!!!!!!!!";
    msRegulatorMap.clear();
}

bool ThreadPcap::CheckIgnoreMap( string deviceId )
{
    MutexLocker mutexLock( &msMutex );
    if ( msIgnoreMap.find(deviceId) == msIgnoreMap.end() )
    {
        return false;
    }
    // found in ignore list..will not send out
    return true;
}

bool ThreadPcap::CheckReportingTime( string deviceId, time_t updateTime )
{
    MutexLocker mutexLock( &msMutex ); 

    msIt = msRegulatorMap.find(deviceId);

    if ( msIt == msRegulatorMap.end() )
    {
        msRegulatorMap[deviceId] = updateTime;
        return true;
    }

    time_t lastUpdateTime = msIt->second;
    if ( updateTime - lastUpdateTime > msSensorRegulatorSec )
    {
        msIt->second = updateTime;
        return true;
    }

    // this sensor update will be lost since there is regulation
    return false;
}
