#include <stdlib.h>
#include <stdio.h>
#include "threadPcap.h"
#include "timeMachine.h"
#include "mgrConfigStatic.h"
#include "mgrConfigDynamic.h"
#include "mgrReporter.h"
#include "mgrPeriodic.h"
#include "mgrTracker.h"
#include "mgrArea.h"
#include "msgSensorStatus.h"
#include "msgDiscoveryAp.h"
#include "msgDiscoveryTrk.h"
#include "msgDiscoveryMon.h"
#include "msgLinkRttRequest.h"
#include "logger.h"


MgrPeriodic* MgrPeriodic::msInstance = 0;


MgrPeriodic::MgrPeriodic()
{
	mLinkQualityKey = 0;
}

MgrPeriodic& MgrPeriodic::getInstance()
{
	if ( msInstance == 0 )
	{
		msInstance = new MgrPeriodic();
		msInstance->init();
	}
	return *msInstance;
}

bool MgrPeriodic::init()
{
    string srcAddress = MgrConfigStatic::getInstance().getSrcAddress();
    if ( srcAddress.length() != 0 )
    {
	    memset( mSrcAddressArr, 0, __ARRAY_20_LENGTH );
	    memcpy( mSrcAddressArr, srcAddress.c_str(), srcAddress.length() );
	}

    string srcMac = MgrConfigStatic::getInstance().getSrcMac();
    if ( srcMac.length() != 0 )
    {
	    memset( mSrcMacArr, 0, __ARRAY_6_LENGTH );
	    char* pEnd;
	    for ( int i=0; i<__ARRAY_6_LENGTH; ++i )
	    {
	        mSrcMacArr[i] = (unsigned char)( strtol( srcMac.substr(i*2,2).c_str(), &pEnd, 16) );
	    }
	}

	return true;
}

void MgrPeriodic::sendApStatus()
{
	int apPosX = MgrConfigDynamic::getInstance().getApPosX();
	int apPosY = MgrConfigDynamic::getInstance().getApPosY();
	int apPosZ = MgrConfigDynamic::getInstance().getApPosZ();
	int thresholdSr = MgrConfigDynamic::getInstance().getThresholdSr();
	int thresholdMr = MgrConfigDynamic::getInstance().getThresholdMr();
	int lrCutoff = MgrConfigDynamic::getInstance().getLrCutoff();
    int r0 = MgrConfigDynamic::getInstance().getR0();
    int d0 = MgrConfigDynamic::getInstance().getD0();

	QItem qitem = mFactoryMsg.get( ID_SENSOR_STATUS );
    MsgSensorStatus* msg = (MsgSensorStatus*)(qitem.getDataPointer());

    msg->data()->positionX = apPosX;
    msg->data()->positionY = apPosY;
    msg->data()->positionZ = apPosZ;
    memcpy( msg->data()->commsAddress, mSrcMacArr, __ARRAY_6_LENGTH );
    msg->data()->apRssiThresholdSr = thresholdSr;
    msg->data()->apRssiThresholdMr = thresholdMr;
    msg->data()->apRssiLimit = lrCutoff;
    msg->data()->r0 = r0;
    msg->data()->d0 = d0;

    MgrReporter::getInstance().rudpBroadcastToAp( qitem );
    MgrReporter::getInstance().rudpUnicastToTrk( qitem->clone() );
}

void MgrPeriodic::sendDiscoveryAp()
{
    LOG(DEBUG) << "Send discovery ap";
	QItem qitem = mFactoryMsg.get( ID_DISCOVERY_AP );
    MsgDiscoveryAp* msg = (MsgDiscoveryAp*)(qitem.getDataPointer());
    memcpy( msg->data()->srcAddress, mSrcAddressArr, __ARRAY_20_LENGTH );
    
    string trackerAddress = MgrConfigStatic::getInstance().getTrackerAddress();
    msg->data()->trackerId = MgrConfigStatic::getInstance().getTrackerId();
    memcpy( msg->data()->trackerAddress, trackerAddress.c_str(), trackerAddress.length() );

    string monitorAddress = MgrConfigStatic::getInstance().getMonitorAddress();
    msg->data()->monitorId = MgrConfigStatic::getInstance().getMonitorId();
    memcpy( msg->data()->monitorAddress, monitorAddress.c_str(), monitorAddress.length() );

    MgrReporter::getInstance().udpBroadcastToAp( qitem );

    // to cater for APs which are more than 1 hop away
    MgrReporter::getInstance().rudpUnicastToTrk( qitem->clone() );
    MgrReporter::getInstance().rudpUnicastToMon( qitem->clone() );

}

void MgrPeriodic::sendDiscoveryTrk()
{
    LOG(DEBUG) << "Send discovery trk";
	QItem qitem = mFactoryMsg.get( ID_DISCOVERY_TRK );
    MsgDiscoveryTrk* msg = (MsgDiscoveryTrk*)(qitem.getDataPointer());
    memcpy( msg->data()->srcAddress, mSrcAddressArr, __ARRAY_20_LENGTH );
    MgrReporter::getInstance().udpBroadcastToAp( qitem );
}

void MgrPeriodic::sendDiscoveryMon()
{
    LOG(DEBUG) << "Send discovery mon";
	QItem qitem = mFactoryMsg.get( ID_DISCOVERY_MON );
    MsgDiscoveryMon* msg = (MsgDiscoveryMon*)(qitem.getDataPointer());
    memcpy( msg->data()->srcAddress, mSrcAddressArr, __ARRAY_20_LENGTH );
    MgrReporter::getInstance().udpBroadcastToAp( qitem );
}

void MgrPeriodic::sendLinkLatencyRequest()
{
    LOG(DEBUG) << "Send link latency request";
	QItem qitem = mFactoryMsg.get( ID_LINK_RTT_REQUEST );
    MsgLinkRttRequest* msg = (MsgLinkRttRequest*)(qitem.getDataPointer());

    msg->data()->key = ++mLinkQualityKey;
    msg->data()->milliSinceEpoch = TimeMachine::getMilliSinceEpoch();

    MgrReporter::getInstance().rudpBroadcastToAp( qitem );
}

void MgrPeriodic::sendAreaUpdate()
{
    LOG(DEBUG) << "Send area update";
    MgrArea::getInstance().sendAreaUpdate();
}

void MgrPeriodic::maintainTracks()
{
    MgrTracker::getInstance().maintainance();
}

void MgrPeriodic::maintainSensorRegulator()
{
    ThreadPcap::Maintainance();
}