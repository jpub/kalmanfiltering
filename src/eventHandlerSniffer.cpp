#include <string>
#include "__DefineVectorSize.h"
#include "mgrConfigStatic.h"
#include "mgrConfigDynamic.h"
#include "mgrReporter.h"
#include "mgrPeriodic.h"
#include "threadRUdpApToApSender.h"
#include "threadRUdpApToTrkSender.h"
#include "threadRUdpApToMonSender.h"
#include "eventHandlerSniffer.h"
#include "logger.h"

using namespace std;

EventHandlerSniffer::EventHandlerSniffer()
{
	mCycleDiscovery = MgrConfigStatic::getInstance().getCycleDiscovery();
	mCycleApStatus = MgrConfigStatic::getInstance().getCycleApStatus();
	mCycleSensorRegulatorMaintenance = MgrConfigStatic::getInstance().getCycleSensorRegulatorMaintenance();
}

EventHandlerSniffer::~EventHandlerSniffer()
{
}

bool EventHandlerSniffer::handle( MsgPeriodicTimer& msg )
{
	// every update is 5 seconds
	LOG(DEBUG) << "handle timer update: Code:" << msg.header()->messageCode;

	unsigned int timerCounter = msg.data()->timerCounter;

	if ( timerCounter % mCycleDiscovery == 0 )
		MgrPeriodic::getInstance().sendDiscoveryAp();

	if ( timerCounter % mCycleApStatus == 0 )
		MgrPeriodic::getInstance().sendApStatus();

	if ( timerCounter % mCycleSensorRegulatorMaintenance == 0 )
		MgrPeriodic::getInstance().maintainSensorRegulator();
	
	return true;
}

bool EventHandlerSniffer::handle( MsgSensorUpdate& msg )
{
	//LOG(DEBUG) << "handle sensor update";
	int src = msg.header()->sourceId;
	// send to MLT
	MgrReporter::getInstance().sendToMlt( msg.clone() );

	if ( mLastReceivedSrcCounter.find(src) == mLastReceivedSrcCounter.end() )
	{
		return true;
	}

	if ( mRunningSrcCounter.find(src) == mRunningSrcCounter.end() )
		mRunningSrcCounter[src] = 1;
	else
		mRunningSrcCounter[src] += 1;	

	return true;
}

bool EventHandlerSniffer::handle( MsgDeviceLocation& msg )
{
	LOG(DEBUG) << "handle device location: Code:" << msg.header()->messageCode;

	// send to TRK
	MgrReporter::getInstance().rudpUnicastToTrk( msg.clone() );

	return true;
}

bool EventHandlerSniffer::handle( MsgDbgDeviceUpdate& msg )
{
	LOG(DEBUG) << "handle dbg device update: Code:" << msg.header()->messageCode;

	// send to MON
	if ( MgrReporter::getInstance().isMonRudpConnected() )
        MgrReporter::getInstance().rudpUnicastToMon( msg.clone() );

	return true;
}

bool EventHandlerSniffer::handle( MsgSensorStatus& msg )
{
	LOG(DEBUG) << "handle sensor status: Code:" << msg.header()->messageCode;
	
	// send to MLT
	MgrReporter::getInstance().sendToMlt( msg.clone() );

	return true;
}

bool EventHandlerSniffer::handle( MsgSendCounter& msg )
{	
	int src = msg.header()->sourceId;
	LOG(DEBUG) << "handle send counter from " << src << ": Code:" << msg.header()->messageCode;
	unsigned int counter = msg.data()->sendCounter;

	map<int,int>::iterator it;
	if ( mLastReceivedSrcCounter.find(src) == mLastReceivedSrcCounter.end() )
	{
		mLastReceivedSrcCounter[src] = counter;
		LOG(DEBUG) << "First encounter";
		return true;
	}

	unsigned int totalSend = counter - mLastReceivedSrcCounter[src];
	unsigned int totalReceived = mRunningSrcCounter[src];

	// reset the counter so that we can have a running link quality
	mLastReceivedSrcCounter[src] = counter;
	mRunningSrcCounter[src] = 0;

	QItem qitem = mFactoryMsg.get( ID_LINK_QUALITY );
    MsgLinkQuality* msgLinkQuality = (MsgLinkQuality*)(qitem.getDataPointer());
    msgLinkQuality->data()->clientId = src;
    msgLinkQuality->data()->receivedPercentage = ((float)totalReceived/totalSend)*100;
    msgLinkQuality->data()->totalReceived = totalReceived;
    msgLinkQuality->data()->totalSend = totalSend;

    // send to TRK
    MgrReporter::getInstance().rudpUnicastToTrk( qitem );

	return true;
}

bool EventHandlerSniffer::handle( MsgLinkQuality& msg )
{
	int src = msg.header()->sourceId;
	int client = msg.data()->clientId;
	int percent = msg.data()->receivedPercentage;
	int totalReceived = msg.data()->totalReceived;
	int totalSend = msg.data()->totalSend;


	LOG(DEBUG) << "handle link quality from " << src << " : Code:" << msg.header()->messageCode;
	LOG(DEBUG) << "LINK QUALITY " << src << " -- " << client << " : " << percent << "%  " << totalReceived << "/" << totalSend;

	return true;
}

bool EventHandlerSniffer::handle( MsgDiscoveryAp& msg )
{
	LOG(DEBUG) << "handle Discovery AP: Code:" << msg.header()->messageCode;
	int apId = msg.header()->orgSourceId;
	LOG(DEBUG) << "AP " << apId << " - " << msg.data()->srcAddress;
	
	char apIpArr[__ARRAY_20_LENGTH] = {0};
	memcpy( apIpArr, msg.data()->srcAddress, __ARRAY_20_LENGTH );
	string apIp = apIpArr;	

	// connection to AP
	if ( ! MgrReporter::getInstance().isApRudpConnected(apIp) )
	{
		int apApRudpPort = MgrConfigStatic::getInstance().getApApRudpPort();
		int ttl = MgrConfigStatic::getInstance().getTtl();
		int linkQualityCounter = MgrConfigStatic::getInstance().getLinkQualityCounter();

		MsgQueue<QItem>* rudpApOutgoingQueue = new MsgQueue<QItem>(); 
		ThreadRUdpApToApSender* rudpApToApSender = new ThreadRUdpApToApSender(
			rudpApOutgoingQueue,
			apId,
	        apIp.c_str(),
	        apApRudpPort,
	        INADDR_ANY,
	        0,
	        ttl,
	        linkQualityCounter );

		rudpApToApSender->start();
		MgrReporter::getInstance().addRudpApSendingQueue( apIp, rudpApOutgoingQueue );
	}

	// connection to TRK
	if ( msg.data()->trackerId != 0 )
	{
		int trackerId = msg.data()->trackerId;
		char trackerIpArr[__ARRAY_20_LENGTH] = {0};
		memcpy( trackerIpArr, msg.data()->trackerAddress, __ARRAY_20_LENGTH );
		string trackerIp = trackerIpArr;

		if ( ! MgrReporter::getInstance().isTrkRudpConnected(trackerIp) )
		{
			LOG(DEBUG) << "Connecting to Tracker " << trackerIp << " thru AP Discovery";
			int trackerPort = MgrConfigStatic::getInstance().getTrackerPort();
			int ttl = MgrConfigStatic::getInstance().getTtl();

			// to cater for more than 1 hop away from tracker
			MgrConfigStatic::getInstance().setTrackerId( trackerId );
			MgrConfigStatic::getInstance().setTrackerAddress( trackerIp );

			MsgQueue<QItem>* rudpTrkOutgoingQueue = new MsgQueue<QItem>(); 
			ThreadRUdpApToTrkSender* rudpApToTrkSender = new ThreadRUdpApToTrkSender(
				rudpTrkOutgoingQueue,
				trackerId,
		        trackerIp.c_str(),
		        trackerPort,
		        INADDR_ANY,
		        0,
		        ttl );

			rudpApToTrkSender->start();
			MgrReporter::getInstance().addRudpTrkSendingQueue( trackerIp, rudpTrkOutgoingQueue );
		}
	}

	// connection to MON
	if ( msg.data()->monitorId != 0 )
	{
		int monitorId = msg.data()->monitorId;
		char monitorIpArr[__ARRAY_20_LENGTH] = {0};
		memcpy( monitorIpArr, msg.data()->monitorAddress, __ARRAY_20_LENGTH );
		string monitorIp = monitorIpArr;

		if ( ! MgrReporter::getInstance().isMonRudpConnected() )
		{
			LOG(DEBUG) << "Connecting to Monitor " << monitorIp << " thru AP Discovery";
			int monitorPort = MgrConfigStatic::getInstance().getMonitorPort();
			int ttl = MgrConfigStatic::getInstance().getTtl();

			// to cater for more than 1 hop away from monitor
			MgrConfigStatic::getInstance().setMonitorId( monitorId );
			MgrConfigStatic::getInstance().setMonitorAddress( monitorIp );

			MsgQueue<QItem>* rudpMonOutgoingQueue = new MsgQueue<QItem>(); 
			ThreadRUdpApToMonSender* rudpApToMonSender = new ThreadRUdpApToMonSender(
				rudpMonOutgoingQueue,
				monitorId,
		        monitorIp.c_str(),
		        monitorPort,
		        INADDR_ANY,
		        0,
		        ttl );

			rudpApToMonSender->start();
			MgrReporter::getInstance().addRudpMonSendingQueue( monitorIp, rudpMonOutgoingQueue );
		}
	}
	return true;
}

bool EventHandlerSniffer::handle( MsgDiscoveryTrk& msg )
{
	LOG(DEBUG) << "handle Discovery TRK: Code:" << msg.header()->messageCode;
	int destId = msg.header()->orgSourceId;
	LOG(DEBUG) << "TRK " << destId << " - " << msg.data()->srcAddress;

	char destIpArr[__ARRAY_20_LENGTH] = {0};
	memcpy( destIpArr, msg.data()->srcAddress, __ARRAY_20_LENGTH );
	string destIp = destIpArr;

	// to cater for more than 1 hop away from tracker
	MgrConfigStatic::getInstance().setTrackerId( destId );
	MgrConfigStatic::getInstance().setTrackerAddress( destIp );

	if ( MgrReporter::getInstance().isTrkRudpConnected(destIp) )
		return true;

	int trackerPort = MgrConfigStatic::getInstance().getTrackerPort();
	int ttl = MgrConfigStatic::getInstance().getTtl();

	MsgQueue<QItem>* rudpTrkOutgoingQueue = new MsgQueue<QItem>(); 
	ThreadRUdpApToTrkSender* rudpApToTrkSender = new ThreadRUdpApToTrkSender(
		rudpTrkOutgoingQueue,
		destId,
        destIp.c_str(),
        trackerPort,
        INADDR_ANY,
        0,
        ttl );

	rudpApToTrkSender->start();
	MgrReporter::getInstance().addRudpTrkSendingQueue( destIp, rudpTrkOutgoingQueue );

	return true;
}

bool EventHandlerSniffer::handle( MsgDiscoveryMon& msg )
{
	LOG(DEBUG) << "handle Discovery MON: Code:" << msg.header()->messageCode;
	int destId = msg.header()->orgSourceId;
	LOG(DEBUG) << "MON " << destId << " - " << msg.data()->srcAddress;

	char destIpArr[__ARRAY_20_LENGTH] = {0};
	memcpy( destIpArr, msg.data()->srcAddress, __ARRAY_20_LENGTH );
	string destIp = destIpArr;

	// to cater for more than 1 hop away from tracker
	MgrConfigStatic::getInstance().setMonitorId( destId );
	MgrConfigStatic::getInstance().setMonitorAddress( destIp );

	if ( MgrReporter::getInstance().isMonRudpConnected() )
		return true;

	int monitorPort = MgrConfigStatic::getInstance().getMonitorPort();
	int ttl = MgrConfigStatic::getInstance().getTtl();

	MsgQueue<QItem>* rudpMonOutgoingQueue = new MsgQueue<QItem>(); 
	ThreadRUdpApToMonSender* rudpApToMonSender = new ThreadRUdpApToMonSender(
		rudpMonOutgoingQueue,
		destId,
        destIp.c_str(),
        monitorPort,
        INADDR_ANY,
        0,
        ttl );

	rudpApToMonSender->start();
	MgrReporter::getInstance().addRudpMonSendingQueue( destIp, rudpMonOutgoingQueue );

	return true;
}

bool EventHandlerSniffer::handle( MsgMltStatus& msg )
{
	LOG(DEBUG) << "handle MLT status: Code:" << msg.header()->messageCode;
	// send to TRK
	MgrReporter::getInstance().rudpUnicastToTrk( msg.clone() );
	return true;
}

bool EventHandlerSniffer::handle( MsgMltStatusRequest& msg )
{
	LOG(DEBUG) << "handle MLT status request: Code:" << msg.header()->messageCode;
	// send to MLT
	MgrReporter::getInstance().sendToMlt( msg.clone() );
	return true;
}

bool EventHandlerSniffer::handle( MsgGUpdate& msg )
{
	LOG(DEBUG) << "handle G update: Code:" << msg.header()->messageCode;
	// send to MLT
	MgrReporter::getInstance().sendToMlt( msg.clone() );
	return true;
}

bool EventHandlerSniffer::handle( MsgGUpdateAck& msg )
{
	LOG(DEBUG) << "handle G update ack: Code:" << msg.header()->messageCode;
	// send to TRK
	MgrReporter::getInstance().rudpUnicastToTrk( msg.clone() );
	return true;
}

bool EventHandlerSniffer::handle( MsgApUpdate& msg )
{
	LOG(DEBUG) << "handle AP update: Code:" << msg.header()->messageCode;

	int apId = msg.data()->apId;
	if ( apId != MgrConfigStatic::getInstance().getSrcAid() )
	{
		LOG(DEBUG) << "skipping AP update meant for AP " << apId;
		return false;
	}

	int MASK_RSSI_SR_THRESHOLD = 1 << 0;
	int MASK_RSSI_MR_THRESHOLD = 1 << 1;
	int MASK_RSSI_LR_CUTOFF = 1 << 2;
	int MASK_R0_D0 = 1 << 3;
	int MASK_POSITION = 1 << 4;

	int mask = msg.data()->bitmask;

	if ( mask & MASK_RSSI_SR_THRESHOLD )
		MgrConfigDynamic::getInstance().setThresholdSr( msg.data()->apRssiThresholdSr );

	if ( mask & MASK_RSSI_MR_THRESHOLD )
		MgrConfigDynamic::getInstance().setThresholdMr( msg.data()->apRssiThresholdMr );

	if ( mask & MASK_RSSI_LR_CUTOFF )
		MgrConfigDynamic::getInstance().setLrCutoff( msg.data()->apRssiLimit );

	if ( mask & MASK_R0_D0 )
	{
		MgrConfigDynamic::getInstance().setR0( msg.data()->r0 );
		MgrConfigDynamic::getInstance().setD0( msg.data()->d0 );
	}

	if ( mask & MASK_POSITION )
	{
		MgrConfigDynamic::getInstance().setApPosX( msg.data()->positionX );
		MgrConfigDynamic::getInstance().setApPosY( msg.data()->positionY );
		MgrConfigDynamic::getInstance().setApPosZ( msg.data()->positionZ );
	}

	MgrConfigDynamic::getInstance().writeToFile();
	MgrPeriodic::getInstance().sendApStatus();
	LOG(DEBUG) << "successfully update AP";

	return true;
}

bool EventHandlerSniffer::handle( MsgLinkRttRequest& msg )
{
	LOG(DEBUG) << "handle link RTT request: Code:" << msg.header()->messageCode;

	QItem qitem = mFactoryMsg.get( ID_LINK_RTT_ACK );
    MsgLinkRttAck* msgLinkRttAck = (MsgLinkRttAck*)(qitem.getDataPointer());
    msgLinkRttAck->data()->key = msg.data()->key;
    msgLinkRttAck->data()->milliSinceEpoch = msg.data()->milliSinceEpoch;

	// send to TRK
	MgrReporter::getInstance().rudpUnicastToTrk( qitem );
	return true;
}