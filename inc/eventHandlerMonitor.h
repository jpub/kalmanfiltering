#ifndef __EVENT_HANDLER_MONITOR_H
#define __EVENT_HANDLER_MONITOR_H

#include <map>
#include "eventHandlerBase.h"
#include "allMessages.h"


class EventHandlerMonitor
	: public EventHandlerBase,
	  public EventHandler<MsgPeriodicTimer>,
	  public EventHandler<MsgSensorUpdate>,
	  public EventHandler<MsgDbgDeviceUpdate>,
	  public EventHandler<MsgDeviceLocation>,
	  public EventHandler<MsgSensorStatus>,
	  public EventHandler<MsgSendCounter>,
	  public EventHandler<MsgLinkQuality>,
	  public EventHandler<MsgDiscoveryAp>,
	  public EventHandler<MsgDiscoveryTrk>,
	  public EventHandler<MsgDiscoveryMon>,
	  public EventHandler<MsgMltStatus>,
	  public EventHandler<MsgMltStatusRequest>,
	  public EventHandler<MsgGUpdate>,
	  public EventHandler<MsgGUpdateAck>,
	  public EventHandler<MsgApUpdate>,
	  public EventHandler<MsgApUpdateAck>,
	  public EventHandler<MsgLinkRttRequest>,
	  public EventHandler<MsgLinkRttAck>,
	  public EventHandler<MsgLinkRtt>,
	  public EventHandler<MsgSensorRegulatorUpdate>,
	  public EventHandler<MsgTrackedLocation>

{
public:
	EventHandlerMonitor();
	virtual ~EventHandlerMonitor();

public:
	virtual bool handle( MsgPeriodicTimer& );
	virtual bool handle( MsgSensorUpdate& );
	virtual bool handle( MsgDbgDeviceUpdate& );
	virtual bool handle( MsgDeviceLocation& );
	virtual bool handle( MsgSensorStatus& );
	virtual bool handle( MsgSendCounter& );
	virtual bool handle( MsgLinkQuality& );
	virtual bool handle( MsgDiscoveryAp& );
	virtual bool handle( MsgDiscoveryTrk& );
	virtual bool handle( MsgDiscoveryMon& );
	virtual bool handle( MsgMltStatus& );
	virtual bool handle( MsgMltStatusRequest& );
	virtual bool handle( MsgGUpdate& );
	virtual bool handle( MsgGUpdateAck& );
	virtual bool handle( MsgApUpdate& );
	virtual bool handle( MsgApUpdateAck& );
	virtual bool handle( MsgLinkRttRequest& );
	virtual bool handle( MsgLinkRttAck& );
	virtual bool handle( MsgLinkRtt& );
	virtual bool handle( MsgSensorRegulatorUpdate& );
	virtual bool handle( MsgTrackedLocation& );

private:
	bool defaultHandling( MsgBase& );

private:
	int mCycleDiscovery;
};

#endif