#ifndef __THREAD_UNIX_DS_RECEIVER_H
#define __THREAD_UNIX_DS_RECEIVER_H

#include <arpa/inet.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <string>
#include "threadBaseReceiver.h"

class ThreadUnixDsReceiver : public ThreadBaseReceiver
{
public:
    ThreadUnixDsReceiver( MsgQueue<QItem>* incomingQueue,
                          std::string localPath );
    
    virtual void* run( void* arg );

private:
    std::string mLocalPath;
    int mSockfd;
    struct sockaddr_un mLocalAddr;
};

#endif
