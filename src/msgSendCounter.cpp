#include "byteBuffer.h"
#include "msgBase.h"
#include "msgSendCounter.h"
#include <stdio.h>


QItem MsgSendCounter::clone()
{
    MsgSendCounter* msg = new MsgSendCounter();
    if ( this->mRawBuffer )
        msg->mRawBuffer = new ByteBuffer( *this->mRawBuffer );
    msg->mHeader = this->mHeader;
    msg->mData = this->mData;

    return QItem( msg );
}

MsgSendCounter::MsgSendCounter()
{
    memset( &mData, 0, sizeof(mData) );
    mHeader.messageCode = ID_SEND_COUNTER;
    mHeader.messageLength = getSize();
}

MsgSendCounter::MsgSendCounter( ByteBuffer* inBuffer )
    :MsgBase( inBuffer )
{
    int bytePosition = 0;
    unpack( bytePosition );
}

MsgSendCounter::MsgSendCounter( const MsgSendCounter& rhs )
    :MsgBase( rhs )
{
    mData = rhs.mData;
}

MsgSendCounter& MsgSendCounter::operator=( const MsgSendCounter& rhs )
{
    MsgBase::operator = (rhs);
    mData = rhs.mData;
    return *this;
}

int MsgSendCounter::getSize()
{
    int totalSize = 0;
    totalSize += MsgBase::getSize();
    totalSize += sizeof( mData );
    return totalSize;
}

ByteBuffer* MsgSendCounter::pack( int& bytePosition )
{
    MsgBase::pack( bytePosition );
    mRawBuffer->appendByteArray( (char*)&mData, sizeof(mData) );
    return mRawBuffer;
}

void MsgSendCounter::unpack( int& bytePosition )
{
    MsgBase::unpack( bytePosition );
    mRawBuffer->popByteArray( (char*)&mData, sizeof(mData) );
}

int MsgSendCounter::print()
{
    int charWritten = MsgBase::print();

    charWritten = snprintf( mPrintBuffer + charWritten, PRINT_BUFFER_SIZE,
        "TimerCounter:%d",
        mData.sendCounter );

    LOG(DEBUG) << mPrintBuffer;
    return charWritten;
}
