#ifndef __MGR_PERIODIC_H
#define __MGR_PERIODIC_H

#include <string>
#include "__DefineVectorSize.h"
#include "factoryMsg.h"

using std::string;

class MgrPeriodic
{
public:
	static MgrPeriodic& getInstance();
	bool init();
	void sendApStatus();
	void sendDiscoveryAp();
	void sendDiscoveryTrk();
	void sendDiscoveryMon();
	void sendLinkLatencyRequest();
	void sendAreaUpdate();
	void maintainTracks();
	void maintainSensorRegulator();
	
private:
	MgrPeriodic();

private:
	static MgrPeriodic* msInstance;


private:
	FactoryMsg mFactoryMsg;
	unsigned char mSrcMacArr[__ARRAY_6_LENGTH];
	unsigned char mSrcAddressArr[__ARRAY_20_LENGTH];
	unsigned short mLinkQualityKey;
};

#endif