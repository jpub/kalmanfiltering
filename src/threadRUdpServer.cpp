#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "__DefineVectorSize.h"
#include "threadRUdpServer.h"
#include "threadRUdpReceiver.h"
#include "byteBuffer.h"
#include "msgQueue.h"
#include "logger.h"




ThreadRUdpServer::ThreadRUdpServer(
    MsgQueue<QItem>* incomingQueue,
    int incomingPort,
    const char* localBindIp )
    : ThreadBase(incomingQueue),
      mIncomingPort(incomingPort),
      mLocalBindIp(localBindIp)
{
    memset( &mHints, 0, sizeof(struct addrinfo) );
    mHints.ai_flags = AI_PASSIVE;
    mHints.ai_family = AF_INET;
    mHints.ai_socktype = SOCK_DGRAM;

    char incomingPortStr[PORT_STRING_LENGTH] = {0};
    sprintf( incomingPortStr, "%d", mIncomingPort );
    if ( getaddrinfo(NULL, incomingPortStr, &mHints, &mOwnAddr) != 0 )
    {
        LOG(FATAL) << "Unable to get own address or port busy:" << UDT::getlasterror().getErrorMessage();
    }

    mSockfd = UDT::socket( mOwnAddr->ai_family, mOwnAddr->ai_socktype, mOwnAddr->ai_protocol );
    if ( mSockfd == -1 )
    {
        LOG(FATAL) << "Create socket fail:" << UDT::getlasterror().getErrorMessage();
    }

    if ( UDT::bind(mSockfd, mOwnAddr->ai_addr, mOwnAddr->ai_addrlen) == UDT::ERROR )
    {
        LOG(FATAL) << "Unable to bind server socket:" << UDT::getlasterror().getErrorMessage();
    }

    if ( UDT::listen(mSockfd, MAX_CONNECTED_CLIENT) == UDT::ERROR )
    {
        LOG(FATAL) << "Unable to listen on socket:" << UDT::getlasterror().getErrorMessage();
    }

    LOG(INFO) << "UDT server listening on port:" << mIncomingPort;
}

void* ThreadRUdpServer::run( void* arg )
{
    char* nullArg = 0;
    sockaddr_storage clientaddr;
    int addrlen = sizeof(clientaddr);
    UDTSOCKET clientfd;

    while ( true )
    {
        clientfd = UDT::accept( mSockfd, (sockaddr*)&clientaddr, &addrlen );
        if ( clientfd == UDT::INVALID_SOCK )
        {
            LOG(FATAL) << "Server accept socket failure:" << UDT::getlasterror().getErrorMessage();
            return nullArg;
        }

        char clienthost[NI_MAXHOST];
        char clientservice[NI_MAXSERV];
        getnameinfo((sockaddr *)&clientaddr, addrlen, clienthost, sizeof(clienthost), clientservice, sizeof(clientservice), NI_NUMERICHOST|NI_NUMERICSERV);
        LOG(INFO) << "New incoming connection:" << clienthost << " -- " << clientservice;

        ThreadRUdpReceiver* receiverThread = new ThreadRUdpReceiver( mQueue, clientfd );
        receiverThread->start();
    }
}