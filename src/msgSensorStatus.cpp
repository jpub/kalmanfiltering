#include "byteBuffer.h"
#include "msgBase.h"
#include "msgSensorStatus.h"
#include <stdio.h>


QItem MsgSensorStatus::clone()
{
    MsgSensorStatus* msg = new MsgSensorStatus();
    if ( this->mRawBuffer )
        msg->mRawBuffer = new ByteBuffer( *this->mRawBuffer );
    msg->mHeader = this->mHeader;
    msg->mData = this->mData;

    return QItem( msg );
}

MsgSensorStatus::MsgSensorStatus()
{
    memset( &mData, 0, sizeof(mData) );
    mHeader.messageCode = ID_SENSOR_STATUS;
    mHeader.messageLength = getSize();
}

MsgSensorStatus::MsgSensorStatus( ByteBuffer* inBuffer )
    :MsgBase( inBuffer )
{
    int bytePosition = 0;
    unpack( bytePosition );
}

MsgSensorStatus::MsgSensorStatus( const MsgSensorStatus& rhs )
    :MsgBase( rhs )
{
    mData = rhs.mData;
}

MsgSensorStatus& MsgSensorStatus::operator=( const MsgSensorStatus& rhs )
{
    MsgBase::operator = (rhs);
    mData = rhs.mData;
    return *this;
}

int MsgSensorStatus::getSize()
{
    int totalSize = 0;
    totalSize += MsgBase::getSize();
    totalSize += sizeof( mData );
    return totalSize;
}

ByteBuffer* MsgSensorStatus::pack( int& bytePosition )
{
    MsgBase::pack( bytePosition );
    mRawBuffer->appendByteArray( (char*)&mData, sizeof(mData) );
    return mRawBuffer;
}

void MsgSensorStatus::unpack( int& bytePosition )
{
    MsgBase::unpack( bytePosition );
    mRawBuffer->popByteArray( (char*)&mData, sizeof(mData) );
}

int MsgSensorStatus::print()
{
    int charWritten = MsgBase::print();
    charWritten = snprintf( mPrintBuffer + charWritten, PRINT_BUFFER_SIZE,
        "PosX:%d PosY:%d PosZ:%d CommsMac:%02x%02x%02x%02x%02x%02x SrThr:%d MrThr:%d LrThr:%d",
        mData.positionX,
        mData.positionY,
        mData.positionZ,
        mData.commsAddress[0],
        mData.commsAddress[1],
        mData.commsAddress[2],
        mData.commsAddress[3],
        mData.commsAddress[4],
        mData.commsAddress[5],
        mData.apRssiThresholdSr,
        mData.apRssiThresholdMr,
        mData.apRssiLimit );

    LOG(DEBUG) << mPrintBuffer;
    return charWritten;
}
