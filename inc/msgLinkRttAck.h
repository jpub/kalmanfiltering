#ifndef __MSG_LINK_RTT_ACK_H
#define __MSG_LINK_RTT_ACK_H

#include "msgBase.h"

class ByteBuffer;

class MsgLinkRttAck : public MsgBase
{
public:
    ENABLE_EVENT_DISPATCH();

public:
    virtual QItem clone();

public:
    MsgLinkRttAck();
    MsgLinkRttAck( ByteBuffer* inBuffer );
    MsgLinkRttAck( const MsgLinkRttAck& rhs );
    MsgLinkRttAck& operator=( const MsgLinkRttAck& rhs );

    virtual int getSize();
    virtual ByteBuffer* pack( int& bytePosition );
    virtual void unpack( int& bytePosition );
    virtual int print();

    LINK_RTT_ACK* data()
    {
        return &mData;
    }
    
private:
    LINK_RTT_ACK mData;
};

#endif
