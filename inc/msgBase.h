#ifndef __MSG_BASE_H
#define __MSG_BASE_H

#include "__EventIds.h"
#include "__DefineVectorSize.h"
#include "__GenStructs.h"
#include "eventBase.h"
#include "byteBuffer.h"




class MsgBase : public EventBase
{
    // Important!! MsgBase::pack is not thread safe.
    // If the same item need to be put into different channel concurrently,
    // you must clone this message
public:
    virtual QItem clone() = 0;

public:
    MsgBase();
    MsgBase( ByteBuffer* inBuffer );
    virtual ~MsgBase();
    MsgBase( const MsgBase& rhs );
    MsgBase& operator=( const MsgBase& rhs );
    
    virtual int getSize();
    virtual ByteBuffer* pack( int& bytePosition );
    virtual void unpack( int& bytePosition );
    virtual int print();

    MSG_HEADER* header()
    {
        return &mHeader;
    }


protected:
    ByteBuffer* mRawBuffer;
    char mPrintBuffer[PRINT_BUFFER_SIZE];
    
protected:
    MSG_HEADER mHeader;
};

#endif
