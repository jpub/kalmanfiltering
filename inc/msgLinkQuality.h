#ifndef __MSG_LINK_QUALITY_H
#define __MSG_LINK_QUALITY_H

#include "msgBase.h"

class ByteBuffer;

class MsgLinkQuality : public MsgBase
{
public:
    ENABLE_EVENT_DISPATCH();

public:
    virtual QItem clone();

public:
    MsgLinkQuality();
    MsgLinkQuality( ByteBuffer* inBuffer );
    MsgLinkQuality( const MsgLinkQuality& rhs );
    MsgLinkQuality& operator=( const MsgLinkQuality& rhs );

    virtual int getSize();
    virtual ByteBuffer* pack( int& bytePosition );
    virtual void unpack( int& bytePosition );
    virtual int print();

    LINK_QUALITY* data()
    {
        return &mData;
    }
    
private:
    LINK_QUALITY mData;
};

#endif
