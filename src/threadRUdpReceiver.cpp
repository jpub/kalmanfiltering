#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "threadRUdpReceiver.h"
#include "msgQueue.h"
#include "logger.h"



ThreadRUdpReceiver::ThreadRUdpReceiver(
    MsgQueue<QItem>* incomingQueue,
    UDTSOCKET clientfd )
    : ThreadBaseReceiver(incomingQueue),
      mClientfd(clientfd)
{
}

void* ThreadRUdpReceiver::run( void* arg )
{
    char* nullArg = 0;
    char socketData[RECV_BUFFER_SIZE];

    while( true )
    {
        int bytesRecv = UDT::recvmsg( mClientfd, socketData, RECV_BUFFER_SIZE );

        if ( bytesRecv == UDT::ERROR )
        {
            LOG(ERROR) << "Client discconnected:" << UDT::getlasterror().getErrorMessage();
            LOG(ERROR) << "Current thread will be terminated";
            return nullArg;
        }
        else
        {
            if ( bytesRecv == 0 )
            {
                LOG(WARNING) << "Receive empty data..continue";
                continue;
            }

            if ( !mRingBuffer.write(socketData, bytesRecv) )
            {
                LOG(WARNING) << "Message not written to buffer";
                continue;
            }

            // process the data in the ring buffer
            // If this is too time consuming..may have to shift to another thread
            processRingBuffer();
        }
    }
}
