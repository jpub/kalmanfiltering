#ifndef __MSG_MLT_STATUS_H
#define __MSG_MLT_STATUS_H

#include "msgBase.h"

class ByteBuffer;

class MsgMltStatus : public MsgBase
{
public:
    ENABLE_EVENT_DISPATCH();

public:
    virtual QItem clone();

public:
    MsgMltStatus();
    MsgMltStatus( ByteBuffer* inBuffer );
    MsgMltStatus( const MsgMltStatus& rhs );
    MsgMltStatus& operator=( const MsgMltStatus& rhs );

    virtual int getSize();
    virtual ByteBuffer* pack( int& bytePosition );
    virtual void unpack( int& bytePosition );
    virtual int print();

    MLT_STATUS* data()
    {
        return &mData;
    }
    
private:
    MLT_STATUS mData;
};

#endif
