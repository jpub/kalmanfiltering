#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netdb.h>
#include "udt.h"

void* recvdata(void* usocket)
{
    UDTSOCKET recver = *(UDTSOCKET*)usocket;
    delete (UDTSOCKET*)usocket;

    char* data;
    int size = 100000;
    data = new char[size];
    int counter = 0;

    while( true )
    {
        memset( data, 0, size );
        int rcv_size = UDT::recvmsg( recver, data, size );
        if ( rcv_size == -1 )
        {
            printf( "size [%d] error %s\n", rcv_size, UDT::getlasterror().getErrorMessage() );
            break;
        }
        else
        {
            ++counter;
            printf( "%d -- size [%d] -- %s\n", counter, rcv_size, data );
            usleep( 500000 );
        }
    }
}




int main( int argc, char** argv )
{
    if ( argc != 2 )
    {
        printf( "usage: appserver [server_port]\n" );
        return 0;
    }

    addrinfo hints;
    addrinfo* res;
    
    memset(&hints, 0, sizeof(struct addrinfo));

    hints.ai_flags = AI_PASSIVE;
    hints.ai_family = AF_INET;
    //hints.ai_socktype = SOCK_STREAM;
    hints.ai_socktype = SOCK_DGRAM;

    char* service = argv[1];

    if (0 != getaddrinfo(NULL, service, &hints, &res))
    {
        printf( "illegal port number or port is busy.\n" );
        return 0;
    }

    UDTSOCKET serv = UDT::socket(res->ai_family, res->ai_socktype, res->ai_protocol);

    // UDT Options
    //UDT::setsockopt(serv, 0, UDT_CC, new CCCFactory<CUDPBlast>, sizeof(CCCFactory<CUDPBlast>));
    //UDT::setsockopt(serv, 0, UDT_MSS, new int(9000), sizeof(int));
    //UDT::setsockopt(serv, 0, UDT_RCVBUF, new int(10000000), sizeof(int));
    //UDT::setsockopt(serv, 0, UDP_RCVBUF, new int(10000000), sizeof(int));

    if (UDT::ERROR == UDT::bind(serv, res->ai_addr, res->ai_addrlen))
    {
        printf( "bind: %s\n", UDT::getlasterror().getErrorMessage() );
        return 0;
    }
    
    freeaddrinfo(res);

    printf( "server is ready at port: %s", service );

    if (UDT::ERROR == UDT::listen(serv, 10))
    {
        printf( "listen: %s\n", UDT::getlasterror().getErrorMessage() );
        return 0;
    }

    sockaddr_storage clientaddr;
    int addrlen = sizeof(clientaddr);

    UDTSOCKET recver;

    while (true)
    {
        if (UDT::INVALID_SOCK == (recver = UDT::accept(serv, (sockaddr*)&clientaddr, &addrlen)))
        {
            printf( "accept: %s\n", UDT::getlasterror().getErrorMessage() );
            return 0;
        }

        char clienthost[NI_MAXHOST];
        char clientservice[NI_MAXSERV];
        getnameinfo((sockaddr *)&clientaddr, addrlen, clienthost, sizeof(clienthost), clientservice, sizeof(clientservice), NI_NUMERICHOST|NI_NUMERICSERV);
        printf( "new connection: %s : %s\n", clienthost, clientservice );


        pthread_t rcvthread;
        pthread_create(&rcvthread, NULL, recvdata, new UDTSOCKET(recver));
        pthread_detach(rcvthread);
    }

    UDT::close(serv);

    return 0;
}


