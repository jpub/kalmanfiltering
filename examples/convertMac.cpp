#include <stdlib.h>
#include <stdio.h>
#include <map>
#include <string>

using namespace std;


int main( int argc, char** argv )
{
    if ( argc != 2 )
    {
        printf( "usage: ./convertMac <MAC_ADDRESS>\n" );
        return -1;
    }
    
    string actualMac = argv[1];
    printf( ">>>>>>>>> HEXA >>>>>>>>>>>\n" );

    for ( int i=0; i<6; ++i )
        printf( "%s\n", actualMac.substr(i*2,2).c_str() );

    printf( ">>>>>>>>> INT >>>>>>>>>>>\n" );

    unsigned char mac[6] = {0};
    char* pend;
    long int qqq = 0;
    for ( int i=0; i<6; ++i )
    {
        qqq = strtol( actualMac.substr(i*2,2).c_str(), &pend, 16 );
        mac[i] = (unsigned char)qqq;
        printf( "%d\n", mac[i] );
    }

    printf( ">>>>>>>>> LONG LONG >>>>>>>>>>>\n" );

    long long* xxx = 0;
    xxx = (long long*)mac;
    printf( "%lld\n", *xxx );

    printf( ">>>>>>>>> MAC >>>>>>>>>>>\n" );

    char tmpMacBuffer[20] = {0};
    snprintf( tmpMacBuffer, 13, "%02x%02x%02x%02x%02x%02x",
              mac[0],
              mac[1],
              mac[2],
              mac[3],
              mac[4],
              mac[5] );
    printf( "%s\n", tmpMacBuffer );

    return 0;
}
