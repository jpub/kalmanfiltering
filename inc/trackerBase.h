#ifndef __TRACKER_BASE_H
#define __TRACKER_BASE_H

#include "trackerInState.h"
#include "trackerOutResult.h"

class TrackerBase
{
public:
	virtual TrackerOutResult update( TrackerInState& inState ) = 0;
	virtual ~TrackerBase() {}

};

#endif