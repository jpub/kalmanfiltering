// sudo apt-get install librlog-dev

#include <rlog/rlog.h>
#include <rlog/StdioNode.h>
#include <rlog/RLogChannel.h>

int main( int argc, char** argv )
{
    rlog::RLogInit( argc, argv );
    int outFlag =
        rlog::StdioNode::DefaultOutput |
        rlog::StdioNode::OutputColor |
        rlog::StdioNode::OutputContext |
        rlog::StdioNode::OutputChannel;
    rlog::StdioNode stdLog( 2, outFlag );
    stdLog.subscribeTo( RLOG_CHANNEL ("debug") );
    stdLog.subscribeTo( RLOG_CHANNEL ("info") );
    stdLog.subscribeTo( RLOG_CHANNEL ("warning") );
    stdLog.subscribeTo( RLOG_CHANNEL ("error") );

    rDebug( "this is debug" );
    rInfo( "this is info" );
    rWarning( "this is warning %d", 100 );
    rError( "this is error %s", "more str" );

    return 0;
}

