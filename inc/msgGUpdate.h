#ifndef __MSG_G_UPDATE_H
#define __MSG_G_UPDATE_H

#include "msgBase.h"

class ByteBuffer;

class MsgGUpdate : public MsgBase
{
public:
    ENABLE_EVENT_DISPATCH();

public:
    virtual QItem clone();

public:
    MsgGUpdate();
    MsgGUpdate( ByteBuffer* inBuffer );
    MsgGUpdate( const MsgGUpdate& rhs );
    MsgGUpdate& operator=( const MsgGUpdate& rhs );

    virtual int getSize();
    virtual ByteBuffer* pack( int& bytePosition );
    virtual void unpack( int& bytePosition );
    virtual int print();

    G_UPDATE* data()
    {
        return &mData;
    }
    
private:
    G_UPDATE mData;
};

#endif
