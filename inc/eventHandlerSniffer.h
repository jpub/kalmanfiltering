#ifndef __EVENT_HANDLER_SNIFFER_H
#define __EVENT_HANDLER_SNIFFER_H

#include <map>
#include "eventHandlerBase.h"
#include "factoryMsg.h"
#include "allMessages.h"


class EventHandlerSniffer
	: public EventHandlerBase,
	  public EventHandler<MsgPeriodicTimer>,
	  public EventHandler<MsgSensorUpdate>,
	  public EventHandler<MsgDbgDeviceUpdate>,
	  public EventHandler<MsgDeviceLocation>,
	  public EventHandler<MsgSensorStatus>,
	  public EventHandler<MsgSendCounter>,
	  public EventHandler<MsgLinkQuality>,
	  public EventHandler<MsgDiscoveryAp>,
	  public EventHandler<MsgDiscoveryTrk>,
	  public EventHandler<MsgDiscoveryMon>,
	  public EventHandler<MsgMltStatus>,
	  public EventHandler<MsgMltStatusRequest>,
	  public EventHandler<MsgGUpdate>,
	  public EventHandler<MsgGUpdateAck>,
	  public EventHandler<MsgApUpdate>,
	  public EventHandler<MsgLinkRttRequest>
{
public:
	EventHandlerSniffer();
	virtual ~EventHandlerSniffer();

public:
	virtual bool handle( MsgPeriodicTimer& );
	virtual bool handle( MsgSensorUpdate& );
	virtual bool handle( MsgDbgDeviceUpdate& );
	virtual bool handle( MsgDeviceLocation& );
	virtual bool handle( MsgSensorStatus& );
	virtual bool handle( MsgSendCounter& );
	virtual bool handle( MsgLinkQuality& );
	virtual bool handle( MsgDiscoveryAp& );
	virtual bool handle( MsgDiscoveryTrk& );
	virtual bool handle( MsgDiscoveryMon& );
	virtual bool handle( MsgMltStatus& );
	virtual bool handle( MsgMltStatusRequest& );
	virtual bool handle( MsgGUpdate& );
	virtual bool handle( MsgGUpdateAck& );
	virtual bool handle( MsgApUpdate& );
	virtual bool handle( MsgLinkRttRequest& );

private:
	FactoryMsg mFactoryMsg;
	std::map<int,unsigned int> mRunningSrcCounter;
	std::map<int,unsigned int> mLastReceivedSrcCounter;

	int mCycleDiscovery;
	int mCycleApStatus;
	int mCycleSensorRegulatorMaintenance;
};

#endif