#ifndef __THREAD_MASTER_RECEIVER_H
#define __THREAD_MASTER_RECEIVER_H

#include "threadBase.h"
#include "eventHandlerBase.h"

class ThreadMasterReceiver : public ThreadBase
{
public:
	ThreadMasterReceiver( MsgQueue<QItem>* incomingQueue, EventHandlerBase* eventHandlerBase );
	virtual void* run( void* arg );

protected:
	EventHandlerBase* mEventHandlerBase;
};

#endif