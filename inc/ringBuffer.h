#ifndef __RING_BUFFER_H
#define __RING_BUFFER_H

#include <algorithm>

class RingBuffer
{
public:
    RingBuffer( size_t capacity );

    ~RingBuffer();
    
    size_t size() const
    {
        return mSize;
    }

    size_t capacity() const
    {
        return mCapacity;
    }

    bool write( const char* data, size_t len );
    bool read( char* data, size_t len );
    bool peek( char* data, size_t len );
    void print();
    
private:
    size_t mStartIndex, mEndIndex, mSize, mCapacity;
    char* mBuffer;
};

#endif
