#include "msgBase.h"
#include "byteBuffer.h"
#include "logger.h"
#include <stdio.h>


MsgBase::MsgBase()
{
    memset( &mHeader, 0, sizeof(mHeader) );
    mRawBuffer = 0;
}

MsgBase::MsgBase( ByteBuffer* inBuffer )
{
    mRawBuffer = inBuffer;
    // unlike C#, I cannot call virtual unpack here because the derived object is not contructed yet
    // calling unpack here will only invoke the Base::unpack even if it is virtual
}

MsgBase::~MsgBase()
{
    delete mRawBuffer;
    mRawBuffer = 0;
}

MsgBase::MsgBase( const MsgBase& rhs )
{
    mHeader = rhs.mHeader;
}

MsgBase& MsgBase::operator=( const MsgBase& rhs )
{
    mHeader = rhs.mHeader;
    return *this;
}

int MsgBase::getSize()
{
    return sizeof( mHeader );
}

ByteBuffer* MsgBase::pack( int& bytePosition )
{
    // just in case pack is called more than once
    if ( mRawBuffer )
        delete mRawBuffer;

    bytePosition = 0;
    int totalSize = getSize();
    mRawBuffer = new ByteBuffer( totalSize );

    mHeader.messageLength = totalSize;

    mRawBuffer->appendByteArray( (char*)&mHeader, sizeof(mHeader) );
    return mRawBuffer;
}

void MsgBase::unpack( int& bytePosition )
{
    mRawBuffer->popByteArray( (char*)&mHeader, sizeof(mHeader) );
}

int MsgBase::print()
{
    memset( mPrintBuffer, 0, PRINT_BUFFER_SIZE );

    int charWritten = sprintf( mPrintBuffer,
        "MsgCode:%d MsgLen:%d MsgTime:%d MsgNum:%d OrgSrcId:%d SrcId:%d DestId:%d ",
        mHeader.messageCode,
        mHeader.messageLength,
        mHeader.messageTime,
        mHeader.messageNumber,
        mHeader.orgSourceId,
        mHeader.sourceId,
        mHeader.destinationId );

    return charWritten;
}
