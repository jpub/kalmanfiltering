#include "byteBuffer.h"

ByteBuffer::ByteBuffer( int size )
{
    mBuffer = new char[size];
    memset( mBuffer, 0, size );
    mCurrentPointer = mBuffer;
    mBufferSize = size;
}

ByteBuffer::ByteBuffer( const ByteBuffer& rhs )
{
    mBuffer = new char[rhs.mBufferSize];
    mBufferSize = rhs.mBufferSize;
    mCurrentPointer = mBuffer;

    memcpy( mBuffer, rhs.mBuffer, mBufferSize );
}

ByteBuffer& ByteBuffer::operator=( const ByteBuffer& rhs )
{
    mBuffer = new char[rhs.mBufferSize];
    mBufferSize = rhs.mBufferSize;
    mCurrentPointer = mBuffer;

    memcpy( mBuffer, rhs.mBuffer, mBufferSize );
    return *this;
}

ByteBuffer::~ByteBuffer()
{
    delete []mBuffer;
    mBuffer = 0;
    mBufferSize = 0;
}


void ByteBuffer::resetPointer()
{
    mCurrentPointer = mBuffer;
}

char* ByteBuffer::getDataPtr()
{
    return mBuffer;
}

int ByteBuffer::getSize()
{
    return mBufferSize;
}



void ByteBuffer::appendByte( unsigned char data )
{
    append<unsigned char>( data );
}

void ByteBuffer::appendSByte( char data )
{
    append<char>( data );
}

void ByteBuffer::appendUShort( unsigned short data )
{
    append<unsigned short>( data );
}

void ByteBuffer::appendShort( short data )
{
    append<short>( data );
}

void ByteBuffer::appendUInt( unsigned int data )
{
    append<unsigned int>( data );
}

void ByteBuffer::appendInt( int data )
{
    append<int>( data );
}

void ByteBuffer::appendFloat( float data )
{
    append<float>( data );
}

void ByteBuffer::appendDouble( double data )
{
    append<double>( data );
}

void ByteBuffer::appendByteArray( char* data, int length )
{
    memcpy( mCurrentPointer, data, length );
    mCurrentPointer += length;
}





unsigned char ByteBuffer::popByte()
{
    return pop<unsigned char>();
}

char ByteBuffer::popSByte()
{
    return pop<char>();
}

unsigned short ByteBuffer::popUShort()
{
    return pop<unsigned short>();
}

short ByteBuffer::popShort()
{
    return pop<short>();
}

unsigned int ByteBuffer::popUInt()
{
    return pop<unsigned int>();
}

int ByteBuffer::popInt()
{
    return pop<int>();
}

float ByteBuffer::popFloat()
{
    return pop<float>();
}

double ByteBuffer::popDouble()
{
    return pop<double>();
}

void ByteBuffer::popByteArray( char* data, int length )
{
    memcpy( data, mCurrentPointer, length );
    mCurrentPointer += length;
}








int ByteBuffer::putByte( unsigned char data, int index )
{
    return put<unsigned char>( data, index );
}

int ByteBuffer::putSByte( char data, int index )
{
    return put<char>( data, index );
}

int ByteBuffer::putUShort( unsigned short data, int index )
{
    return put<unsigned short>( data, index );
}

int ByteBuffer::putShort( short data, int index )
{
    return put<short>( data, index );
}

int ByteBuffer::putUInt( unsigned int data, int index )
{
    return put<unsigned int>( data, index );
}

int ByteBuffer::putInt( int data, int index )
{
    return put<int>( data, index );
}

int ByteBuffer::putFloat( float data, int index )
{
    return put<float>( data, index );
}

int ByteBuffer::putDouble( double data, int index )
{
    return put<double>( data, index );
}

int ByteBuffer::putByteArray( char* data, int length, int index )
{
    memcpy( mBuffer+index, data, length );
    return index+length;
}






unsigned char ByteBuffer::getByte( int index )
{
    return get<unsigned char>( index );
}

char ByteBuffer::getSByte( int index )
{
    return get<char>( index );
}

unsigned short ByteBuffer::getUShort( int index )
{
    return get<unsigned short>( index );
}

short ByteBuffer::getShort( int index )
{
    return get<short>( index );
}

unsigned int ByteBuffer::getUInt( int index )
{
    return get<unsigned int>( index );
}

int ByteBuffer::getInt( int index )
{
    return get<int>( index );
}

float ByteBuffer::getFloat( int index )
{
    return get<float>( index );
}

double ByteBuffer::getDouble( int index )
{
    return get<double>( index );
}

void ByteBuffer::getByteArray( char* data, int length, int index )
{
    memcpy( data, mBuffer+index, length );
}
