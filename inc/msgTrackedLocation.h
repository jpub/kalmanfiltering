#ifndef __MSG_TRACKED_LOCATION_H
#define __MSG_TRACKED_LOCATION_H

#include "msgBase.h"

class ByteBuffer;

class MsgTrackedLocation : public MsgBase
{
public:
    ENABLE_EVENT_DISPATCH();

public:
    virtual QItem clone();

public:
    MsgTrackedLocation();
    MsgTrackedLocation( ByteBuffer* inBuffer );
    MsgTrackedLocation( const MsgTrackedLocation& rhs );
    MsgTrackedLocation& operator=( const MsgTrackedLocation& rhs );

    virtual int getSize();
    virtual ByteBuffer* pack( int& bytePosition );
    virtual void unpack( int& bytePosition );
    virtual int print();

    TRACKED_LOCATION* data()
    {
        return &mData;
    }
    
private:
    TRACKED_LOCATION mData;
};

#endif
