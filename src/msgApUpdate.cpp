#include "byteBuffer.h"
#include "msgBase.h"
#include "msgApUpdate.h"
#include <stdio.h>


QItem MsgApUpdate::clone()
{
    MsgApUpdate* msg = new MsgApUpdate();
    if ( this->mRawBuffer )
        msg->mRawBuffer = new ByteBuffer( *this->mRawBuffer );
    msg->mHeader = this->mHeader;
    msg->mData = this->mData;

    return QItem( msg );
}

MsgApUpdate::MsgApUpdate()
{
    memset( &mData, 0, sizeof(mData) );
    mHeader.messageCode = ID_AP_UPDATE;
    mHeader.messageLength = getSize();
}

MsgApUpdate::MsgApUpdate( ByteBuffer* inBuffer )
    :MsgBase( inBuffer )
{
    int bytePosition = 0;
    unpack( bytePosition );
}

MsgApUpdate::MsgApUpdate( const MsgApUpdate& rhs )
    :MsgBase( rhs )
{
    mData = rhs.mData;
}

MsgApUpdate& MsgApUpdate::operator=( const MsgApUpdate& rhs )
{
    MsgBase::operator = (rhs);
    mData = rhs.mData;
    return *this;
}

int MsgApUpdate::getSize()
{
    int totalSize = 0;
    totalSize += MsgBase::getSize();
    totalSize += sizeof( mData );
    return totalSize;
}

ByteBuffer* MsgApUpdate::pack( int& bytePosition )
{
    MsgBase::pack( bytePosition );
    mRawBuffer->appendByteArray( (char*)&mData, sizeof(mData) );
    return mRawBuffer;
}

void MsgApUpdate::unpack( int& bytePosition )
{
    MsgBase::unpack( bytePosition );
    mRawBuffer->popByteArray( (char*)&mData, sizeof(mData) );
}

int MsgApUpdate::print()
{
    int charWritten = MsgBase::print();

    charWritten = snprintf( mPrintBuffer + charWritten, PRINT_BUFFER_SIZE,
        "ApId:%d Bitmask:%d SrThr:%d MrThr:%d LrThr:%d R0:%d D0:%d PosX:%d PosY:%d PosZ:%d",
        mData.apId,
        mData.bitmask,
        mData.apRssiThresholdSr,
        mData.apRssiThresholdMr,
        mData.apRssiLimit,
        mData.r0,
        mData.d0,
        mData.positionX,
        mData.positionY,
        mData.positionZ );

    LOG(DEBUG) << mPrintBuffer;
    return charWritten;
}
