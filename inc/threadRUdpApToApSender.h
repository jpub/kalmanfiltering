#ifndef __THREAD_RUDP_AP_TO_AP_SEDNER_H
#define __THREAD_RUDP_AP_TO_AP_SEDNER_H

#include <netdb.h>
#include "factoryMsg.h"
#include "threadRUdpBaseSender.h"
#include "udt.h"


class ThreadRUdpApToApSender : public ThreadRUdpBaseSender
{
public:
    ThreadRUdpApToApSender( MsgQueue<QItem>* outgoingQueue,
    						int destinationId,
                            const char* destIp,
                            int destPort,
                            const char* localBindIp,
                            int localBindPort,
                            int ttl,
                            int linkQualityCounter );

    virtual void handleDisconnection();
    virtual void handleSuccessfulSending( MsgBase& );

protected:
	int mLinkQualityCounter;
};

#endif
