#include "byteBuffer.h"
#include "msgBase.h"
#include "msgGUpdate.h"
#include <stdio.h>


QItem MsgGUpdate::clone()
{
    MsgGUpdate* msg = new MsgGUpdate();
    if ( this->mRawBuffer )
        msg->mRawBuffer = new ByteBuffer( *this->mRawBuffer );
    msg->mHeader = this->mHeader;
    msg->mData = this->mData;

    return QItem( msg );
}

MsgGUpdate::MsgGUpdate()
{
    memset( &mData, 0, sizeof(mData) );
    mHeader.messageCode = ID_G_UPDATE;
    mHeader.messageLength = getSize();
}

MsgGUpdate::MsgGUpdate( ByteBuffer* inBuffer )
    :MsgBase( inBuffer )
{
    int bytePosition = 0;
    unpack( bytePosition );
}

MsgGUpdate::MsgGUpdate( const MsgGUpdate& rhs )
    :MsgBase( rhs )
{
    mData = rhs.mData;
}

MsgGUpdate& MsgGUpdate::operator=( const MsgGUpdate& rhs )
{
    MsgBase::operator = (rhs);
    mData = rhs.mData;
    return *this;
}

int MsgGUpdate::getSize()
{
    int totalSize = 0;
    totalSize += MsgBase::getSize();
    totalSize += mData.numberOfBytes;

    return totalSize;
}

ByteBuffer* MsgGUpdate::pack( int& bytePosition )
{
    MsgBase::pack( bytePosition );
    int variableSize = getSize() - sizeof(MSG_HEADER);
    mRawBuffer->appendByteArray( (char*)mData.payload, variableSize );
    return mRawBuffer;
}

void MsgGUpdate::unpack( int& bytePosition )
{
    MsgBase::unpack( bytePosition );
    int variableSize = mRawBuffer->getSize() - sizeof(MSG_HEADER);
    mData.numberOfBytes = variableSize;
    mRawBuffer->popByteArray( (char*)mData.payload, variableSize );
}

int MsgGUpdate::print()
{
    int charWritten = MsgBase::print();

    charWritten = snprintf( mPrintBuffer + charWritten, PRINT_BUFFER_SIZE,
        "PIPE_THROUGH_BYTES:%d",
        mData.numberOfBytes );

    LOG(DEBUG) << mPrintBuffer;
    return charWritten;
}
