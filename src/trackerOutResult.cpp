#include "trackerOutResult.h"

bool TrackerOutResult::getIsTrackedSuccess()
{
	return mIsTrackedSuccess;
}

string TrackerOutResult::getDeviceId()
{
	return mDeviceId;
}

double TrackerOutResult::getTrackerLongitude()
{
	return mLongitude;
}

double TrackerOutResult::getTrackerLatitude()
{
	return mLatitude;
}


void TrackerOutResult::setIsTrackedSuccess( bool isTrackedSuccess )
{
	mIsTrackedSuccess = isTrackedSuccess;
}

void TrackerOutResult::setDeviceId( string deviceId )
{
	mDeviceId = deviceId;
}

void TrackerOutResult::setTrackerLongitude( double longitude )
{
	mLongitude = longitude;
}

void TrackerOutResult::setTrackerLatitude( double latitude )
{
	mLatitude = latitude;
}
