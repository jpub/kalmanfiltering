#ifndef __THREAD_PCAP_H
#define __THREAD_PCAP_H

#include <pcap.h>
#include <map>
#include "mutexLocker.h"
#include "__DefineVectorSize.h"
#include "threadBase.h"

using std::map;
using std::string;

class ThreadPcap : public ThreadBase
{
public:
	ThreadPcap( MsgQueue<QItem>* outgoingQueue, const char* dev, const char* srcMac, int srcAid, map<string,string> ignoreMap, int sensorRegulatorSec );
	~ThreadPcap();
	virtual void* run( void* arg );

protected:
	const char* mDev;
	pcap_t* mHandler;
	struct pcap_stat mPs;
	char mSrcMac[__ARRAY_12_LENGTH];
	int mSrcAid;

/****************************/
/*		PCAP workings		*/
/****************************/
public:
	static void PcapCallbackIncomingPackets(
		unsigned char* args,
		const struct pcap_pkthdr* pkthdr,
		const unsigned char* packet );

	static void Maintainance();

private:
	static bool CheckIgnoreMap( string deviceId );
	static bool CheckReportingTime( string deviceId, time_t updateTime );

protected:
	static pthread_mutex_t msMutex;
	static bool msSimulation;
	static int msSensorRegulatorSec;
	static map<string, string> msIgnoreMap; 
	static map<string, time_t> msRegulatorMap;
	static map<string, time_t>::iterator msIt;
};

#endif