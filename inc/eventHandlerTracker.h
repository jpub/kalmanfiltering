#ifndef __EVENT_HANDLER_TRACKER_H
#define __EVENT_HANDLER_TRACKER_H

#include <map>
#include "eventHandlerBase.h"
#include "factoryMsg.h"
#include "allMessages.h"


class EventHandlerTracker
	: public EventHandlerBase,
	  public EventHandler<MsgPeriodicTimer>,
	  public EventHandler<MsgDbgDeviceUpdate>,
	  public EventHandler<MsgDeviceLocation>,
	  public EventHandler<MsgSensorStatus>,
	  public EventHandler<MsgLinkQuality>,
	  public EventHandler<MsgDiscoveryAp>,
	  public EventHandler<MsgDiscoveryTrk>,
	  public EventHandler<MsgDiscoveryMon>,
	  public EventHandler<MsgApUpdate>,
	  public EventHandler<MsgApUpdateAck>,
	  public EventHandler<MsgLinkRttAck>
{
public:
	EventHandlerTracker();
	virtual ~EventHandlerTracker();

public:
	virtual bool handle( MsgPeriodicTimer& );
	virtual bool handle( MsgDbgDeviceUpdate& );
	virtual bool handle( MsgDeviceLocation& );
	virtual bool handle( MsgSensorStatus& );
	virtual bool handle( MsgLinkQuality& );
	virtual bool handle( MsgDiscoveryAp& );
	virtual bool handle( MsgDiscoveryTrk& );
	virtual bool handle( MsgDiscoveryMon& );
	virtual bool handle( MsgApUpdate& );
	virtual bool handle( MsgApUpdateAck& );
	virtual bool handle( MsgLinkRttAck& );

private:
	FactoryMsg mFactoryMsg;
	int mCycleDiscovery;
	int mCycleLinkLatency;
	int mCycleTrackMaintenance;
	int mCycleAreaStatus;
};

#endif