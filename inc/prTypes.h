#ifndef __PR_TYPES_H
#define __PR_TYPES_H

typedef signed char SBYTE;
typedef unsigned char BYTE;
typedef unsigned short UINT16;
typedef short INT16;
typedef unsigned int UINT32;
typedef int INT32;
typedef float FLOAT;
typedef double DOUBLE;
typedef long long unsigned int UINT64;
typedef long long int INT64;

#endif
