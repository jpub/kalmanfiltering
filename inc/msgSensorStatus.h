#ifndef __MSG_SENSOR_STATUS_H
#define __MSG_SENSOR_STATUS_H

#include "msgBase.h"

class ByteBuffer;

class MsgSensorStatus : public MsgBase
{
public:
    ENABLE_EVENT_DISPATCH();

public:
    virtual QItem clone();

public:
    MsgSensorStatus();
    MsgSensorStatus( ByteBuffer* inBuffer );
    MsgSensorStatus( const MsgSensorStatus& rhs );
    MsgSensorStatus& operator=( const MsgSensorStatus& rhs );

    virtual int getSize();
    virtual ByteBuffer* pack( int& bytePosition );
    virtual void unpack( int& bytePosition );
    virtual int print();

    SENSOR_STATUS* data()
    {
        return &mData;
    }
    
private:
    SENSOR_STATUS mData;
};

#endif
