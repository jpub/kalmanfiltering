#include <stdio.h>
#include <string.h>
#include "ringBuffer.h"


RingBuffer::RingBuffer( size_t capacity )
    : mStartIndex(0),
      mEndIndex(0),
      mSize(0),
      mCapacity(capacity)
{
    mBuffer = new char[capacity];
    memset( mBuffer, 0, capacity );
}

RingBuffer::~RingBuffer()
{
    delete []mBuffer;
}

bool RingBuffer::write( const char* data, size_t len )
{
    if ( len == 0 )
        return false;

    size_t spaceLeft = mCapacity - mSize;
    if ( len > spaceLeft )
        return false;

    if ( len <= mCapacity - mEndIndex )
    {
        memcpy( mBuffer+mEndIndex, data, len );
        mEndIndex += len;
        if ( mEndIndex == mCapacity )
            mEndIndex = 0;
    }
    else
    {
        size_t fragment1 = mCapacity - mEndIndex;
        memcpy( mBuffer+mEndIndex, data, fragment1 );
        size_t fragment2 = len - fragment1;
        memcpy( mBuffer, data+fragment1, fragment2 );
        mEndIndex = fragment2;
    }

    mSize += len;
    return true;
}

bool RingBuffer::read( char* data, size_t len )
{
    if ( len == 0 || len > mSize )
        return false;

    if ( len <= mCapacity - mStartIndex )
    {
        memcpy( data, mBuffer+mStartIndex, len );
        mStartIndex += len;
        if ( mStartIndex == mCapacity )
            mStartIndex = 0;
    }
    else
    {
        size_t fragment1 = mCapacity - mStartIndex;
        memcpy( data, mBuffer+mStartIndex, fragment1 );
        size_t fragment2 = len - fragment1;
        memcpy( data+fragment1, mBuffer, fragment2 );
        mStartIndex = fragment2;
    }

    mSize -= len;
    return true;
}

bool RingBuffer::peek( char* data, size_t len )
{
    if ( len == 0 || len > mSize )
        return false;

    if ( len <= mCapacity - mStartIndex )
    {
        memcpy( data, mBuffer+mStartIndex, len );
    }
    else
    {
        size_t fragment1 = mCapacity - mStartIndex;
        memcpy( data, mBuffer+mStartIndex, fragment1 );
        size_t fragment2 = len - fragment1;
        memcpy( data+fragment1, mBuffer, fragment2 );
    }

    return true;
}

void RingBuffer::print()
{
    for ( unsigned int i=0; i<mCapacity; ++i )
    {
        printf( "%d -- %d\n", i, mBuffer[i] );
    }
}
