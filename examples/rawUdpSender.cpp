#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>

#include "__GenStructs.h"
#include "__EventIds.h"

int main( int argc, char** argv )
{
    int sockfd;
    struct sockaddr_in destAddr;
    int slen = sizeof( destAddr );

    if ( argc != 5 )
    {
        printf( "usage: ./sender <IP> <PORT> <TOTAL_MSG> <DELAY_MICRO>\n" );
        return -1;
    }

    char* destinationIP = argv[1];
    int destinationPort = atoi( argv[2] );
    int totalMsg = atoi( argv[3] );
    int delayMicro = atoi( argv[4] );

    struct addrinfo hints, *local;
    memset(&hints, 0, sizeof(struct addrinfo));    
    hints.ai_flags = AI_PASSIVE;
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_DGRAM;

    if ( getaddrinfo(NULL, "0", &hints, &local) != 0 )
    {
        printf( "unable to get local address\n" );
        return -1;
    }

    sockfd = socket( local->ai_family, local->ai_socktype, local->ai_protocol );
    if ( sockfd == -1 )
    {
        printf( "unable to create socket\n" );
        return -1;
    }


    // if want to do broadcast
    int broadcast = 1;
    setsockopt( sockfd, SOL_SOCKET, SO_BROADCAST, &broadcast, sizeof(broadcast) );
    memset( (char*)&destAddr, 0, sizeof(destAddr) );
    destAddr.sin_family = AF_INET;
    destAddr.sin_port = htons( destinationPort );
    if ( inet_aton(destinationIP, &destAddr.sin_addr) == 0 )
    {
        printf( "inet_aton fail\n" );
        return -1;
    }

    SENSOR_UPDATE_MSG msg;
    msg.messageHeader.messageCode = ID_SENSOR_UPDATE;
    msg.messageHeader.messageLength = sizeof( SENSOR_UPDATE_MSG );
    
    for ( int i=0; i<totalMsg; ++i )
    {
        msg.messageHeader.messageNumber = i;
        
        int bytesSend = sendto(
            sockfd, (char*)&msg, sizeof(SENSOR_UPDATE_MSG), 0, (struct sockaddr*)&destAddr, slen );
        
        printf( "%d sending len [%d]\n", i, bytesSend );
        usleep( delayMicro );
    }
    
    return 0;
}
