#include "byteBuffer.h"
#include "msgBase.h"
#include "msgMltStatusRequest.h"
#include <stdio.h>


QItem MsgMltStatusRequest::clone()
{
    MsgMltStatusRequest* msg = new MsgMltStatusRequest();
    if ( this->mRawBuffer )
        msg->mRawBuffer = new ByteBuffer( *this->mRawBuffer );
    msg->mHeader = this->mHeader;
    msg->mData = this->mData;

    return QItem( msg );
}

MsgMltStatusRequest::MsgMltStatusRequest()
{
    memset( &mData, 0, sizeof(mData) );
    mHeader.messageCode = ID_MLT_STATUS_REQUEST;
    mHeader.messageLength = getSize();
}

MsgMltStatusRequest::MsgMltStatusRequest( ByteBuffer* inBuffer )
    :MsgBase( inBuffer )
{
    int bytePosition = 0;
    unpack( bytePosition );
}

MsgMltStatusRequest::MsgMltStatusRequest( const MsgMltStatusRequest& rhs )
    :MsgBase( rhs )
{
    mData = rhs.mData;
}

MsgMltStatusRequest& MsgMltStatusRequest::operator=( const MsgMltStatusRequest& rhs )
{
    MsgBase::operator = (rhs);
    mData = rhs.mData;
    return *this;
}

int MsgMltStatusRequest::getSize()
{
    int totalSize = 0;
    totalSize += MsgBase::getSize();
    totalSize += mData.numberOfBytes;

    return totalSize;
}

ByteBuffer* MsgMltStatusRequest::pack( int& bytePosition )
{
    MsgBase::pack( bytePosition );
    int variableSize = getSize() - sizeof(MSG_HEADER);
    mRawBuffer->appendByteArray( (char*)mData.payload, variableSize );
    return mRawBuffer;
}

void MsgMltStatusRequest::unpack( int& bytePosition )
{
    MsgBase::unpack( bytePosition );
    int variableSize = mRawBuffer->getSize() - sizeof(MSG_HEADER);
    mData.numberOfBytes = variableSize;
    mRawBuffer->popByteArray( (char*)mData.payload, variableSize );
}

int MsgMltStatusRequest::print()
{
    int charWritten = MsgBase::print();

    charWritten = snprintf( mPrintBuffer + charWritten, PRINT_BUFFER_SIZE,
        "PIPE_THROUGH_BYTES:%d",
        mData.numberOfBytes );

    LOG(DEBUG) << mPrintBuffer;
    return charWritten;
}
