#ifndef __SMART_POINTER_H
#define __SMART_POINTER_H

#include "mutexLocker.h"

template <typename T>
class SmartPointer
{
        // Internal class for reference counting
private:
    
    class RefC
    {

    public:
        // RefC METHODS
        RefC() :
            mCount(0)
        {
            pthread_mutex_init( &mMutex, NULL );
        }

        ~RefC()
        {
            pthread_mutex_destroy( &mMutex );
        }
        
        void attach()
        {
            MutexLocker mutexLock( &mMutex );                    
            ++mCount;
        }
        
        int detach()
        {
            MutexLocker mutexLock( &mMutex );    
            return --mCount;
        }
        
        int count()
        {
            MutexLocker mutexLock( &mMutex );    
            return mCount;
        }
        
    private:
        // RefC ATTRIBUTES
        int mCount;
        pthread_mutex_t mMutex;
        
    }; // end RefC class
    
public:
    // Default Constructor to be used in STL map
    SmartPointer()
    {
        mData = 0;
        mRefC = new RefC();
        mRefC->attach();
    }
    
    // Prevent implicit conversion
    explicit SmartPointer( T* value )
    {
        mData = value;
        mRefC = new RefC();
        mRefC->attach();
    }
    
    // Only to be used for changing underlying data
    T* getDataPointer()
    {
        return mData;
    }
    
    // Copy Constructor
    SmartPointer( const SmartPointer<T>& rhs )
    {
        mData = rhs.mData;
        mRefC = rhs.mRefC;
        mRefC->attach();
    }
    
    // Assignment Operator
    SmartPointer<T>& operator=( const SmartPointer<T>& rhs )
    {
        if ( this != &rhs )
        {
            if ( mRefC->detach() == 0 )
            {
                if ( mData )
                {
                    delete mData;
                    mData = 0;
                }
                
                delete mRefC;
                mRefC = 0;
            }
            
            mData = rhs.mData;
            mRefC = rhs.mRefC;
            mRefC->attach();
        }
        return *this;
    }
    
    T& operator*()
    {
        return *mData;
    }
    
    T* operator->()
    {
        return mData;
    }
    
    // Destructor
    ~SmartPointer()
    {
        if ( mRefC->detach() == 0 )
        {
            if ( mData )
            {
                delete mData;
                mData = 0;
            }
            
            delete mRefC;
            mRefC = 0;
        }
    }
    
private:
    T* mData;
    RefC* mRefC;


};
#endif

