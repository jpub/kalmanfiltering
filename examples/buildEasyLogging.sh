g++ \
-DRLOG_COMPONENT="logtest" -DELPP_THREAD_SAFE -DELPP_NO_DEFAULT_LOG_FILE \
-std=c++11 -Wunused-variable -Wall \
mainEasyLogging.cpp \
-lpthread \
-o mainEasyLogging
