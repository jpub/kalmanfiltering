#include "byteBuffer.h"
#include "msgBase.h"
#include "msgDiscoveryTrk.h"
#include <stdio.h>


QItem MsgDiscoveryTrk::clone()
{
    MsgDiscoveryTrk* msg = new MsgDiscoveryTrk();
    if ( this->mRawBuffer )
        msg->mRawBuffer = new ByteBuffer( *this->mRawBuffer );
    msg->mHeader = this->mHeader;
    msg->mData = this->mData;

    return QItem( msg );
}

MsgDiscoveryTrk::MsgDiscoveryTrk()
{
    memset( &mData, 0, sizeof(mData) );
    mHeader.messageCode = ID_DISCOVERY_TRK;
    mHeader.messageLength = getSize();
}

MsgDiscoveryTrk::MsgDiscoveryTrk( ByteBuffer* inBuffer )
    :MsgBase( inBuffer )
{
    int bytePosition = 0;
    unpack( bytePosition );
}

MsgDiscoveryTrk::MsgDiscoveryTrk( const MsgDiscoveryTrk& rhs )
    :MsgBase( rhs )
{
    mData = rhs.mData;
}

MsgDiscoveryTrk& MsgDiscoveryTrk::operator=( const MsgDiscoveryTrk& rhs )
{
    MsgBase::operator = (rhs);
    mData = rhs.mData;
    return *this;
}

int MsgDiscoveryTrk::getSize()
{
    int totalSize = 0;
    totalSize += MsgBase::getSize();
    totalSize += sizeof( mData );
    return totalSize;
}

ByteBuffer* MsgDiscoveryTrk::pack( int& bytePosition )
{
    MsgBase::pack( bytePosition );
    mRawBuffer->appendByteArray( (char*)&mData, sizeof(mData) );
    return mRawBuffer;
}

void MsgDiscoveryTrk::unpack( int& bytePosition )
{
    MsgBase::unpack( bytePosition );
    mRawBuffer->popByteArray( (char*)&mData, sizeof(mData) );
}

int MsgDiscoveryTrk::print()
{
    int charWritten = MsgBase::print();

    charWritten = snprintf( mPrintBuffer + charWritten, PRINT_BUFFER_SIZE,
        "SrcIP:%.*s",
        __ARRAY_20_LENGTH, mData.srcAddress );

    LOG(DEBUG) << mPrintBuffer;
    return charWritten;
}