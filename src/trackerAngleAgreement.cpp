#include <math.h>
#include "logger.h"
#include "trackerAngleAgreement.h"

TrackerAngleAgreement::TrackerAngleAgreement(
	bool activateAngleTracker,
	bool activateDistanceTracker,
	double maxAngleAgreement,
	double humanWalkSpeed )
	: mActivateAngleTracker(activateAngleTracker),
	  mActivateDistanceTracker(activateDistanceTracker),
	  mMaxAngleAgreement(maxAngleAgreement),
	  mHumanWalkSpeed(humanWalkSpeed)
{

}

TrackerOutResult TrackerAngleAgreement::update( TrackerInState& inState )
{
	TrackerOutResult outResult;

	string deviceId = inState.getDeviceId();
	outResult.setDeviceId( deviceId );

	if ( !(mActivateAngleTracker | mActivateDistanceTracker) )
	{
		outResult.setIsTrackedSuccess( true );
		outResult.setTrackerLongitude( inState.getDetectedLongitude() );
		outResult.setTrackerLatitude( inState.getDetectedLatitude() );
		return outResult;
	}

	addPointHistory( deviceId, inState.getDetectedLongitude(), inState.getDetectedLatitude() );
	int totalPoints = mDeviceAngleCheckerMap[deviceId].size();
	int timeLapsed = inState.getSecondsSinceTrackerLastUpdate();

	// send out first upon detection to remove laggy effect
	if ( totalPoints == 1 )
	{
		outResult.setIsTrackedSuccess( true );
		outResult.setTrackerLongitude( inState.getDetectedLongitude() );
		outResult.setTrackerLatitude( inState.getDetectedLatitude() );
		return outResult;
	}

	if ( totalPoints == 2 )
	{
		if ( mActivateAngleTracker )
		{
			outResult.setIsTrackedSuccess( false );
			return outResult;
		}
		else
		{
			if ( mActivateDistanceTracker )
			{
				updateDevice( mDeviceAngleCheckerMap[deviceId][1], timeLapsed, outResult );
				outResult.setIsTrackedSuccess( true );
				return outResult;
			}
		}
	}

	if ( totalPoints >= 3 )
	{
		POINTF_2D anchorPoint = mDeviceAngleCheckerMap[deviceId][0];
		POINTF_2D latestPoint = mDeviceAngleCheckerMap[deviceId][totalPoints-1];

		for ( int i=1; i<= totalPoints-2; ++i )
		{
			POINTF_2D intermediateCheckPoint = mDeviceAngleCheckerMap[deviceId][i];
			if ( checkAgreement(anchorPoint, intermediateCheckPoint, latestPoint, timeLapsed, outResult) )
			{
				outResult.setIsTrackedSuccess( true );
				return outResult;
			}
		}
	}

	outResult.setIsTrackedSuccess( false );
	return outResult;
}

void TrackerAngleAgreement::addPointHistory( string deviceId, double x, double y )
{
	POINTF_2D pt;
	pt.x = x;
	pt.y = y;

	if ( mDeviceAngleCheckerMap.find(deviceId) != mDeviceAngleCheckerMap.end() )
	{
		mDeviceAngleCheckerMap[deviceId].push_back( pt );
	}
	else
	{
		vector<POINTF_2D> ptlist;
		ptlist.push_back( pt );
		mDeviceAngleCheckerMap[deviceId] = ptlist;
	}
}

double TrackerAngleAgreement::getAngle( POINTF_2D anchorPoint, POINTF_2D pointA, POINTF_2D pointB )
{
	POINTF_2D vecA;
	vecA.x = pointA.x - anchorPoint.x;
	vecA.y = pointA.y - anchorPoint.y;

	POINTF_2D vecB;
	vecB.x = pointB.x - anchorPoint.x;
	vecB.y = pointB.y - anchorPoint.y;

	double lenA = sqrt( vecA.x * vecA.x + vecA.y * vecA.y );
	double lenB = sqrt( vecB.x * vecB.x + vecB.y * vecB.y );

	if ( lenA * lenB == 0 )
	{
		return 0;
	}

	double aDotB = vecA.x * vecB.x + vecA.y * vecB.y;
	double cosTheta = aDotB / ( lenA * lenB );
	return acos( cosTheta );
}

bool TrackerAngleAgreement::checkAgreement( POINTF_2D anchorPoint, POINTF_2D point1, POINTF_2D point2, int timeLapsed, TrackerOutResult& outResult )
{
	double angle = getAngle( anchorPoint, point1, point2 );
	if ( angle < mMaxAngleAgreement )
	{
		updateDevice( point2, timeLapsed, outResult );
		return true;
	}
	return false;
}

void TrackerAngleAgreement::updateDevice( POINTF_2D selectedPoint, int timeLapsed, TrackerOutResult& outResult )
{
	string deviceId = outResult.getDeviceId();
	POINTF_2D anchorPoint = mDeviceAngleCheckerMap[deviceId][0];
	
	POINTF_2D vecRawFromAnchor;
	vecRawFromAnchor.x = selectedPoint.x - anchorPoint.x;
	vecRawFromAnchor.y = selectedPoint.y - anchorPoint.y;
	double lenRaw = sqrt( vecRawFromAnchor.x * vecRawFromAnchor.x + vecRawFromAnchor.y * vecRawFromAnchor.y );
	
	POINTF_2D unitVecRaw;
	unitVecRaw.x = vecRawFromAnchor.x / lenRaw;
	unitVecRaw.y = vecRawFromAnchor.y / lenRaw;

	double maximumAllowableDistanceFromAnchor = timeLapsed * mHumanWalkSpeed;

	bool isOvershoot = lenRaw > maximumAllowableDistanceFromAnchor ? true : false;
	POINTF_2D timeAgreementPoint;
	timeAgreementPoint.x = 0;
	timeAgreementPoint.y = 0;

	if ( isOvershoot )
	{
		POINTF_2D dampVecRawFromOrigin;
		dampVecRawFromOrigin.x = maximumAllowableDistanceFromAnchor * unitVecRaw.x;
		dampVecRawFromOrigin.y = maximumAllowableDistanceFromAnchor * unitVecRaw.y;

		// shift the vector to anchor point
		timeAgreementPoint.x = dampVecRawFromOrigin.x + anchorPoint.x;
		timeAgreementPoint.y = dampVecRawFromOrigin.y + anchorPoint.y;
	}
	else
	{
		timeAgreementPoint.x = selectedPoint.x;
		timeAgreementPoint.y = selectedPoint.y;
	}

	outResult.setTrackerLongitude( timeAgreementPoint.x );
	outResult.setTrackerLatitude( timeAgreementPoint.y );
	mDeviceAngleCheckerMap[deviceId].clear();
	mDeviceAngleCheckerMap[deviceId].push_back( timeAgreementPoint );
}

