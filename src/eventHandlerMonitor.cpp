#include "eventHandlerMonitor.h"
#include "mgrConfigStatic.h"
#include "mgrReporter.h"
#include "mgrPeriodic.h"
#include "logger.h"

using namespace std;

EventHandlerMonitor::EventHandlerMonitor()
{
	mCycleDiscovery = MgrConfigStatic::getInstance().getCycleDiscovery();	
}

EventHandlerMonitor::~EventHandlerMonitor()
{
}

bool EventHandlerMonitor::handle( MsgPeriodicTimer& msg )
{
	LOG(DEBUG) << "handle timer update";

	unsigned int timerCounter = msg.data()->timerCounter;

	if ( timerCounter % mCycleDiscovery == 0 )
		MgrPeriodic::getInstance().sendDiscoveryMon();

	return true;
}

bool EventHandlerMonitor::handle( MsgSensorUpdate& msg )
{
	return defaultHandling( msg );
}

bool EventHandlerMonitor::handle( MsgDbgDeviceUpdate& msg )
{
	return defaultHandling( msg );
}

bool EventHandlerMonitor::handle( MsgDeviceLocation& msg )
{
	return defaultHandling( msg );
}

bool EventHandlerMonitor::handle( MsgSensorStatus& msg )
{
	return defaultHandling( msg );
}

bool EventHandlerMonitor::handle( MsgSendCounter& msg )
{
	return defaultHandling( msg );
}

bool EventHandlerMonitor::handle( MsgLinkQuality& msg )
{
	return defaultHandling( msg );
}

bool EventHandlerMonitor::handle( MsgDiscoveryAp& msg )
{
	return defaultHandling( msg );
}

bool EventHandlerMonitor::handle( MsgDiscoveryTrk& msg )
{
	return defaultHandling( msg );
}

bool EventHandlerMonitor::handle( MsgDiscoveryMon& msg )
{
	return defaultHandling( msg );
}

bool EventHandlerMonitor::handle( MsgMltStatus& msg )
{
	return defaultHandling( msg );
}

bool EventHandlerMonitor::handle( MsgMltStatusRequest& msg )
{
	return defaultHandling( msg );
}

bool EventHandlerMonitor::handle( MsgGUpdate& msg )
{
	return defaultHandling( msg );
}

bool EventHandlerMonitor::handle( MsgGUpdateAck& msg )
{
	return defaultHandling( msg );
}

bool EventHandlerMonitor::handle( MsgApUpdate& msg )
{
	return defaultHandling( msg );
}

bool EventHandlerMonitor::handle( MsgApUpdateAck& msg )
{
	return defaultHandling( msg );
}

bool EventHandlerMonitor::handle( MsgLinkRttRequest& msg )
{
	return defaultHandling( msg );
}

bool EventHandlerMonitor::handle( MsgLinkRttAck& msg )
{
	return defaultHandling( msg );
}

bool EventHandlerMonitor::handle( MsgLinkRtt& msg )
{
	return defaultHandling( msg );
}

bool EventHandlerMonitor::handle( MsgSensorRegulatorUpdate& msg )
{
	return defaultHandling( msg );
}

bool EventHandlerMonitor::handle( MsgTrackedLocation& msg )
{
	return defaultHandling( msg );
}

bool EventHandlerMonitor::defaultHandling( MsgBase& msg )
{
	msg.print();

	// send to EXF
	MgrReporter::getInstance().sendToExf( msg.clone() );
	
	return true;
}