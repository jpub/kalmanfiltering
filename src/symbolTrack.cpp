#include "symbolTrack.h"

string SymbolTrack::getDeviceId()
{
	return mDeviceId;
}

double SymbolTrack::getLongitude()
{
	return mLongitude;
}

double SymbolTrack::getLatitude()
{
	return mLatitude;
}

unsigned int SymbolTrack::getLastUpdateTime()
{
	return mLastUpdateTime;
}




void SymbolTrack::setDeviceId( string deviceId )
{
	mDeviceId = deviceId;
}

void SymbolTrack::setLongitude( double longitude )
{
	mLongitude = longitude;
}

void SymbolTrack::setLatitude( double latitude )
{
	mLatitude = latitude;
}

void SymbolTrack::setLastUpdateTime( unsigned int lastUpdateTime )
{
	mLastUpdateTime = lastUpdateTime;
}
