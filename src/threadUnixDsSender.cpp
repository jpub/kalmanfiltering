#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>

#include "byteBuffer.h"
#include "threadUnixDsSender.h"
#include "msgQueue.h"
#include "mgrReporter.h"
#include "logger.h"


using std::string;

ThreadUnixDsSender::ThreadUnixDsSender(
    MsgQueue<QItem>* incomingQueue,
    int destinationId,
    string remotePath )
    : ThreadBaseSender(incomingQueue, destinationId),
      mRemotePath(remotePath)
{
    mSockfd = socket( AF_UNIX, SOCK_DGRAM, 0 );
    if ( mSockfd == -1 )
    {
        LOG(FATAL) << "Create domain socket fail. Error:" << errno;
    }

    mRemoteAddr.sun_family = AF_UNIX;
    strcpy( mRemoteAddr.sun_path, mRemotePath.c_str() );
}

bool ThreadUnixDsSender::sendMessage( MsgBase& msg )
{
    int position = 0;
    ByteBuffer* buffer = msg.pack( position );

    int totalSend = 0;
    int totalLength = buffer->getSize();
    int bytesLeft = totalLength;
    int bytesSend;
    char* data = buffer->getDataPtr();

    while ( totalSend < totalLength )
    {
        bytesSend =
            sendto(
                mSockfd,
                data+totalSend,
                bytesLeft,
                0,
                (struct sockaddr*)&mRemoteAddr,
                sizeof(sockaddr_un) );

        if ( bytesSend == -1 )
        {
            LOG(WARNING) << "Send to UDS fail";
            break;
        }

        totalSend += bytesSend;
        bytesLeft -= bytesSend;        
    }

    return true;
}

void ThreadUnixDsSender::handleSuccessfulSending( MsgBase& msg )
{
    if ( MgrReporter::getInstance().isMonRudpConnected() )
        MgrReporter::getInstance().rudpUnicastToMon( msg.clone() );
}
