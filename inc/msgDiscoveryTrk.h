#ifndef __MSG_DISCOVERY_TRK_H
#define __MSG_DISCOVERY_TRK_H

#include "msgBase.h"

class ByteBuffer;

class MsgDiscoveryTrk : public MsgBase
{
public:
    ENABLE_EVENT_DISPATCH();

public:
    virtual QItem clone();

public:
    MsgDiscoveryTrk();
    MsgDiscoveryTrk( ByteBuffer* inBuffer );
    MsgDiscoveryTrk( const MsgDiscoveryTrk& rhs );
    MsgDiscoveryTrk& operator=( const MsgDiscoveryTrk& rhs );

    virtual int getSize();
    virtual ByteBuffer* pack( int& bytePosition );
    virtual void unpack( int& bytePosition );
    virtual int print();

    DISCOVERY_TRK* data()
    {
        return &mData;
    }
    
private:
    DISCOVERY_TRK mData;
};

#endif
