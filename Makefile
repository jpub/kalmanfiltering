#Specify source file extension
SRCEXT = cpp
#Specify directory where the source files are residing
SRCDIR = src

APP_TRACKER = tracker
APP_MONITOR = monitor
APP_SNIFFER = sniffer

ifeq ($(target),pi)
	CC = arm-linux-gnueabihf-g++-4.8 -I./inc
	LIBDIR = ./lib_pi
	BINDIR = bin_pi
	OBJDIR = obj_pi
else
	CC = g++ -I./inc
	LIBDIR = ./lib
	BINDIR = bin
	OBJDIR = obj
endif


#Find all the source files name
TRACKER_SRC = $(shell find $(SRCDIR) -name '*.$(SRCEXT)' | grep -vE '$(APP_SNIFFER).cpp|$(APP_MONITOR).cpp')
#Replace all the .cpp extension to .o extension as objects
TRACKER_OBJ = $(patsubst %.$(SRCEXT),%.o,$(TRACKER_SRC))
TRACKER_OBJS = $(patsubst $(SRCDIR)/%,$(OBJDIR)/%,$(TRACKER_OBJ))

#Find all the source files name
MONITOR_SRC = $(shell find $(SRCDIR) -name '*.$(SRCEXT)' | grep -vE '$(APP_SNIFFER).cpp|$(APP_TRACKER).cpp')
#Replace all the .cpp extension to .o extension as objects
MONITOR_OBJ = $(patsubst %.$(SRCEXT),%.o,$(MONITOR_SRC))
MONITOR_OBJS = $(patsubst $(SRCDIR)/%,$(OBJDIR)/%,$(MONITOR_OBJ))

#Find all the source files name
SNIFFER_SRC = $(shell find $(SRCDIR) -name '*.$(SRCEXT)' | grep -vE '$(APP_TRACKER).cpp|$(APP_MONITOR).cpp')
#test printing
#$(error $(SNIFFER_SRC))
#Replace all the .cpp extension to .o extension as objects
SNIFFER_OBJ = $(patsubst %.$(SRCEXT),%.o,$(SNIFFER_SRC))
SNIFFER_OBJS = $(patsubst $(SRCDIR)/%,$(OBJDIR)/%,$(SNIFFER_OBJ))

CCFLAGS = -c -std=c++11 -Wall \
-DRLOG_COMPONENT="sniffer" -DELPP_THREAD_SAFE -DELPP_NO_DEFAULT_LOG_FILE

LDFLAGS = -lpthread -lpcap -ludt

all: stamp monitor tracker sniffer

stamp:
	- ./updateBuild.sh

monitor: $(MONITOR_OBJS)
	$(CC) $(MONITOR_OBJS) -L$(LIBDIR) $(LDFLAGS) -o $(BINDIR)/$(APP_MONITOR)

tracker: $(TRACKER_OBJS)
	$(CC) $(TRACKER_OBJS) -L$(LIBDIR) $(LDFLAGS) -o $(BINDIR)/$(APP_TRACKER)

sniffer: $(SNIFFER_OBJS)
	$(CC) $(SNIFFER_OBJS) -L$(LIBDIR) $(LDFLAGS) -o $(BINDIR)/$(APP_SNIFFER)

#If the make command is "make clean", ignore this include
ifneq "$(MAKECMDGOALS)" "clean"
-include $(OBJS:.o:.d)
endif

$(OBJDIR)/%.o: $(SRCDIR)/%.$(SRCEXT)
	@$(call make-depend,$<,$@,$(subst .o,.d,$@))
	$(CC) $(CCFLAGS) $< -o $@

.PHONY: clean

clean:
	-rm -f $(BINDIR)/$(APP_MONITOR)
	-rm -f $(BINDIR)/$(APP_TRACKER)
	-rm -f $(BINDIR)/$(APP_SNIFFER)
	-rm -f $(BINDIR)_pi/$(APP_MONITOR)
	-rm -f $(BINDIR)_pi/$(APP_TRACKER)	
	-rm -f $(BINDIR)_pi/$(APP_SNIFFER)
	-rm -f $(OBJDIR)/*.o
	-rm -f $(OBJDIR)/*.d
	-rm -f $(OBJDIR)_pi/*.o
	-rm -f $(OBJDIR)_pi/*.d

#Call make-depend (source file, object file, depend file)
define make-depend
	$(CC) -MM		\
	      -MF $3		\
	      -MP		\
	      -MT $2		\
	      $(CCFLAGS)	\
	      $1
endef
