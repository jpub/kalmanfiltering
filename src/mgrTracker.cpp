#include "mgrTracker.h"
#include "db.h"
#include "logger.h"
#include "timeMachine.h"
#include "mgrReporter.h"
#include "mgrArea.h"
#include "symbolTrack.h"

MgrTracker* MgrTracker::msInstance = 0;


MgrTracker::MgrTracker()
{
    mTracker = 0;
}

MgrTracker::~MgrTracker()
{
    delete mTracker;
}

MgrTracker& MgrTracker::getInstance()
{
	if ( msInstance == 0 )
	{
		msInstance = new MgrTracker();
	}
	return *msInstance;
}

void MgrTracker::init(
    bool activateAngleTracker,
    bool activateDistanceTracker,
    double maxAngleAgreement,
    double humanWalkSpeed,
    int trackAgeTime )
{
    mActivateAngleTracker = activateAngleTracker;
    mActivateDistanceTracker = activateDistanceTracker;
    mTrackAgeTime = trackAgeTime;

	mTracker = new TrackerAngleAgreement(
        activateAngleTracker,
        activateDistanceTracker,
        maxAngleAgreement,
        humanWalkSpeed );
}

bool MgrTracker::update( MsgDeviceLocation& msgDeviceLocation )
{
    DEVICE_LOCATION* dataStruct = msgDeviceLocation.data();
    char tmpMacBuffer[20] = {0};
    snprintf( tmpMacBuffer, 13, "%02x%02x%02x%02x%02x%02x",
              dataStruct->macAddress[0],
              dataStruct->macAddress[1],
              dataStruct->macAddress[2],
              dataStruct->macAddress[3],
              dataStruct->macAddress[4],
              dataStruct->macAddress[5] );
    string deviceId( tmpMacBuffer );

    SymbolTrack symbolTrack;
    if ( ! Db<SymbolTrack>::getInstance().get(deviceId, symbolTrack) )
    {
        symbolTrack.setDeviceId( deviceId );
        symbolTrack.setLongitude( dataStruct->positionX );
        symbolTrack.setLatitude( dataStruct->positionY );
        symbolTrack.setLastUpdateTime( 0 );
    }

    time_t timeNow = TimeMachine::getTimeNow();
    TrackerInState inState;
    inState.setDeviceId( deviceId );
    inState.setCurrentTrackerLongitude( symbolTrack.getLongitude() );
    inState.setCurrentTrackerLatitude( symbolTrack.getLatitude() );
    inState.setDetectedLongitude( dataStruct->positionX );
    inState.setDetectedLatitude( dataStruct->positionY );
    inState.setLastUpdateTime( symbolTrack.getLastUpdateTime() );
    inState.setSecondsSinceTrackerLastUpdate( timeNow - symbolTrack.getLastUpdateTime() );

    TrackerOutResult outResult = mTracker->update( inState );

    if ( outResult.getIsTrackedSuccess() )
    {
        symbolTrack.setLongitude( outResult.getTrackerLongitude() );
        symbolTrack.setLatitude( outResult.getTrackerLatitude() );
        symbolTrack.setLastUpdateTime( timeNow );
        Db<SymbolTrack>::getInstance().update( deviceId, symbolTrack );

        QItem qitem = mFactoryMsg.get( ID_TRACKED_LOCATION );
        MsgTrackedLocation* msgTrackedLocation = (MsgTrackedLocation*)(qitem.getDataPointer());
        msgTrackedLocation->header()->orgSourceId = msgDeviceLocation.header()->orgSourceId;
        memcpy( msgTrackedLocation->data()->macAddress, dataStruct->macAddress, __ARRAY_6_LENGTH );
        msgTrackedLocation->data()->positionX = symbolTrack.getLongitude();
        msgTrackedLocation->data()->positionY = symbolTrack.getLatitude();
        msgTrackedLocation->data()->positionZ = 0;
        msgTrackedLocation->data()->angleTrackerActive = mActivateAngleTracker == true ? 1 : 0;
        msgTrackedLocation->data()->distanceTrackerActive = mActivateDistanceTracker == true ? 1 : 0;

        // update area count
        MgrArea::getInstance().update( deviceId, outResult.getTrackerLongitude(), outResult.getTrackerLatitude() );

        // send to EXF
        MgrReporter::getInstance().sendToExf( qitem );
        return true;
    }
    return false;
}

bool MgrTracker::maintainance()
{
    time_t timeNow = TimeMachine::getTimeNow();
    vector<SymbolTrack> symbolTrackVec = Db<SymbolTrack>::getInstance().getAll();
    vector<SymbolTrack>::iterator it;

    for ( it=symbolTrackVec.begin(); it!=symbolTrackVec.end(); ++it )
    {
        if ( timeNow - it->getLastUpdateTime() > mTrackAgeTime )
        {
            Db<SymbolTrack>::getInstance().remove( it->getDeviceId() );
            MgrArea::getInstance().removeDevice( it->getDeviceId() );
        }
    }

    return true;
}
