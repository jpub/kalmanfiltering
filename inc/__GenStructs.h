#ifndef __GEN_STRUCTS_H
#define __GEN_STRUCTS_H

#include "__DefineVectorSize.h"
#include "prTypes.h"

/*
 * Pack 1, so that no padding is created unintentionally
 */
#pragma pack(push,1)

struct RADIO_WIFI
{
    SBYTE garbage1[__ARRAY_14_LENGTH];
    SBYTE rssi;
    SBYTE garbage2[__ARRAY_3_LENGTH]; // end of radio tap header
    SBYTE garbage3[__ARRAY_10_LENGTH];
    BYTE srcMac[__ARRAY_6_LENGTH];
};

struct MSG_HEADER
{
    UINT16 messageCode;
    UINT16 messageLength;
    UINT32 messageTime;    
    UINT32 messageNumber;
    UINT16 orgSourceId;
    UINT16 sourceId;
    UINT16 destinationId;
};

struct NIL_CONTENT
{
    INT32 spare;
};

struct SENSOR_UPDATE
{
    BYTE tgtMac[__ARRAY_6_LENGTH];
    SBYTE tgtRssi;
};

struct DEVICE_LOCATION
{
    BYTE macAddress[__ARRAY_6_LENGTH];
    INT32 positionX;
    INT32 positionY;
    INT32 positionZ;
};

struct TRACKED_LOCATION
{
    BYTE macAddress[__ARRAY_6_LENGTH];
    INT32 positionX;
    INT32 positionY;
    INT32 positionZ;
    BYTE angleTrackerActive;
    BYTE distanceTrackerActive;
};

struct SENSOR_STATUS
{
    BYTE commsAddress[__ARRAY_6_LENGTH];
    SBYTE apRssiThresholdSr;
    SBYTE apRssiThresholdMr;
    SBYTE apRssiLimit;
    SBYTE r0;
    UINT16 d0;
    INT32 positionX;
    INT32 positionY;
    INT32 positionZ;
};

struct SIM_AREA
{
    BYTE areaName[__ARRAY_32_LENGTH];
    UINT16 count;
    INT32 x1;
    INT32 y1;
    INT32 x2;
    INT32 y2;
    INT32 x3;
    INT32 y3;
    INT32 x4;
    INT32 y4;
};

struct LINK_QUALITY
{
    UINT16 clientId;
    UINT16 receivedPercentage;
    UINT16 totalReceived;
    UINT16 totalSend;
};

struct LINK_RTT_REQUEST
{
    UINT16 key;
    UINT64 milliSinceEpoch;
};

struct LINK_RTT_ACK
{
    UINT16 key;
    UINT64 milliSinceEpoch;
};

struct LINK_RTT
{
    UINT16 clientId;
    UINT16 milliRtt;
    UINT16 key;
};

struct DISCOVERY_AP
{
    BYTE srcAddress[__ARRAY_20_LENGTH];
    UINT16 trackerId;
    BYTE trackerAddress[__ARRAY_20_LENGTH];
    UINT16 monitorId;
    BYTE monitorAddress[__ARRAY_20_LENGTH];
};

struct DISCOVERY_TRK
{
    BYTE srcAddress[__ARRAY_20_LENGTH];
    INT32 spare;
};

struct DISCOVERY_MON
{
    BYTE srcAddress[__ARRAY_20_LENGTH];
    INT32 spare;
};

struct SEND_COUNTER
{
    UINT16 sendCounter;
};

struct PERIODIC_TIMER
{
    UINT32 timerCounter;
};

struct AP_INFO
{
    UINT16 apId;
    UINT32 apUpdateTime;
    SBYTE apRssiThresholdSr;
    SBYTE apRssiThresholdMr;
    SBYTE apRssiLimit;
    INT32 positionX;
    INT32 positionY;
    INT32 positionZ;
    SBYTE detectedRssi;
    UINT16 detectedDistance;
    UINT16 refApId;
    SBYTE refApRssi;
    UINT16 refApDistance;
    FLOAT refApG;
    FLOAT refApN;
    UINT16 isInUse;
};

struct DBG_DEVICE_UPDATE
{
    BYTE deviceMac[__ARRAY_6_LENGTH];
    INT32 positionX;
    INT32 positionY;
    INT32 positionZ;
    UINT16 multilatType;
    UINT16 srApId;
    UINT16 mrApId1;
    UINT16 mrApId2;
    UINT16 mrApId3;
    UINT16 numberOfApInfo;
    AP_INFO apInfo[__ARRAY_20_LENGTH];
};

struct CODE_INFO
{
    UINT16 messageCode;
};

/*
*   mode value
*   0: invalid
*   1: capture ALL messages
*   2: DO NOT capture code listed
*/
struct DBG_UPDATE_CAPTURE
{
    BYTE mode;
    BYTE numberOfCode;
    CODE_INFO codeInfo[__ARRAY_50_LENGTH];
};

struct AP_UPDATE
{
    UINT16 apId;
    UINT32 bitmask;
    SBYTE apRssiThresholdSr;
    SBYTE apRssiThresholdMr;
    SBYTE apRssiLimit;
    SBYTE r0;
    UINT16 d0;
    INT32 positionX;
    INT32 positionY;
    INT32 positionZ;
};

struct AP_UPDATE_ACK
{
    UINT16 apId;
    UINT32 bitmask;
    SBYTE apRssiThresholdSr;
    SBYTE apRssiThresholdMr;
    SBYTE apRssiLimit;
    SBYTE r0;
    UINT16 d0;
    INT32 positionX;
    INT32 positionY;
    INT32 positionZ;
};

struct TRK_STATUS
{
    INT16 regulatorSeconds;
};

struct SENSOR_REGULATOR_UPDATE
{
    INT16 regulatorSeconds;
};

/************************************/
/*          PIPE THROUGH            */
/************************************/
struct MLT_STATUS
{
    UINT16 numberOfBytes;
    BYTE payload[__ARRAY_512_LENGTH];
};

struct MLT_STATUS_REQUEST
{
    UINT16 numberOfBytes;
    BYTE payload[__ARRAY_512_LENGTH];
};

struct G_UPDATE
{
    UINT16 numberOfBytes;
    BYTE payload[__ARRAY_512_LENGTH];
};

struct G_UPDATE_ACK
{
    UINT16 numberOfBytes;
    BYTE payload[__ARRAY_512_LENGTH];
};

/************************************/
/*      APPLICATION STRUCTS         */
/************************************/
struct POINTF_2D
{
    double x;
    double y;
};

#pragma pack(pop)
#endif
