#include <string>
#include "__DefineVectorSize.h"
#include "timeMachine.h"
#include "mgrConfigStatic.h"
#include "mgrReporter.h"
#include "mgrPeriodic.h"
#include "mgrTracker.h"
#include "threadRUdpTrkToApSender.h"
#include "threadRUdpTrkToMonSender.h"
#include "eventHandlerTracker.h"
#include "logger.h"

using namespace std;

EventHandlerTracker::EventHandlerTracker()
{
	mCycleDiscovery = MgrConfigStatic::getInstance().getCycleDiscovery();	
	mCycleLinkLatency = MgrConfigStatic::getInstance().getCycleLinkLatencyRequest();
	mCycleTrackMaintenance = MgrConfigStatic::getInstance().getCycleTrackMaintenance();
	mCycleAreaStatus = MgrConfigStatic::getInstance().getCycleAreaStatus();
}

EventHandlerTracker::~EventHandlerTracker()
{
}

bool EventHandlerTracker::handle( MsgPeriodicTimer& msg )
{
	LOG(DEBUG) << "handle timer update: Code:" << msg.header()->messageCode;

	unsigned int timerCounter = msg.data()->timerCounter;

	if ( timerCounter % mCycleDiscovery == 0 )
		MgrPeriodic::getInstance().sendDiscoveryTrk();

	if ( timerCounter % mCycleLinkLatency == 0 )
		MgrPeriodic::getInstance().sendLinkLatencyRequest();

	if ( timerCounter % mCycleTrackMaintenance == 0 )
		MgrPeriodic::getInstance().maintainTracks();

	if ( timerCounter % mCycleAreaStatus == 0 )
		MgrPeriodic::getInstance().sendAreaUpdate();

	return true;
}

bool EventHandlerTracker::handle( MsgDbgDeviceUpdate& msg )
{
	LOG(DEBUG) << "handle dbg device update: Code:" << msg.header()->messageCode;

	// send to EXF
	MgrReporter::getInstance().sendToExf( msg.clone() );
	return true;
}

bool EventHandlerTracker::handle( MsgDeviceLocation& msg )
{
	LOG(DEBUG) << "handle device location: Code:" << msg.header()->messageCode;

	// send to EXF
	//MgrReporter::getInstance().sendToExf( msg.clone() );
	MgrTracker::getInstance().update( msg );
	return true;
}

bool EventHandlerTracker::handle( MsgSensorStatus& msg )
{
	LOG(DEBUG) << "handle sensor status: Code:" << msg.header()->messageCode;
	
	// send to EXF
	MgrReporter::getInstance().sendToExf( msg.clone() );

	return true;
}

bool EventHandlerTracker::handle( MsgLinkQuality& msg )
{
	int src = msg.header()->orgSourceId;
	int client = msg.data()->clientId;
	int percent = msg.data()->receivedPercentage;
	int totalReceived = msg.data()->totalReceived;
	int totalSend = msg.data()->totalSend;


	LOG(DEBUG) << "handle link quality from " << src << ": Code:" << msg.header()->messageCode;
	LOG(DEBUG) << "LINK QUALITY " << src << " -- " << client << " : " << percent << "%  " << totalReceived << "/" << totalSend;

	// send to EXF
	MgrReporter::getInstance().sendToExf( msg.clone() );
	
	return true;
}

bool EventHandlerTracker::handle( MsgDiscoveryAp& msg )
{
	LOG(DEBUG) << "handle Discovery AP: Code:" << msg.header()->messageCode;
	int destId = msg.header()->orgSourceId;
	LOG(DEBUG) << "AP " << destId << " - " << msg.data()->srcAddress;
	
	char destIpArr[__ARRAY_20_LENGTH] = {0};
	memcpy( destIpArr, msg.data()->srcAddress, __ARRAY_20_LENGTH );
	string destIp = destIpArr;

	if ( MgrReporter::getInstance().isApRudpConnected(destIp) )
		return true;

	int apApRudpPort = MgrConfigStatic::getInstance().getApApRudpPort();
	int ttl = MgrConfigStatic::getInstance().getTtl();

	MsgQueue<QItem>* rudpApOutgoingQueue = new MsgQueue<QItem>(); 
	ThreadRUdpTrkToApSender* rudpTrkToApSender = new ThreadRUdpTrkToApSender(
		rudpApOutgoingQueue,
		destId,
        destIp.c_str(),
        apApRudpPort,
        INADDR_ANY,
        0,
        ttl );

	rudpTrkToApSender->start();
	MgrReporter::getInstance().addRudpApSendingQueue( destIp, rudpApOutgoingQueue );

	return true;
}

bool EventHandlerTracker::handle( MsgDiscoveryTrk& msg )
{
	LOG(DEBUG) << "handle Discovery TRK: Code:" << msg.header()->messageCode;
	LOG(DEBUG) << "NO OP";
	return true;
}

bool EventHandlerTracker::handle( MsgDiscoveryMon& msg )
{
	LOG(DEBUG) << "handle Discovery MON: Code:" << msg.header()->messageCode;
	int destId = msg.header()->orgSourceId;
	LOG(DEBUG) << "MON " << destId << " - " << msg.data()->srcAddress;

	char destIpArr[__ARRAY_20_LENGTH] = {0};
	memcpy( destIpArr, msg.data()->srcAddress, __ARRAY_20_LENGTH );
	string destIp = destIpArr;

	if ( MgrReporter::getInstance().isMonRudpConnected() )
		return true;

	int monitorPort = MgrConfigStatic::getInstance().getMonitorPort();
	int ttl = MgrConfigStatic::getInstance().getTtl();

	MsgQueue<QItem>* rudpMonOutgoingQueue = new MsgQueue<QItem>(); 
	ThreadRUdpTrkToMonSender* rudpTrkToMonSender = new ThreadRUdpTrkToMonSender(
		rudpMonOutgoingQueue,
		destId,
        destIp.c_str(),
        monitorPort,
        INADDR_ANY,
        0,
        ttl );

	rudpTrkToMonSender->start();
	MgrReporter::getInstance().addRudpMonSendingQueue( destIp, rudpMonOutgoingQueue );

	return true;
}

bool EventHandlerTracker::handle( MsgApUpdate& msg )
{
	LOG(DEBUG) << "handle AP update: Code:" << msg.header()->messageCode;

	// broadcast to all SNF
	MgrReporter::getInstance().rudpBroadcastToAp( msg.clone() );
	return true;
}

bool EventHandlerTracker::handle( MsgApUpdateAck& msg )
{
	LOG(DEBUG) << "handle AP update ack: Code:" << msg.header()->messageCode;

	// send to EXF
	MgrReporter::getInstance().sendToExf( msg.clone() );
	return true;
}

bool EventHandlerTracker::handle( MsgLinkRttAck& msg )
{
	int src = msg.header()->orgSourceId;
	LOG(DEBUG) << "handle link RTT ack from " << src << ": Code:" << msg.header()->messageCode;

	
	long long unsigned int sendTime = msg.data()->milliSinceEpoch;
    long long unsigned int recvTime = TimeMachine::getMilliSinceEpoch();
    unsigned int latency = recvTime - sendTime;

	LOG(DEBUG) << "LINK RTT TRK -- " << src << " : " << latency << "ms : key[" << msg.data()->key << "]";

	QItem qitem = mFactoryMsg.get( ID_LINK_RTT );
    MsgLinkRtt* msgLinkRtt = (MsgLinkRtt*)(qitem.getDataPointer());
    msgLinkRtt->data()->clientId = src;
    msgLinkRtt->data()->milliRtt = latency;
    msgLinkRtt->data()->key = msg.data()->key;

    // send to EXF
    MgrReporter::getInstance().sendToExf( qitem );

	return true;
}

