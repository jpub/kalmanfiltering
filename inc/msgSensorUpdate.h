#ifndef __MSG_SENSOR_UPDATE_H
#define __MSG_SENSOR_UPDATE_H

#include "msgBase.h"

class ByteBuffer;

class MsgSensorUpdate : public MsgBase
{
public:
    ENABLE_EVENT_DISPATCH();

public:
    virtual QItem clone();

public:
    MsgSensorUpdate();
    MsgSensorUpdate( ByteBuffer* inBuffer );
    MsgSensorUpdate( const MsgSensorUpdate& rhs );
    MsgSensorUpdate& operator=( const MsgSensorUpdate& rhs );

    virtual int getSize();
    virtual ByteBuffer* pack( int& bytePosition );
    virtual void unpack( int& bytePosition );
    virtual int print();


    SENSOR_UPDATE* data()
    {
        return &mData;
    }
    
private:
    SENSOR_UPDATE mData;
};

#endif
