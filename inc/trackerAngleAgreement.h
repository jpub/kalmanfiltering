#ifndef __TRACKER_ANGLE_AGREEMENT_H
#define __TRACKER_ANGLE_AGREEMENT_H

#include <map>
#include <vector>
#include <string>
#include "__GenStructs.h"
#include "trackerBase.h"


using std::map;
using std::vector;
using std::string;


class TrackerAngleAgreement : public TrackerBase
{
public:
	TrackerAngleAgreement(
		bool activateAngleTracker,
		bool activateDistanceTracker,
		double maxAngleAgreement,
		double humanWalkSpeed );

	virtual TrackerOutResult update( TrackerInState& inState );


private:
	void addPointHistory( string deviceId, double x, double y );
	double getAngle( POINTF_2D anchorPoint, POINTF_2D pointA, POINTF_2D pointB );
	bool checkAgreement( POINTF_2D anchorPoint, POINTF_2D point1, POINTF_2D point2, int timeLapsed, TrackerOutResult& outResult );
	void updateDevice( POINTF_2D selectedPoint, int timeLapsed, TrackerOutResult& outResult );


private:
	map< string, vector<POINTF_2D> > mDeviceAngleCheckerMap;
	bool mActivateAngleTracker;
	bool mActivateDistanceTracker;
	double mMaxAngleAgreement;
	double mHumanWalkSpeed;
};

#endif