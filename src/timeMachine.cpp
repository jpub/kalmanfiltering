#include <sys/time.h>
#include <time.h>
#include <stdio.h>
#include "timeMachine.h"

unsigned int TimeMachine::getTimeNow()
{
	// struct timeval tv;
 //    gettimeofday( &tv, NULL );
 //    return tv.tv_sec;
	return time(0);
}

long long unsigned int TimeMachine::getMilliSinceEpoch()
{
	struct timeval tv;
    gettimeofday( &tv, NULL );
    return ( (tv.tv_sec*1000) + (tv.tv_usec/1000) );
}