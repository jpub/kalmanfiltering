#include "easylogging++.h"
#include <pthread.h>
#include <unistd.h>

INITIALIZE_EASYLOGGINGPP

static void* func1( void* arg )
{
    char* uargv = 0;
    while ( true )
    {
        usleep( 1000000 );
        LOG(DEBUG) << "func1 wake up";
    }
    return uargv;
}

static void* func2( void* arg )
{
    char* uargv = 0;
    while ( true )
    {
        usleep( 1000000 );
        LOG(DEBUG) << "func2 wake up";
    }
    return uargv;
}

int main( int argc, char** argv )
{
    // without this line it will still get truncated but not so fast
    el::Loggers::addFlag( el::LoggingFlag::StrictLogFileSizeCheck );
    el::Configurations conf( "log.conf" );    
    el::Loggers::reconfigureAllLoggers( conf );
    
    int x = 23;
    LOG(DEBUG) << "hello DEBUG " << x;
    // this will terminate the application
    //LOG(FATAL) << "hello FATAL " << x;
    LOG(WARNING) << "hello WARNING " << x;
    LOG(INFO) << "hello INFO " << x;
    LOG(ERROR) << "hello ERROR " << x;


    pthread_t thread1;
    pthread_t thread2;

    pthread_create( &thread1, NULL, func1, &x );
    pthread_create( &thread2, NULL, func2, &x );

    while ( true )
    {
        usleep( 1000000 );
        LOG(DEBUG) << "main wake up";
    }
    return 0;
}
