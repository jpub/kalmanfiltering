#ifndef __SYMBOL_TRACK_H
#define __SYMBOL_TRACK_H

#include <string>

using std::string;

class SymbolTrack
{
public:
	string getDeviceId();
	double getLongitude();
	double getLatitude();
	unsigned int getLastUpdateTime();

	void setDeviceId( string deviceId );
	void setLongitude( double longitude );
	void setLatitude( double latitude );
	void setLastUpdateTime( unsigned int lastUpdateTime );

private:
	string mDeviceId;
	double mLongitude;
	double mLatitude;
	unsigned int mLastUpdateTime;
};

#endif