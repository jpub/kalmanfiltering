#ifndef __MSG_MLT_STATUS_REQUEST_H
#define __MSG_MLT_STATUS_REQUEST_H

#include "msgBase.h"

class ByteBuffer;

class MsgMltStatusRequest : public MsgBase
{
public:
    ENABLE_EVENT_DISPATCH();

public:
    virtual QItem clone();

public:
    MsgMltStatusRequest();
    MsgMltStatusRequest( ByteBuffer* inBuffer );
    MsgMltStatusRequest( const MsgMltStatusRequest& rhs );
    MsgMltStatusRequest& operator=( const MsgMltStatusRequest& rhs );

    virtual int getSize();
    virtual ByteBuffer* pack( int& bytePosition );
    virtual void unpack( int& bytePosition );
    virtual int print();

    MLT_STATUS_REQUEST* data()
    {
        return &mData;
    }
    
private:
    MLT_STATUS_REQUEST mData;
};

#endif
