#include "byteBuffer.h"
#include "msgBase.h"
#include "msgNilContent.h"
#include <stdio.h>


QItem MsgNilContent::clone()
{
    MsgNilContent* msg = new MsgNilContent();
    if ( this->mRawBuffer )
        msg->mRawBuffer = new ByteBuffer( *this->mRawBuffer );
    msg->mHeader = this->mHeader;
    msg->mData = this->mData;

    return QItem( msg );
}

MsgNilContent::MsgNilContent()
{
    memset( &mData, 0, sizeof(mData) );
    mHeader.messageCode = ID_NIL_CONTENT;
    mHeader.messageLength = getSize();
}

MsgNilContent::MsgNilContent( ByteBuffer* inBuffer )
    :MsgBase( inBuffer )
{
    int bytePosition = 0;
    unpack( bytePosition );
}

MsgNilContent::MsgNilContent( const MsgNilContent& rhs )
    :MsgBase( rhs )
{
    mData = rhs.mData;
}

MsgNilContent& MsgNilContent::operator=( const MsgNilContent& rhs )
{
    MsgBase::operator = (rhs);
    mData = rhs.mData;
    return *this;
}

int MsgNilContent::getSize()
{
    int totalSize = 0;
    totalSize += MsgBase::getSize();
    totalSize += sizeof( mData );
    return totalSize;
}

ByteBuffer* MsgNilContent::pack( int& bytePosition )
{
    MsgBase::pack( bytePosition );
    mRawBuffer->appendByteArray( (char*)&mData, sizeof(mData) );
    return mRawBuffer;
}

void MsgNilContent::unpack( int& bytePosition )
{
    MsgBase::unpack( bytePosition );
    mRawBuffer->popByteArray( (char*)&mData, sizeof(mData) );
}

int MsgNilContent::print()
{
    int charWritten = MsgBase::print();

    charWritten = snprintf( mPrintBuffer + charWritten, PRINT_BUFFER_SIZE,
        "No content" );

    LOG(DEBUG) << mPrintBuffer;
    return charWritten;
}
