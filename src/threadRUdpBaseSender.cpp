#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "__DefineVectorSize.h"
#include "byteBuffer.h"
#include "threadRUdpBaseSender.h"
#include "msgQueue.h"
#include "logger.h"


ThreadRUdpBaseSender::ThreadRUdpBaseSender(
    MsgQueue<QItem>* outgoingQueue,
    int destinationId,
    const char* destIp,
    int destPort,
    const char* localBindIp,
    int localBindPort,
    int ttl )
    : ThreadBaseSender(outgoingQueue, destinationId),
      mDestIp(destIp),
      mDestPort(destPort),
      mLocalBindIp(localBindIp),
      mLocalBindPort(localBindPort),
      mTtl(ttl)
{
    struct addrinfo hints;
    memset( &hints, 0, sizeof(struct addrinfo) );
    
    hints.ai_flags = AI_PASSIVE;
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_DGRAM;

    if ( getaddrinfo(NULL, "0", &hints, &mLocalAddr) != 0 )
    {
        LOG(FATAL) << "UDT unable to get local address" << UDT::getlasterror().getErrorMessage();
    }

    mSockfd = UDT::socket( mLocalAddr->ai_family, mLocalAddr->ai_socktype, mLocalAddr->ai_protocol ); 
    if ( mSockfd == -1 )
    {
        LOG(FATAL) << "UDT unable to create socket:" << UDT::getlasterror().getErrorMessage();
    }

    char destPortStr[PORT_STRING_LENGTH] = { 0 };
    sprintf( destPortStr, "%d", mDestPort );
    if ( getaddrinfo(mDestIp, destPortStr, &hints, &mDestAddr) != 0 )
    {
        LOG(FATAL) << "UDT unable to get destination address" << UDT::getlasterror().getErrorMessage();
    }

    // connect to the server, implict bind
    if ( UDT::ERROR == UDT::connect(mSockfd, mDestAddr->ai_addr, mDestAddr->ai_addrlen) )
    {
        LOG(WARNING) << "UDT connect failure:" << UDT::getlasterror().getErrorMessage();
    }
    else
    {
        LOG(INFO) << "UDT connect to " << mDestIp;
    }
    mCounter = 0;
}

bool ThreadRUdpBaseSender::sendMessage( MsgBase& msg )
{
    int position = 0;
    ByteBuffer* buffer = msg.pack( position );

    int totalLength = buffer->getSize();
    char* data = buffer->getDataPtr();
    int bytesSend = UDT::sendmsg( mSockfd, data, totalLength, mTtl, true );
    if ( bytesSend == -1 )
    {
        handleDisconnection();
        return false;
    }
    
    return true;
}