#ifndef __MSG_SENSOR_REGULATOR_H
#define __MSG_SENSOR_REGULATOR_H

#include "msgBase.h"

class ByteBuffer;

class MsgSensorRegulatorUpdate : public MsgBase
{
public:
    ENABLE_EVENT_DISPATCH();

public:
    virtual QItem clone();

public:
    MsgSensorRegulatorUpdate();
    MsgSensorRegulatorUpdate( ByteBuffer* inBuffer );
    MsgSensorRegulatorUpdate( const MsgSensorRegulatorUpdate& rhs );
    MsgSensorRegulatorUpdate& operator=( const MsgSensorRegulatorUpdate& rhs );

    virtual int getSize();
    virtual ByteBuffer* pack( int& bytePosition );
    virtual void unpack( int& bytePosition );
    virtual int print();

    SENSOR_REGULATOR_UPDATE* data()
    {
        return &mData;
    }
    
private:
    SENSOR_REGULATOR_UPDATE mData;
};

#endif
