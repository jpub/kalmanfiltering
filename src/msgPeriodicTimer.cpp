#include "byteBuffer.h"
#include "msgBase.h"
#include "msgPeriodicTimer.h"
#include <stdio.h>


QItem MsgPeriodicTimer::clone()
{
    MsgPeriodicTimer* msg = new MsgPeriodicTimer();
    if ( this->mRawBuffer )
        msg->mRawBuffer = new ByteBuffer( *this->mRawBuffer );
    msg->mHeader = this->mHeader;
    msg->mData = this->mData;

    return QItem( msg );
}

MsgPeriodicTimer::MsgPeriodicTimer()
{
    memset( &mData, 0, sizeof(mData) );
    mHeader.messageCode = ID_PERIODIC_TIMER;
    mHeader.messageLength = getSize();
}

MsgPeriodicTimer::MsgPeriodicTimer( ByteBuffer* inBuffer )
    :MsgBase( inBuffer )
{
    int bytePosition = 0;
    unpack( bytePosition );
}

MsgPeriodicTimer::MsgPeriodicTimer( const MsgPeriodicTimer& rhs )
    :MsgBase( rhs )
{
    mData = rhs.mData;
}

MsgPeriodicTimer& MsgPeriodicTimer::operator=( const MsgPeriodicTimer& rhs )
{
    MsgBase::operator = (rhs);
    mData = rhs.mData;
    return *this;
}

int MsgPeriodicTimer::getSize()
{
    int totalSize = 0;
    totalSize += MsgBase::getSize();
    totalSize += sizeof( mData );
    return totalSize;
}

ByteBuffer* MsgPeriodicTimer::pack( int& bytePosition )
{
    MsgBase::pack( bytePosition );
    mRawBuffer->appendByteArray( (char*)&mData, sizeof(mData) );
    return mRawBuffer;
}

void MsgPeriodicTimer::unpack( int& bytePosition )
{
    MsgBase::unpack( bytePosition );
    mRawBuffer->popByteArray( (char*)&mData, sizeof(mData) );
}

int MsgPeriodicTimer::print()
{
    int charWritten = MsgBase::print();

    charWritten = snprintf( mPrintBuffer + charWritten, PRINT_BUFFER_SIZE,
        "TimerCounter:%d",
        mData.timerCounter );

    LOG(DEBUG) << mPrintBuffer;
    return charWritten;
}
