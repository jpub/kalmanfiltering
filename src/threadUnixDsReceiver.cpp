#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>

#include "threadUnixDsReceiver.h"
#include "msgQueue.h"
#include "logger.h"


using std::string;

ThreadUnixDsReceiver::ThreadUnixDsReceiver(
    MsgQueue<QItem>* incomingQueue,
    string localPath )
    : ThreadBaseReceiver(incomingQueue),
      mLocalPath(localPath)
{
    mSockfd = socket( AF_UNIX, SOCK_DGRAM, 0 );
    if ( mSockfd == -1 )
    {
        LOG(FATAL) << "Create domain socket fail. Error:" << errno;
    }

    mLocalAddr.sun_family = AF_UNIX;
    strcpy( mLocalAddr.sun_path, mLocalPath.c_str() );

    int len = strlen(mLocalAddr.sun_path) + sizeof(mLocalAddr.sun_family);
    unlink( mLocalAddr.sun_path );

    if ( bind(mSockfd, (struct sockaddr *)&mLocalAddr, len) == -1 )
    {
        LOG(FATAL) << "Bind domain socket fail. Error:" << errno;
    }
}

void* ThreadUnixDsReceiver::run( void* arg )
{
    char socketData[RECV_BUFFER_SIZE];
    int bytesRecv = 0;
    char* nullArg = 0;

    while ( true )
    {
        bytesRecv = recv( mSockfd, socketData, RECV_BUFFER_SIZE, 0 );

        if ( bytesRecv == -1 )
        {
            LOG(FATAL) << "Receive fail!!! Error:" << errno;
            return nullArg;
        }

        if ( bytesRecv == 0 )
        {
            LOG(WARNING) << "Receive empty data..continue";
            continue;
        }

        if ( !mRingBuffer.write(socketData, bytesRecv) )
        {
            LOG(WARNING) << "Message not written to buffer";
            continue;
        }

        // process the data in the ring buffer
        // If this is too time consuming..may have to shift to another thread
        processRingBuffer();
    }
    return nullArg;
}
