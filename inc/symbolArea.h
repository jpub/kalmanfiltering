#ifndef __SYMBOL_AREA_H
#define __SYMBOL_AREA_H

#include <string>
#include <vector>
#include <map>

#include "__GenStructs.h"

using std::string;
using std::vector;
using std::map;

class SymbolArea
{
public:
	SymbolArea();
	SymbolArea( string name, vector<POINTF_2D> pointList );

public:
	void update( string deviceId, int posX, int posY );
	string getName();
	unsigned int getCount();
	vector<POINTF_2D> getPointVec();
	void removeDevice( string deviceId );
	
private:
	bool isInside( int posX, int posY );
	void addDevice( string deviceId );

private:
	string mName;
	vector<POINTF_2D> mPointList;
	map<string,int> mDeviceMap;
	double mMinX;
	double mMinY;
	double mMaxX;
	double mMaxY;
};

#endif