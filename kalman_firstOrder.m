close all
clear all
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Ground truth %%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Set true trajectory 
Nsamples=200;
dt = .1;
t=0:dt:dt*Nsamples;
Vtrue = zeros(1,Nsamples+1);
Vtrue(1) = 10;

% Xtrue is a vector of true positions of the train 
Xtrue = zeros(1,Nsamples+1);
Xinitial = 30;
Xtrue(1) = Xinitial + Vtrue(1) * dt;

% Vtrue is a vector of true positions of the train 
Vtrue = zeros(1,Nsamples+1);
for index = 1:50
    Vtrue(index) = 10;
end

% the train abrupt u-turn
for index = 51:Nsamples+1
    Vtrue(index) = 0;
end

for index = 2:Nsamples+1
    Xtrue(index) = Xtrue(index - 1) + Vtrue(index) * dt;
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Motion equations %%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Previous state (initial guess): Our guess is that the train starts at 0 with velocity
% that equals to 50% of the real velocity
Xk_prev = [0; 
    .5*Vtrue(1)];

% Current state estimate
X_fused=[];

% Current predict estimate
Xpredict=[];

% Motion equation: X_fused = F*Xk_prev + Noise, that is X_fused(n) = X_fused(n-1) + Vk(n-1) * dt
% Of course, V is not measured, but it is estimated
% F represents the dynamics of the system: it is the motion equation
F = [1 dt;
     0  1];

% The error matrix (or the confidence matrix): P states whether we should 
% give more weight to the new measurement or to the model estimate 
initial_error_estimate = 10;
% P = sigma^2*G*G';
P = [initial_error_estimate^2             0;
                 0			initial_error_estimate^2];

% Q is the process noise covariance. It represents the amount of
% uncertainty in the model. In our case, we arbitrarily assume that the model is perfect (no
% acceleration allowed for the train, or in other words - any acceleration is considered to be a noise)
sigma_process_noise = 0.2;
Q = [0 			0;
			0 				sigma_process_noise^2];

% H is the measurement matrix. 
% We measure X, so H(1) = 1
% We do not measure V, so H(2)= 0
H = [1 0];

% R is the measurement noise covariance. Generally R and sigma_measurement_noise can
% vary between samples. 
sigma_measurement_noise = 1; % 1 m/sec
R = sigma_measurement_noise^2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Kalman iteration %%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Buffers for later display
X_buffer = zeros(2,Nsamples+1);
X_fused_buffer(:,1) = Xk_prev;
Z_buffer = zeros(1,Nsamples+1);

P_buffer(:,:,1) = P;
actualPositionError_buffer = zeros(1,Nsamples+1);
actualSpeedError_buffer = zeros(1,Nsamples+1);
axis_buffer = zeros(1,Nsamples+1);
axis_buffer(1) = 1;

% the actual measurement error. If match with the given specs of sensor,
% this value will be one. If sensor under-declare, we need to increase
% the multiplier

measurement_noise_multiplier = 1;

for k=1:Nsamples
    
    axis_buffer(k+1) = k+1;
    
    % Z is the measurement vector. In our
    % case, Z = TrueData + RandomGaussianNoise
    Z = Xtrue(k+1)+measurement_noise_multiplier*sigma_measurement_noise*randn;
    Z_buffer(k+1) = Z;
    
	% Predict to the current timestep
	
    % Kalman iteration
    P1 = F*P*F' + Q;
    S = H*P1*H' + R;
    
    % K is Kalman gain. If K is large, more weight goes to the measurement.
    % If K is low, more weight goes to the model prediction.
    K = P1*H'*inv(S);
    P = P1 - K*H*P1;
    
    X_fused = F*Xk_prev + K*(Z-H*F*Xk_prev);
    X_fused_buffer(:,k+1) = X_fused;
    
    P_buffer(:,:,k+1) = P;
    actualPositionError_buffer(k+1) = X_fused(1,1)-Xtrue(k+1);
    actualSpeedError_buffer(k+1) = X_fused(2,1)-Vtrue(k+1);
    % For the next iteration
    Xk_prev = X_fused; 
end;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Plot resulting graphs %%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Position Error analysis %%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

position_sigma_buffer = zeros(1,Nsamples+1);
for index = 1:Nsamples+1
    position_sigma_buffer(index) = sqrt(P_buffer(1,1,index));
end

figure;
plot(axis_buffer,position_sigma_buffer,'r');
hold on;
plot(axis_buffer,-1*position_sigma_buffer,'r');
plot(axis_buffer,actualPositionError_buffer,'b');
xlabel('Time (s)');
ylabel('Position Error estimation');
legend('theory','theory','actual');
axis([0 inf -5 5])

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Position analysis %%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure;
plot(t,Xtrue,'Color',[0 0.5 0]);
hold on;
plot(t,Z_buffer,'r');
plot(t,X_fused_buffer(1,:),'b');
title('Position estimation results');
xlabel('Time (s)');
ylabel('Position (m)');
legend('True position','Measurements','Kalman estimated');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Speed Error analysis %%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

speed_sigma_buffer = zeros(1,Nsamples+1);
for index = 1:Nsamples+1
    speed_sigma_buffer(index) = sqrt(P_buffer(2,2,index));
end

figure;
plot(axis_buffer,speed_sigma_buffer,'r');
hold on;
plot(axis_buffer,-1*speed_sigma_buffer,'r');
plot(axis_buffer,actualSpeedError_buffer,'b');
xlabel('Time (s)');
ylabel('Speed Error estimation');
legend('theory','theory','actual');
axis([0 inf -5 5])

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Speed analysis %%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure;
plot(t,Vtrue,'m');
hold on;
plot(t,X_fused_buffer(2,:),'k');
title('Velocity estimation results');
xlabel('Time (s)');
ylabel('Speed (m/s)');
legend('True speed','Kalman estimated');



% Note: K is a measure of the convergence of the filter, so are P(1,1) and
% P(2,2). When the model is good and the data is consistent with the model,
% these should converge to 0.

