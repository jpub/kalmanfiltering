#include "byteBuffer.h"
#include "msgBase.h"
#include "msgSimArea.h"
#include <stdio.h>


QItem MsgSimArea::clone()
{
    MsgSimArea* msg = new MsgSimArea();
    if ( this->mRawBuffer )
        msg->mRawBuffer = new ByteBuffer( *this->mRawBuffer );
    msg->mHeader = this->mHeader;
    msg->mData = this->mData;

    return QItem( msg );
}

MsgSimArea::MsgSimArea()
{
    memset( &mData, 0, sizeof(mData) );
    mHeader.messageCode = ID_SIM_AREA;
    mHeader.messageLength = getSize();
}

MsgSimArea::MsgSimArea( ByteBuffer* inBuffer )
    :MsgBase( inBuffer )
{
    int bytePosition = 0;
    unpack( bytePosition );
}

MsgSimArea::MsgSimArea( const MsgSimArea& rhs )
    :MsgBase( rhs )
{
    mData = rhs.mData;
}

MsgSimArea& MsgSimArea::operator=( const MsgSimArea& rhs )
{
    MsgBase::operator = (rhs);
    mData = rhs.mData;
    return *this;
}

int MsgSimArea::getSize()
{
    int totalSize = 0;
    totalSize += MsgBase::getSize();
    totalSize += sizeof( mData );
    return totalSize;
}

ByteBuffer* MsgSimArea::pack( int& bytePosition )
{
    MsgBase::pack( bytePosition );
    mRawBuffer->appendByteArray( (char*)&mData, sizeof(mData) );
    return mRawBuffer;
}

void MsgSimArea::unpack( int& bytePosition )
{
    MsgBase::unpack( bytePosition );
    mRawBuffer->popByteArray( (char*)&mData, sizeof(mData) );
}

int MsgSimArea::print()
{
    int charWritten = MsgBase::print();
    charWritten = snprintf( mPrintBuffer + charWritten, PRINT_BUFFER_SIZE,
        "AreaName:%s Count:%d X1:%d Y1:%d X2:%d Y2:%d X3:%d Y3:%d X4:%d Y4:%d",
        mData.areaName,
        mData.count,
        mData.x1,
        mData.y1,
        mData.x2,
        mData.y2,
        mData.x3,
        mData.y3,
        mData.x4,
        mData.y4 );

    LOG(DEBUG) << mPrintBuffer;
    return charWritten;
}
