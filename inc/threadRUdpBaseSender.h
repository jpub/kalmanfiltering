#ifndef __THREAD_RUDP_BASE_SEDNER_H
#define __THREAD_RUDP_BASE_SEDNER_H

#include <netdb.h>
#include "factoryMsg.h"
#include "threadBaseSender.h"
#include "udt.h"


class ThreadRUdpBaseSender : public ThreadBaseSender
{
public:
    ThreadRUdpBaseSender( MsgQueue<QItem>* outgoingQueue,
                          int destinationId,
                          const char* destIp,
                          int destPort,
                          const char* localBindIp,
                          int localBindPort,
                          int ttl );

    virtual bool sendMessage( MsgBase& );

    // sub class override for specific behaviour
    virtual void handleDisconnection() = 0;
    

protected:
    const char* mDestIp;
    int mDestPort;
    const char* mLocalBindIp;
    int mLocalBindPort;
    int mTtl;

    unsigned int mCounter;
    FactoryMsg mFactoryMsg;

    UDTSOCKET mSockfd;
    struct addrinfo hints;
    struct addrinfo* mLocalAddr;
    struct addrinfo* mDestAddr;
};

#endif
