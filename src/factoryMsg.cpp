#include "__EventIds.h"
#include "allMessages.h"
#include "factoryMsg.h"
#include "logger.h"
#include "byteBuffer.h"


FactoryMsg::FactoryMsg()
{
    mDummy = QItem(new MsgSensorUpdate());
}

QItem FactoryMsg::get( int messageCode, ByteBuffer* bb )
{
    QItem msgItem;
    
    switch ( messageCode )
    {
    case ID_DBG_DEVICE_UPDATE:
        msgItem = bb == 0 ? QItem(new MsgDbgDeviceUpdate()) : QItem(new MsgDbgDeviceUpdate(bb));
        break;

    case ID_DBG_UPDATE_CAPTURE:
        msgItem = bb == 0 ? QItem(new MsgDbgUpdateCapture()) : QItem(new MsgDbgUpdateCapture(bb));
        break;
        
    case ID_AP_UPDATE:
        msgItem = bb == 0 ? QItem(new MsgApUpdate()) : QItem(new MsgApUpdate(bb));
        break;

    case ID_AP_UPDATE_ACK:
        msgItem = bb == 0 ? QItem(new MsgApUpdateAck()) : QItem(new MsgApUpdateAck(bb));
        break;

    case ID_DEVICE_LOCATION:
        msgItem = bb == 0 ? QItem(new MsgDeviceLocation()) : QItem(new MsgDeviceLocation(bb));
        break;

    case ID_SENSOR_UPDATE:
        msgItem = bb == 0 ? QItem(new MsgSensorUpdate()) : QItem(new MsgSensorUpdate(bb));
        break;

    case ID_SENSOR_STATUS:
        msgItem = bb == 0 ? QItem(new MsgSensorStatus()) : QItem(new MsgSensorStatus(bb));
        break;

    case ID_LINK_QUALITY:
        msgItem = bb == 0 ? QItem(new MsgLinkQuality()) : QItem(new MsgLinkQuality(bb));
        break;

    case ID_DISCOVERY_AP:
        msgItem = bb == 0 ? QItem(new MsgDiscoveryAp()) : QItem(new MsgDiscoveryAp(bb));
        break;

    case ID_DISCOVERY_TRK:
        msgItem = bb == 0 ? QItem(new MsgDiscoveryTrk()) : QItem(new MsgDiscoveryTrk(bb));
        break;

    case ID_DISCOVERY_MON:
        msgItem = bb == 0 ? QItem(new MsgDiscoveryMon()) : QItem(new MsgDiscoveryMon(bb));
        break;

    case ID_SEND_COUNTER:
        msgItem = bb == 0 ? QItem(new MsgSendCounter()) : QItem(new MsgSendCounter(bb));
        break;

    case ID_PERIODIC_TIMER:
        msgItem = bb == 0 ? QItem(new MsgPeriodicTimer()) : QItem(new MsgPeriodicTimer(bb));
        break;
        
    case ID_MLT_STATUS:
        msgItem = bb == 0 ? QItem(new MsgMltStatus()) : QItem(new MsgMltStatus(bb));
        break;

    case ID_MLT_STATUS_REQUEST:
        msgItem = bb == 0 ? QItem(new MsgMltStatusRequest()) : QItem(new MsgMltStatusRequest(bb));
        break;

    case ID_G_UPDATE:
        msgItem = bb == 0 ? QItem(new MsgGUpdate()) : QItem(new MsgGUpdate(bb));
        break;

    case ID_G_UPDATE_ACK:
        msgItem = bb == 0 ? QItem(new MsgGUpdateAck()) : QItem(new MsgGUpdateAck(bb));
        break;

    case ID_LINK_RTT_REQUEST:
        msgItem = bb == 0 ? QItem(new MsgLinkRttRequest()) : QItem(new MsgLinkRttRequest(bb));
        break;

    case ID_LINK_RTT_ACK:
        msgItem = bb == 0 ? QItem(new MsgLinkRttAck()) : QItem(new MsgLinkRttAck(bb));
        break;

    case ID_LINK_RTT:
        msgItem = bb == 0 ? QItem(new MsgLinkRtt()) : QItem(new MsgLinkRtt(bb));
        break;

    case ID_SENSOR_REGULATOR_UPDATE:
        msgItem = bb == 0 ? QItem(new MsgSensorRegulatorUpdate()) : QItem(new MsgSensorRegulatorUpdate(bb));
        break;

    case ID_TRACKED_LOCATION:
        msgItem = bb == 0 ? QItem(new MsgTrackedLocation()) : QItem(new MsgTrackedLocation(bb));
        break;

    case ID_SIM_AREA:
        msgItem = bb == 0 ? QItem(new MsgSimArea()) : QItem(new MsgSimArea(bb));
        break;

    default:
        msgItem = bb == 0 ? QItem(new MsgNilContent()) : QItem(new MsgNilContent(bb));
        LOG(ERROR) << "Unhandle message. Code:" << messageCode;
        break;
    }
    return msgItem;
}
