#include "byteBuffer.h"
#include "msgBase.h"
#include "msgMltStatus.h"
#include <stdio.h>


QItem MsgMltStatus::clone()
{
    MsgMltStatus* msg = new MsgMltStatus();
    if ( this->mRawBuffer )
        msg->mRawBuffer = new ByteBuffer( *this->mRawBuffer );
    msg->mHeader = this->mHeader;
    msg->mData = this->mData;

    return QItem( msg );
}

MsgMltStatus::MsgMltStatus()
{
    memset( &mData, 0, sizeof(mData) );
    mHeader.messageCode = ID_MLT_STATUS;
    mHeader.messageLength = getSize();
}

MsgMltStatus::MsgMltStatus( ByteBuffer* inBuffer )
    :MsgBase( inBuffer )
{
    int bytePosition = 0;
    unpack( bytePosition );
}

MsgMltStatus::MsgMltStatus( const MsgMltStatus& rhs )
    :MsgBase( rhs )
{
    mData = rhs.mData;
}

MsgMltStatus& MsgMltStatus::operator=( const MsgMltStatus& rhs )
{
    MsgBase::operator = (rhs);
    mData = rhs.mData;
    return *this;
}

int MsgMltStatus::getSize()
{
    int totalSize = 0;
    totalSize += MsgBase::getSize();
    totalSize += mData.numberOfBytes;

    return totalSize;
}

ByteBuffer* MsgMltStatus::pack( int& bytePosition )
{
    MsgBase::pack( bytePosition );
    int variableSize = getSize() - sizeof(MSG_HEADER);
    mRawBuffer->appendByteArray( (char*)mData.payload, variableSize );
    return mRawBuffer;
}

void MsgMltStatus::unpack( int& bytePosition )
{
    MsgBase::unpack( bytePosition );
    int variableSize = mRawBuffer->getSize() - sizeof(MSG_HEADER);
    mData.numberOfBytes = variableSize;
    mRawBuffer->popByteArray( (char*)mData.payload, variableSize );
}

int MsgMltStatus::print()
{
    int charWritten = MsgBase::print();

    charWritten = snprintf( mPrintBuffer + charWritten, PRINT_BUFFER_SIZE,
        "PIPE_THROUGH_BYTES:%d",
        mData.numberOfBytes );

    LOG(DEBUG) << mPrintBuffer;
    return charWritten;
}
