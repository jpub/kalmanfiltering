#ifndef __MSG_NIL_CONTENT_H
#define __MSG_NIL_CONTENT_H

#include "msgBase.h"

class ByteBuffer;

class MsgNilContent : public MsgBase
{
public:
    ENABLE_EVENT_DISPATCH();

public:
    virtual QItem clone();

public:
    MsgNilContent();
    MsgNilContent( ByteBuffer* inBuffer );
    MsgNilContent( const MsgNilContent& rhs );
    MsgNilContent& operator=( const MsgNilContent& rhs );

    virtual int getSize();
    virtual ByteBuffer* pack( int& bytePosition );
    virtual void unpack( int& bytePosition );
    virtual int print();

    NIL_CONTENT* data()
    {
        return &mData;
    }
    
private:
    NIL_CONTENT mData;
};

#endif
