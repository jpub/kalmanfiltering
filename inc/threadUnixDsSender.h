#ifndef __THREAD_UNIX_DS_SENDER_H
#define __THREAD_UNIX_DS_SENDER_H

#include <arpa/inet.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <string>
#include "threadBaseSender.h"

class ThreadUnixDsSender : public ThreadBaseSender
{
public:
    ThreadUnixDsSender( MsgQueue<QItem>* outgoingQueue,
    					int destinationId,
                        std::string remotePath );

    virtual bool sendMessage( MsgBase& );
    virtual void handleSuccessfulSending( MsgBase& msg );

private:
    std::string mRemotePath;
    int mSockfd;
    struct sockaddr_un mRemoteAddr;
};

#endif
