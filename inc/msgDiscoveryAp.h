#ifndef __MSG_DISCOVERY_AP_H
#define __MSG_DISCOVERY_AP_H

#include "msgBase.h"

class ByteBuffer;

class MsgDiscoveryAp : public MsgBase
{
public:
    ENABLE_EVENT_DISPATCH();

public:
    virtual QItem clone();

public:
    MsgDiscoveryAp();
    MsgDiscoveryAp( ByteBuffer* inBuffer );
    MsgDiscoveryAp( const MsgDiscoveryAp& rhs );
    MsgDiscoveryAp& operator=( const MsgDiscoveryAp& rhs );

    virtual int getSize();
    virtual ByteBuffer* pack( int& bytePosition );
    virtual void unpack( int& bytePosition );
    virtual int print();

    DISCOVERY_AP* data()
    {
        return &mData;
    }
    
private:
    DISCOVERY_AP mData;
};

#endif
