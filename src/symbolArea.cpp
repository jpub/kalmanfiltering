#include "limits.h"
#include "symbolArea.h"
#include "logger.h"


SymbolArea::SymbolArea()
{
	mName = "";
}

SymbolArea::SymbolArea( string name, vector<POINTF_2D> pointList )
{
	mName = name;
	mPointList = pointList;

	mMinX = INT_MAX;
	mMinY = INT_MAX;
	mMaxX = INT_MIN;
	mMaxY = INT_MIN;

	for ( unsigned int i=0; i<pointList.size(); ++i )
	{
		mMinX = pointList[i].x < mMinX ? pointList[i].x : mMinX;
		mMinY = pointList[i].y < mMinY ? pointList[i].y : mMinY;

		mMaxX = pointList[i].x > mMaxX ? pointList[i].x : mMaxX;
		mMaxY = pointList[i].y > mMaxY ? pointList[i].y : mMaxY;
	}
}

void SymbolArea::update( string deviceId, int posX, int posY )
{
	if ( isInside(posX, posY) )
	{
		addDevice( deviceId );
	}
	else
	{
		removeDevice( deviceId );
	}
}

string SymbolArea::getName()
{
	return mName;
}

unsigned int SymbolArea::getCount()
{
	return mDeviceMap.size();
}

vector<POINTF_2D> SymbolArea::getPointVec()
{
	return mPointList;
}

bool SymbolArea::isInside( int posX, int posY )
{
	if ( (mMinX <= posX && posX <= mMaxX) &&
		 (mMinY <= posY && posY <= mMaxY) )
	{
		return true;
	}
	return false;
}

void SymbolArea::addDevice( string deviceId )
{
	if ( mDeviceMap.find(deviceId) == mDeviceMap.end() )
	{
		mDeviceMap[deviceId] = 0;
	}
}

void SymbolArea::removeDevice( string deviceId )
{
	map<string,int>::iterator it = mDeviceMap.find( deviceId );
	if ( it != mDeviceMap.end() )
	{
		mDeviceMap.erase( it );
	}
}
