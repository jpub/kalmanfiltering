#ifndef __EVENT_BASE_H
#define __EVENT_BASE_H

#include "logger.h"
#include "eventHandlerBase.h"
#include "smartPointer.h"

#define ENABLE_EVENT_DISPATCH()									\
virtual bool													\
dispatchToInternal( class EventHandlerBase & handler )			\
{																\
	return dispatchToInternalImplementaion( *this, handler );	\
}																\


class MsgBase;
typedef SmartPointer<MsgBase> QItem;


class EventBase
{
public:

    EventBase() {}
    virtual ~EventBase() {}
	virtual bool dispatchToInternal( EventHandlerBase& handler ) = 0;

protected:
	
	template< typename EventType >
	bool dispatchToInternalImplementaion( EventType& incomingEvent, EventHandlerBase & handler )
	{
		EventHandler< EventType >* concreteHandler =
			dynamic_cast< EventHandler<EventType>* >( &handler );

		if ( ! concreteHandler )
		{
			LOG(WARNING) << "Unable to find suitable event handler! Code:" << incomingEvent.header()->messageCode;
			return false;
		}

		return concreteHandler->handle( incomingEvent );
	}
};

#endif
