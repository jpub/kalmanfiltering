#!/bin/bash

ls ../bin/*.sh | xargs -i echo {} | cut -c 8- | xargs -i ln -s ../bin/{} {}
ls ../bin/*.conf | xargs -i echo {} | cut -c 8- | xargs -i ln -s ../bin/{} {}
ls ../bin/*.xml | xargs -i echo {} | cut -c 8- | xargs -i ln -s ../bin/{} {}
