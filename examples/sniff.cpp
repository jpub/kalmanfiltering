#include <stdio.h>
#include <pcap.h>
#include <time.h>

time_t rawTime;
time_t prevTime = 0;
pcap_t* handler;
struct pcap_stat ps;

struct WIFI_STRUCT
{
    char garbage1[14];
    signed char rssi;
    char garbage2[3]; // end of radiotap header
    char garbage3[10];
    char srcMac[6];
};

typedef WIFI_STRUCT WIFI_STRUCT;

void incomingPackets(
    unsigned char* useless,
    const struct pcap_pkthdr* pkthdr,
    const unsigned char* packet )
{
    static int count = 0;

    /*
    WIFI_STRUCT* wifiStruct = (WIFI_STRUCT*)packet;
    printf( "%02x:%02x:%02x:%02x:%02x:%02x -- %d\n",
            wifiStruct->srcMac[0],
            wifiStruct->srcMac[1],
            wifiStruct->srcMac[2],
            wifiStruct->srcMac[3],
            wifiStruct->srcMac[4],
            wifiStruct->srcMac[5],
            wifiStruct->rssi );
    */
    
    
    time( &rawTime );
    ++count;
    if ( rawTime > prevTime )
    {
        pcap_stats( handler, &ps );
        printf( "time diff=%ld - total=%d\n", rawTime-prevTime, count );
        printf( "stats -- recv=%d   drop=%d   ifdrop=%d\n\n",
                ps.ps_recv, ps.ps_drop, ps.ps_ifdrop );
        prevTime = rawTime;
        count = 0;
    }
    
    
    //printf( "%d - %d\n", rawTime, pkthdr->len );
}

int main( void )
{
    char dev[] = "wlan0";
    char errbuf[PCAP_ERRBUF_SIZE];
    pcap_if_t* alldevs;
    
    
    if ( pcap_findalldevs(&alldevs, errbuf) == -1 )
    {
        printf( "Error getting %s\n", errbuf );
        return -1;
    }
    
    int i=0;
    for ( pcap_if_t* d=alldevs; d != NULL; d=d->next )
    {
        printf( "%d. %s", ++i, d->name );
        if ( d->name )
            printf( " (%s)\n", d->name );
        else
            printf( " (no description\n");
    }
    printf("%d\n", i);

    /*
    char dev[] = "wlan0";
    pcap_t* descr = pcap_open_live( dev, BUFSIZ, 1, -1, errbuf );
    if ( descr == NULL )
    {
        printf( "pcap_open_live fail due to [%s]\n", errbuf );
        return -1;
    }
    pcap_loop( descr, -1, incomingPackets, NULL );
    */

    handler = pcap_create( dev, errbuf );
    if ( handler == NULL )
    {
        printf( "pcap_create fail [%s]\n", errbuf );
        return -1;
    }

    if ( pcap_set_rfmon(handler, 1) == 0 )
    {
        printf( "monitor mode enabled\n" );       
    }
    else
    {
        printf( "fail to set monitor mode\n" );
        return -1;
    }

    int status = 0;
    //status = pcap_set_snaplen( handler, 0 );
    //printf( "status %d\n", status );
        
    status = pcap_set_promisc( handler, 1 );
    printf( "set promisc status %d\n", status );

    status = pcap_set_timeout( handler, 1000 );
    printf( "set timeout status %d\n", status );

    struct bpf_program fp;
    bpf_u_int32 pMask;
    bpf_u_int32 pNet;
    char filterExp[] = "wlan subtype probe-req";
    status = pcap_lookupnet( dev, &pNet, &pMask, errbuf );
    printf( "lookup net status %d\n", status );
    
    //status = pcap_compile( handler, &fp, filterExp, 0, pNet );
    //printf( "compile filter status %d\n", status );

    //status = pcap_setfilter( handler, &fp );
    //printf( "set filter status %d\n", status );
    
    status = pcap_activate( handler );
    printf( "activate sniff status %d\n", status );

    status = pcap_compile( handler, &fp, filterExp, 0, pNet );
    printf( "compile filter status %d\n", status );

    status = pcap_setfilter( handler, &fp );
    printf( "set filter status %d\n", status );
    
    pcap_loop( handler, -1, incomingPackets, NULL );
    return 0;

    // wlan.fc.type_subtype==4
    // wlan subtype probe-req


}
