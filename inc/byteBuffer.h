#ifndef __BYTE_BUFFER_H
#define __BYTE_BUFFER_H

// for memcpy
#include <string.h>


class ByteBuffer
{
public:
    ByteBuffer( int size );
    ByteBuffer( const ByteBuffer& rhs );
    ByteBuffer& operator=( const ByteBuffer& rhs );
    ~ByteBuffer();

    void resetPointer();
    char* getDataPtr();
    int getSize();

    void appendByte( unsigned char data );
    void appendSByte( char data );
    void appendUShort( unsigned short data );
    void appendShort( short data );
    void appendUInt( unsigned int data );
    void appendInt( int data );
    void appendFloat( float data );
    void appendDouble( double data );
    void appendByteArray( char* data, int length );

    unsigned char popByte();
    char popSByte();
    unsigned short popUShort();
    short popShort();
    unsigned int popUInt();
    int popInt();
    float popFloat();
    double popDouble();
    void popByteArray( char* data, int length );
    
    int putByte( unsigned char data, int index );
    int putSByte( char data, int index );
    int putUShort( unsigned short data, int index );
    int putShort( short data, int index );
    int putUInt( unsigned int data, int index );
    int putInt( int data, int index );
    int putFloat( float data, int index );
    int putDouble( double data, int index );
    int putByteArray( char* data, int length, int index );

    unsigned char getByte( int index );
    char getSByte( int index );
    unsigned short getUShort( int index );
    short getShort( int index );
    unsigned int getUInt( int index );
    int getInt( int index );
    float getFloat( int index );
    double getDouble( int index );
    void getByteArray( char* data, int length, int index );

private:
    int mBufferSize;
    char* mBuffer;
    char* mCurrentPointer;

private:

    template<typename T> void append( T data )
    {
        memcpy( mCurrentPointer, &data, sizeof(data) );
        mCurrentPointer += sizeof( data );
    }

    template<typename T> T pop()
    {
        T value = *((T*)(mCurrentPointer));
        mCurrentPointer += sizeof(T);
        return value;
    }
    
    template<typename T> int put( T data, int index )
    {
        memcpy( mBuffer+index, &data, sizeof(data) );
        return index + sizeof( data );
    }

    template<typename T> T get( int index )
    {
        return *((T*)(mBuffer + index));
    }
};

#endif
