#ifndef __THREAD_UDP_SEDNER_H
#define __THREAD_UDP_SEDNER_H

#include <arpa/inet.h>
#include "threadBaseSender.h"


class ThreadUdpSender : public ThreadBaseSender
{
public:
    ThreadUdpSender( MsgQueue<QItem>* outgoingQueue,
                     int destinationId,
                     const char* destIp,
                     int destPort,
                     const char* localBindIp,
                     int localBindPort );

    virtual bool sendMessage( MsgBase& );    

private:
    const char* mDestIp;
    int mDestPort;
    const char* mLocalBindIp;
    int mLocalBindPort;

    int mSockfd;
    struct addrinfo mHints;
    struct addrinfo* mOwnAddr;
    struct sockaddr_in mDestAddr;
    int mSlen;
};

#endif
