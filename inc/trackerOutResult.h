#ifndef __TRACKER_OUT_RESULT_H
#define __TRACKER_OUT_RESULT_H

#include <string>

using std::string;


class TrackerOutResult
{
public:
	bool getIsTrackedSuccess();
	string getDeviceId();
	double getTrackerLongitude();
	double getTrackerLatitude();

	void setIsTrackedSuccess( bool isTrackedSuccess );
	void setDeviceId( string deviceId );
	void setTrackerLongitude( double longitude );
	void setTrackerLatitude( double latitude );

private:
	bool mIsTrackedSuccess;
	string mDeviceId;
	double mLongitude;
	double mLatitude;
};

#endif