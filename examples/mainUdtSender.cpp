#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netdb.h>
#include "udt.h"

int main( int argc, char** argv )
{
    if ( argc != 6 )
    {
        printf( "usage: ./mainUtdSender <IP> <PORT> <TOTAL> <DELAY> <TTL>\n" );
        return -1;
    }

    int totalMsg = atoi( argv[3] );
    int delayMicro = atoi( argv[4] );
    int ttl = atoi( argv[5] );
    
    struct addrinfo hints, *local, *peer;

    memset(&hints, 0, sizeof(struct addrinfo));
    
    hints.ai_flags = AI_PASSIVE;
    hints.ai_family = AF_INET;
    //hints.ai_socktype = SOCK_STREAM;
    hints.ai_socktype = SOCK_DGRAM;
    
    if (0 != getaddrinfo(NULL, "0", &hints, &local))
    {
        printf( "incorrect network address.\n" );
        return 0;
    }
    printf( "1\n" );
    UDTSOCKET client = UDT::socket(local->ai_family, local->ai_socktype, local->ai_protocol);

    //freeaddrinfo(local);
    printf( "2\n" );
    if (0 != getaddrinfo(argv[1], argv[2], &hints, &peer))
    {
        printf( "incorrect server/peer address. %s : %s\n",argv[1], argv[2] );
        return 0;
    }
    printf( "3\n" );
    // connect to the server, implict bind
    while (UDT::ERROR == UDT::connect(client, peer->ai_addr, peer->ai_addrlen))
    {
        printf( "connect: %s\n", UDT::getlasterror().getErrorMessage() );
        printf( "retrying in 1 sec\n" );
        usleep( 1000000 );
    }
    printf( "4\n" );
    //freeaddrinfo(peer);

    int size = 168;
    char* data = new char[size];
    int counter = 0;
    
    while ( true )
    {
        if ( counter >= totalMsg )
        {
            printf( "already send finish...\n" );
            usleep( 10000000 );
            continue;
        }
        
        ++counter;
        memset( data, 0, size );
        sprintf( data, "%s %d", "Test reliability", counter );
        int bytesSend = UDT::sendmsg( client, data, size, ttl );
        if ( bytesSend == -1 )
        {
            printf( "%d -- send [%d] error: %s\n", counter, bytesSend, UDT::getlasterror().getErrorMessage() );
            client = UDT::socket(local->ai_family, local->ai_socktype, local->ai_protocol);
            while (UDT::ERROR == UDT::connect(client, peer->ai_addr, peer->ai_addrlen))
            {
                printf( "connect: %s\n", UDT::getlasterror().getErrorMessage() );
                printf( "retrying in 1 sec\n" );
                usleep( 1000000 );
            }
        }
        else if ( bytesSend == 0 )
        {
            printf( "0 bytes send\n" );
        }
        else
        {
            printf( "%d -- send [%d] -- %s\n", counter, bytesSend, data );
        }
        usleep( delayMicro );
    }
    return 0;
}
