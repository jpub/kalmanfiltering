#include <stdio.h>
#include <iostream>
#include <sstream>
#include "mgrConfigDynamic.h"
#include "logger.h"

MgrConfigDynamic* MgrConfigDynamic::msInstance = 0;

using namespace std;

MgrConfigDynamic::MgrConfigDynamic()
{
}

MgrConfigDynamic& MgrConfigDynamic::getInstance()
{
	if ( msInstance == 0 )
	{
		msInstance = new MgrConfigDynamic();
	}
	return *msInstance;
}

bool MgrConfigDynamic::init( const char* filename )
{
	if ( mXmlDoc.LoadFile(filename) )
	{
		LOG(FATAL) << "Fail to load config file:" << filename;		
		return false;
	}
	mFilename = filename;
	mRoot = mXmlDoc.FirstChildElement( "Settings" );
	
	mThresholdSr = atoi( getElementText("THRESHOLD_SR") );
	mThresholdMr = atoi( getElementText("THRESHOLD_MR") );
	mLrCutoff = atoi( getElementText("LR_CUTOFF") );
	mR0 = atoi( getElementText("R0") );
	mD0 = atoi( getElementText("D0") );
	mApPosX = atoi( getElementText("AP_POS_X") );
	mApPosY = atoi( getElementText("AP_POS_Y") );
	mApPosZ = atoi( getElementText("AP_POS_Z") );

	LOG(INFO) << "THRESHOLD_SR:" << mThresholdSr;
	LOG(INFO) << "THRESHOLD_MR:" << mThresholdMr;
	LOG(INFO) << "LR_CUTOFF:" << mLrCutoff;
	LOG(INFO) << "R0:" << mR0;
	LOG(INFO) << "D0:" << mD0;
	LOG(INFO) << "AP_POS_X:" << mApPosX;
	LOG(INFO) << "AP_POS_Y:" << mApPosY;
	LOG(INFO) << "AP_POS_Z:" << mApPosZ;

	return true;
}

void MgrConfigDynamic::writeToFile()
{
	mXmlDoc.SaveFile( mFilename.c_str() );
}

const char* MgrConfigDynamic::getElementText( const char* elementName )
{
	mElement = mRoot->FirstChildElement( elementName );
	return mElement->GetText();
}

int MgrConfigDynamic::getThresholdSr()
{
	return mThresholdSr;
}
int MgrConfigDynamic::getThresholdMr()
{
	return mThresholdMr;
}

int MgrConfigDynamic::getLrCutoff()
{
	return mLrCutoff;
}

int MgrConfigDynamic::getR0()
{
	return mR0;
}

int MgrConfigDynamic::getD0()
{
	return mD0;
}

int MgrConfigDynamic::getApPosX()
{
	return mApPosX;
}

int MgrConfigDynamic::getApPosY()
{
	return mApPosY;
}

int MgrConfigDynamic::getApPosZ()
{
	return mApPosZ;
}

void MgrConfigDynamic::setThresholdSr( int thresholdSr )
{
	mThresholdSr = thresholdSr;
	mRoot->FirstChildElement( "THRESHOLD_SR" )->SetText( thresholdSr );
}

void MgrConfigDynamic::setThresholdMr( int thresholdMr )
{
	mThresholdMr = thresholdMr;
	mRoot->FirstChildElement( "THRESHOLD_MR" )->SetText( thresholdMr );
}

void MgrConfigDynamic::setLrCutoff( int lrCutoff )
{
	mLrCutoff = lrCutoff;
	mRoot->FirstChildElement( "LR_CUTOFF" )->SetText( lrCutoff );
}

void MgrConfigDynamic::setR0( int r0 )
{
	mR0 = r0;
	mRoot->FirstChildElement( "R0" )->SetText( r0 );
}

void MgrConfigDynamic::setD0( int d0 )
{
	mD0 = d0;
	mRoot->FirstChildElement( "D0" )->SetText( d0 );
}

void MgrConfigDynamic::setApPosX( int posX )
{
	mApPosX = posX;
	mRoot->FirstChildElement( "AP_POS_X" )->SetText( posX );
}

void MgrConfigDynamic::setApPosY( int posY )
{
	mApPosY = posY;
	mRoot->FirstChildElement( "AP_POS_Y" )->SetText( posY );
}

void MgrConfigDynamic::setApPosZ( int posZ )
{
	mApPosZ = posZ;
	mRoot->FirstChildElement( "AP_POS_Z" )->SetText( posZ );
}
