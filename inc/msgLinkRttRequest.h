#ifndef __MSG_LINK_RTT_REQUEST_H
#define __MSG_LINK_RTT_REQUEST_H

#include "msgBase.h"

class ByteBuffer;

class MsgLinkRttRequest : public MsgBase
{
public:
    ENABLE_EVENT_DISPATCH();

public:
    virtual QItem clone();

public:
    MsgLinkRttRequest();
    MsgLinkRttRequest( ByteBuffer* inBuffer );
    MsgLinkRttRequest( const MsgLinkRttRequest& rhs );
    MsgLinkRttRequest& operator=( const MsgLinkRttRequest& rhs );

    virtual int getSize();
    virtual ByteBuffer* pack( int& bytePosition );
    virtual void unpack( int& bytePosition );
    virtual int print();

    LINK_RTT_REQUEST* data()
    {
        return &mData;
    }
    
private:
    LINK_RTT_REQUEST mData;
};

#endif
