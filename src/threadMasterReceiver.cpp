#include "threadMasterReceiver.h"

ThreadMasterReceiver::ThreadMasterReceiver( MsgQueue<QItem>* incomingQueue, EventHandlerBase* eventHandlerBase )
	: ThreadBase(incomingQueue),
	  mEventHandlerBase(eventHandlerBase)
{
}

void* ThreadMasterReceiver::run( void* arg )
{
	while( true )
	{
		QItem qitem = mQueue->remove();		
		qitem->dispatchToInternal( *mEventHandlerBase );		
	}

	char* nullArg = 0;
	return nullArg;
}