#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "mgrReporter.h"
#include "mgrConfigStatic.h"
#include "threadRUdpApToMonSender.h"
#include "logger.h"


ThreadRUdpApToMonSender::ThreadRUdpApToMonSender(
    MsgQueue<QItem>* outgoingQueue,
    int destinationId,
    const char* destIp,
    int destPort,
    const char* localBindIp,
    int localBindPort,
    int ttl )
    : ThreadRUdpBaseSender(outgoingQueue, destinationId, destIp, destPort, localBindIp, localBindPort, ttl)
{
}

void ThreadRUdpApToMonSender::handleDisconnection()
{
    LOG(WARNING) << "Connection with peer MON " << mDestIp << " lost:" << UDT::getlasterror().getErrorMessage();
    MgrReporter::getInstance().removeRdupMonSendingQueue( mDestIp );

    MgrConfigStatic::getInstance().setMonitorId( 0 );
    MgrConfigStatic::getInstance().setMonitorAddress( "" );
}
