#include "mgrReporter.h"
#include "logger.h"

MgrReporter* MgrReporter::msInstance = 0;

using namespace std;


MgrReporter::MgrReporter()
{
	pthread_mutex_init( &mMutex, NULL );
}

MgrReporter& MgrReporter::getInstance()
{
	if ( msInstance == 0 )
	{
		msInstance = new MgrReporter();
	}
	return *msInstance;
}

void MgrReporter::init( MsgQueue<QItem>* udpBcQueue, MsgQueue<QItem>* mltQueue, MsgQueue<QItem>* exfQueue, MsgQueue<QItem>* internalQueue )
{
	mUdpBcQueue = udpBcQueue;
	mMltQueue = mltQueue;
	mExfQueue = exfQueue;
	mInternalQueue = internalQueue;
}

void MgrReporter::udpBroadcastToAp( QItem qitem )
{
	MutexLocker mutexLock( &mMutex );

	mUdpBcQueue->add( qitem );
}

void MgrReporter::sendToMlt( QItem qitem )
{
	MutexLocker mutexLock( &mMutex );

	mMltQueue->add( qitem );
}

void MgrReporter::sendToExf( QItem qitem )
{
	MutexLocker mutexLock( &mMutex );

	mExfQueue->add( qitem );
}

void MgrReporter::sendToInternal( QItem qitem )
{
	MutexLocker mutexLock( &mMutex );

	mInternalQueue->add( qitem );
}

bool MgrReporter::isApRudpConnected( string destIp )
{
	MutexLocker mutexLock( &mMutex );

	if ( mRUdpBcQueue.find(destIp) == mRUdpBcQueue.end() )
		return false;
	
	LOG(DEBUG) << "AP " << destIp << " already connected";
	return true;
}

bool MgrReporter::addRudpApSendingQueue( string destIp, MsgQueue<QItem>* rdupOutoingQueue )
{
	MutexLocker mutexLock( &mMutex );

	if ( mRUdpBcQueue.find(destIp) != mRUdpBcQueue.end() )
	{
		LOG(WARNING) << "AP " << destIp << " RUDP Destination queue already exist!!";
		return false;
	}

	mRUdpBcQueue[destIp] = rdupOutoingQueue;
	return true;
}

bool MgrReporter::removeRdupApSendingQueue( string destIp )
{
	MutexLocker mutexLock( &mMutex );

	map< string, MsgQueue<QItem>* >::iterator it;
	it = mRUdpBcQueue.find( destIp );

	if ( it == mRUdpBcQueue.end() )
	{
		LOG(WARNING) << "AP " << destIp << " RUDP Destination queue does not exist!!";
		return false;
	}
	
	delete it->second;
	mRUdpBcQueue.erase( it );	
	LOG(DEBUG) << "AP " << destIp << " RUDP queue remove";
	
	return true;
}

bool MgrReporter::rudpBroadcastToAp( QItem qitem )
{
	MutexLocker mutexLock( &mMutex );

	map< string, MsgQueue<QItem>* >::iterator it;
	for ( it = mRUdpBcQueue.begin(); it != mRUdpBcQueue.end(); ++it )
	{
		it->second->add( qitem->clone() );
	}
	return true;
}

bool MgrReporter::rudpUnicastToAp( string destIp, QItem qitem )
{
	MutexLocker mutexLock( &mMutex );

	if ( mRUdpBcQueue.find(destIp) == mRUdpBcQueue.end() )
	{
		LOG(WARNING) << "AP " << destIp << " RUDP Destination queue does not exist!!";
		return false;
	}

	mRUdpBcQueue[destIp]->add( qitem );

	return true;
}

bool MgrReporter::isTrkRudpConnected( string destIp )
{
	MutexLocker mutexLock( &mMutex );

	if ( mRUdpTrkQueue.find(destIp) == mRUdpTrkQueue.end() )
	{
		if ( mRUdpTrkQueue.size() == 0 )
			return false;
		else
		{
			map< string, MsgQueue<QItem>* >::iterator it;
			it = mRUdpTrkQueue.begin();
			LOG(DEBUG) << "Already has a connected TRK " << it->first << " - disallow " << destIp;
			return true;
		}
	}

	LOG(DEBUG) << "TRK " << destIp << " already connected";
	return true;
}

bool MgrReporter::addRudpTrkSendingQueue( string destIp, MsgQueue<QItem>* rdupOutoingQueue )
{
	MutexLocker mutexLock( &mMutex );

	if ( mRUdpTrkQueue.find(destIp) != mRUdpTrkQueue.end() )
	{
		LOG(WARNING) << "TRK " << destIp << " RUDP Destination queue already exist!!";
		return false;
	}

	mRUdpTrkQueue[destIp] = rdupOutoingQueue;
	return true;
}

bool MgrReporter::removeRdupTrkSendingQueue( string destIp )
{
	MutexLocker mutexLock( &mMutex );

	map< string, MsgQueue<QItem>* >::iterator it;
	it = mRUdpTrkQueue.find( destIp );

	if ( it == mRUdpTrkQueue.end() )
	{
		LOG(WARNING) << "TRK " << destIp << " RUDP Destination queue does not exist!!";
		return false;
	}
	
	delete it->second;
	mRUdpTrkQueue.erase( it );	

	return true;
}

bool MgrReporter::rudpUnicastToTrk( QItem qitem )
{
	MutexLocker mutexLock( &mMutex );

	if ( mRUdpTrkQueue.size() == 0 )
	{
		LOG(WARNING) << "TRK RUDP Destination queue does not exist!!";
		return false;
	}

	map< string, MsgQueue<QItem>* >::iterator it = mRUdpTrkQueue.begin();
	it->second->add( qitem );
	
	return true;
}

bool MgrReporter::isMonRudpConnected()
{
	MutexLocker mutexLock( &mMutex );

	if ( mRUdpMonQueue.size() == 1 )
	{
		//LOG(DEBUG) << "MON already connected";
		return true;
	}
	return false;
	/*
	if ( mRUdpMonQueue.find(destIp) == mRUdpMonQueue.end() )
	{
		if ( mRUdpMonQueue.size() == 0 )
			return false;
		else
		{
			map< std::string, MsgQueue<QItem>* >::iterator it;
			it = mRUdpMonQueue.begin();
			LOG(DEBUG) << "Already has a connected MON " << it->first << " - disallow " << destIp;
			return true;
		}
	}

	LOG(DEBUG) << "MON " << destIp << " already connected";
	return true;
	*/
}

bool MgrReporter::addRudpMonSendingQueue( string destIp, MsgQueue<QItem>* rdupOutoingQueue )
{
	MutexLocker mutexLock( &mMutex );

	if ( mRUdpMonQueue.find(destIp) != mRUdpMonQueue.end() )
	{
		LOG(WARNING) << "MON " << destIp << " RUDP Destination queue already exist!!";
		return false;
	}

	mRUdpMonQueue[destIp] = rdupOutoingQueue;
	return true;
}

bool MgrReporter::removeRdupMonSendingQueue( string destIp )
{
	MutexLocker mutexLock( &mMutex );

	map< string, MsgQueue<QItem>* >::iterator it;
	it = mRUdpMonQueue.find( destIp );

	if ( it == mRUdpMonQueue.end() )
	{
		LOG(WARNING) << "MON " << destIp << " RUDP Destination queue does not exist!!";
		return false;
	}
		
	delete it->second;
	mRUdpMonQueue.erase( it );

	return true;
}

bool MgrReporter::rudpUnicastToMon( QItem qitem )
{
	MutexLocker mutexLock( &mMutex );

	if ( mRUdpMonQueue.size() == 0 )
	{		
		return false;
	}

	int msgCode = qitem->header()->messageCode;
	if ( mFilterCodeMap.find(msgCode) != mFilterCodeMap.end() )
		return false;

	map< string, MsgQueue<QItem>* >::iterator it = mRUdpMonQueue.begin();
	it->second->add( qitem );
	return true;
}

void MgrReporter::updateFilterList( vector<int> filterCodeVec )
{
	MutexLocker mutexLock( &mMutex );
	
	mFilterCodeMap.clear();
	vector<int>::iterator it;

	for ( it=filterCodeVec.begin(); it!=filterCodeVec.end(); ++it )
		mFilterCodeMap[*it] = *it;
}