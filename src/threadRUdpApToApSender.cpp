#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "__DefineVectorSize.h"
#include "mgrReporter.h"

#include "msgSendCounter.h"
#include "threadRUdpApToApSender.h"
#include "logger.h"


ThreadRUdpApToApSender::ThreadRUdpApToApSender(
    MsgQueue<QItem>* outgoingQueue,
    int destinationId,
    const char* destIp,
    int destPort,
    const char* localBindIp,
    int localBindPort,
    int ttl,
    int linkQualityCounter )
    : ThreadRUdpBaseSender(outgoingQueue, destinationId, destIp, destPort, localBindIp, localBindPort, ttl),
      mLinkQualityCounter(linkQualityCounter)
{
}

void ThreadRUdpApToApSender::handleDisconnection()
{
    LOG(WARNING) << "Connection with peer AP " << mDestIp << " lost:" << UDT::getlasterror().getErrorMessage();
    MgrReporter::getInstance().removeRdupApSendingQueue( mDestIp );
}

void ThreadRUdpApToApSender::handleSuccessfulSending( MsgBase& msg )
{
    if ( MgrReporter::getInstance().isMonRudpConnected() )
        MgrReporter::getInstance().rudpUnicastToMon( msg.clone() );

    if ( msg.header()->messageCode == ID_SENSOR_UPDATE )
    {
        ++mCounter;
        if ( mCounter % mLinkQualityCounter == 0 )
        {
            LOG(DEBUG) << "UDT send counter message to AP " << mDestIp;
            QItem qitem = mFactoryMsg.get( ID_SEND_COUNTER );  
            MsgSendCounter* msg = (MsgSendCounter*)(qitem.getDataPointer());        
            msg->data()->sendCounter = mCounter;
            MgrReporter::getInstance().rudpUnicastToAp( mDestIp, qitem );
        }
    }
}
