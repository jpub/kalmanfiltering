#include "byteBuffer.h"
#include "msgBase.h"
#include "msgDiscoveryMon.h"
#include <stdio.h>


QItem MsgDiscoveryMon::clone()
{
    MsgDiscoveryMon* msg = new MsgDiscoveryMon();
    if ( this->mRawBuffer )
        msg->mRawBuffer = new ByteBuffer( *this->mRawBuffer );
    msg->mHeader = this->mHeader;
    msg->mData = this->mData;

    return QItem( msg );
}

MsgDiscoveryMon::MsgDiscoveryMon()
{
    memset( &mData, 0, sizeof(mData) );
    mHeader.messageCode = ID_DISCOVERY_MON;
    mHeader.messageLength = getSize();
}

MsgDiscoveryMon::MsgDiscoveryMon( ByteBuffer* inBuffer )
    :MsgBase( inBuffer )
{
    int bytePosition = 0;
    unpack( bytePosition );
}

MsgDiscoveryMon::MsgDiscoveryMon( const MsgDiscoveryMon& rhs )
    :MsgBase( rhs )
{
    mData = rhs.mData;
}

MsgDiscoveryMon& MsgDiscoveryMon::operator=( const MsgDiscoveryMon& rhs )
{
    MsgBase::operator = (rhs);
    mData = rhs.mData;
    return *this;
}

int MsgDiscoveryMon::getSize()
{
    int totalSize = 0;
    totalSize += MsgBase::getSize();
    totalSize += sizeof( mData );
    return totalSize;
}

ByteBuffer* MsgDiscoveryMon::pack( int& bytePosition )
{
    MsgBase::pack( bytePosition );
    mRawBuffer->appendByteArray( (char*)&mData, sizeof(mData) );
    return mRawBuffer;
}

void MsgDiscoveryMon::unpack( int& bytePosition )
{
    MsgBase::unpack( bytePosition );
    mRawBuffer->popByteArray( (char*)&mData, sizeof(mData) );
}

int MsgDiscoveryMon::print()
{
    int charWritten = MsgBase::print();

    charWritten = snprintf( mPrintBuffer + charWritten, PRINT_BUFFER_SIZE,
        "SrcIP:%.*s",
        __ARRAY_20_LENGTH, mData.srcAddress );

    LOG(DEBUG) << mPrintBuffer;
    return charWritten;
}
