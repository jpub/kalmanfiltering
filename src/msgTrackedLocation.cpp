#include "byteBuffer.h"
#include "msgBase.h"
#include "msgTrackedLocation.h"
#include <stdio.h>


QItem MsgTrackedLocation::clone()
{
    MsgTrackedLocation* msg = new MsgTrackedLocation();
    if ( this->mRawBuffer )
        msg->mRawBuffer = new ByteBuffer( *this->mRawBuffer );
    msg->mHeader = this->mHeader;
    msg->mData = this->mData;

    return QItem( msg );
}

MsgTrackedLocation::MsgTrackedLocation()
{
    memset( &mData, 0, sizeof(mData) );
    mHeader.messageCode = ID_TRACKED_LOCATION;
    mHeader.messageLength = getSize();
}

MsgTrackedLocation::MsgTrackedLocation( ByteBuffer* inBuffer )
    :MsgBase( inBuffer )
{
    int bytePosition = 0;
    unpack( bytePosition );
}

MsgTrackedLocation::MsgTrackedLocation( const MsgTrackedLocation& rhs )
    :MsgBase( rhs )
{
    mData = rhs.mData;
}

MsgTrackedLocation& MsgTrackedLocation::operator=( const MsgTrackedLocation& rhs )
{
    MsgBase::operator = (rhs);
    mData = rhs.mData;
    return *this;
}

int MsgTrackedLocation::getSize()
{
    int totalSize = 0;
    totalSize += MsgBase::getSize();
    totalSize += sizeof( mData );
    return totalSize;
}

ByteBuffer* MsgTrackedLocation::pack( int& bytePosition )
{
    MsgBase::pack( bytePosition );
    mRawBuffer->appendByteArray( (char*)&mData, sizeof(mData) );
    return mRawBuffer;
}

void MsgTrackedLocation::unpack( int& bytePosition )
{
    MsgBase::unpack( bytePosition );
    mRawBuffer->popByteArray( (char*)&mData, sizeof(mData) );
}

int MsgTrackedLocation::print()
{
    int charWritten = MsgBase::print();

    charWritten = snprintf( mPrintBuffer + charWritten, PRINT_BUFFER_SIZE,
        "MacAdd:%02x%02x%02x%02x%02x%02x PosX:%d PosY:%d PosZ:%d AngleTrkActive:%d DistTrkActive:%d",
        mData.macAddress[0],
        mData.macAddress[1],
        mData.macAddress[2],
        mData.macAddress[3],
        mData.macAddress[4],
        mData.macAddress[5],
        mData.positionX,
        mData.positionY,
        mData.positionZ,
        mData.angleTrackerActive,
        mData.distanceTrackerActive );

    LOG(DEBUG) << mPrintBuffer;
    return charWritten;
}
