#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "mgrReporter.h"
#include "mgrConfigStatic.h"
#include "threadRUdpApToTrkSender.h"
#include "logger.h"


ThreadRUdpApToTrkSender::ThreadRUdpApToTrkSender(
    MsgQueue<QItem>* outgoingQueue,
    int destinationId,
    const char* destIp,
    int destPort,
    const char* localBindIp,
    int localBindPort,
    int ttl )
    : ThreadRUdpBaseSender(outgoingQueue, destinationId, destIp, destPort, localBindIp, localBindPort, ttl)
{
}

void ThreadRUdpApToTrkSender::handleDisconnection()
{
    LOG(WARNING) << "Connection with peer TRK " << mDestIp << " lost:" << UDT::getlasterror().getErrorMessage();
    MgrReporter::getInstance().removeRdupTrkSendingQueue( mDestIp );
    
    MgrConfigStatic::getInstance().setTrackerId( 0 );
    MgrConfigStatic::getInstance().setTrackerAddress( "" );
}

void ThreadRUdpApToTrkSender::handleSuccessfulSending( MsgBase& msg )
{
    if ( MgrReporter::getInstance().isMonRudpConnected() )
        MgrReporter::getInstance().rudpUnicastToMon( msg.clone() );
}
