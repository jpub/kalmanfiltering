#!/bin/bash
COMMS_CHANNEL="eth0"
SNIFF_CHANNEL="eth1"

#COMMS_CHANNEL="wlan0"
#SNIFF_CHANNEL="wlan1"

SRC_AID=`/sbin/ifconfig | grep -A 1 $COMMS_CHANNEL | grep inet | awk '{print $2}' | sed 's/addr://' | sed 's/\./ /g' | awk '{print $4}'`
SRC_MAC=`/sbin/ifconfig | grep $COMMS_CHANNEL | awk '{print $5}' | sed 's/://g'`
SRC_ADDRESS=`/sbin/ifconfig | grep $COMMS_CHANNEL -A 1 | grep inet | awk '{print $2}' | sed 's/inet//' | sed 's/addr://'`
BROADCAST_ADDRESS=`/sbin/ifconfig | grep $COMMS_CHANNEL -A 1 | grep Bcast | awk '{print $3}' | sed 's/Bcast://'`

if [ -z $SRC_AID ]; then
	echo "unable to set AID of sniffer"
	exit
fi

if [ -z $SRC_MAC ]; then
	echo "unable to get Mac address"
	exit
fi

if [ -z $SRC_ADDRESS ]; then
	echo "unable to get IP address"
	exit
fi

if [ -z $BROADCAST_ADDRESS ]; then
	echo "unable to get Broadcast address"
	exit
fi

echo "SRC_AID:$SRC_AID"
echo "SRC_MAC:$SRC_MAC"
echo "SRC_ADDRESS:$SRC_ADDRESS"
echo "BROADCAST_ADDRESS:$BROADCAST_ADDRESS"

sudo ./sniffer $SRC_AID $SRC_MAC $SRC_ADDRESS $BROADCAST_ADDRESS $SNIFF_CHANNEL 
