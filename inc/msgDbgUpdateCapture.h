#ifndef __MSG_DBG_UPDATE_CAPTURE_H
#define __MSG_DBG_UPDATE_CAPTURE_H

#include "msgBase.h"

class ByteBuffer;

class MsgDbgUpdateCapture : public MsgBase
{
public:
    ENABLE_EVENT_DISPATCH();

public:
    virtual QItem clone();

public:
    MsgDbgUpdateCapture();
    MsgDbgUpdateCapture( ByteBuffer* inBuffer );
    MsgDbgUpdateCapture( const MsgDbgUpdateCapture& rhs );
    MsgDbgUpdateCapture& operator=( const MsgDbgUpdateCapture& rhs );

    virtual int getSize();
    virtual ByteBuffer* pack( int& bytePosition );
    virtual void unpack( int& bytePosition );
    virtual int print();

    DBG_UPDATE_CAPTURE* data()
    {
        return &mData;
    }
    
private:
    DBG_UPDATE_CAPTURE mData;
};

#endif
