if [ -z $1 ]; then
	echo "Destination needed"
	exit
fi

scp bin_pi/sniffer pi@$1:/home/pi/bin_pi/
scp bin_pi/gateway pi@$1:/home/pi/bin_pi/
scp bin_pi/config.xml pi@$1:/home/pi/bin_pi/
