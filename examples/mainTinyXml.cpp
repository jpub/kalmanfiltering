#include "tinyxml2.h"

using namespace tinyxml2;

int main( int argc, char** argv )
{
    if ( argc != 2 )
    {
        printf( "usage: ./mainTinyXml <CHANGE_VALUE>\n" );
        return -1;
    }

    char* changeValue = argv[1];
    
    XMLDocument xmlDoc;
    int status = xmlDoc.LoadFile( "config.xml" );
    printf( "status %d\n", status );
    XMLElement *pRoot, *pParam;
    pRoot = xmlDoc.FirstChildElement( "Settings" );
    pParam = pRoot->FirstChildElement( "SRC_AID" );
    printf( "SRC_AID: %s\n", pParam->GetText() );
    pParam = pRoot->FirstChildElement( "BROADCAST_PORT" );
    printf( "BROADCAST_PORT: %s\n", pParam->GetText() );

    pRoot->FirstChildElement( "BROADCAST_PORT" )->SetText( changeValue );
    printf( "Changing To: %s\n", changeValue );
    xmlDoc.SaveFile( "config.xml" );

    return 0;
}
