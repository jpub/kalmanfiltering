#ifndef __MGR_AREA_H
#define __MGR_AREA_H

#include <string>
#include <map>
#include <vector>

#include "factoryMsg.h"
#include "symbolArea.h"
#include "__GenStructs.h"

using std::string;
using std::map;
using std::vector;

class MgrArea
{
public:
	static MgrArea& getInstance();
	void init( map< string, vector<POINTF_2D> > areaMap );
	void update( string deviceId, double longitude, double latitude );
	void sendAreaUpdate();
	void removeDevice( string deviceId );
	
private:
	MgrArea();
	~MgrArea();

private:
	static MgrArea* msInstance;

private:
	FactoryMsg mFactoryMsg;
	vector<SymbolArea> mSymbolAreaVec;
};

#endif