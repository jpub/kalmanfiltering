#ifndef __MSG_DISCOVERY_MON_H
#define __MSG_DISCOVERY_MON_H

#include "msgBase.h"

class ByteBuffer;

class MsgDiscoveryMon : public MsgBase
{
public:
    ENABLE_EVENT_DISPATCH();

public:
    virtual QItem clone();

public:
    MsgDiscoveryMon();
    MsgDiscoveryMon( ByteBuffer* inBuffer );
    MsgDiscoveryMon( const MsgDiscoveryMon& rhs );
    MsgDiscoveryMon& operator=( const MsgDiscoveryMon& rhs );

    virtual int getSize();
    virtual ByteBuffer* pack( int& bytePosition );
    virtual void unpack( int& bytePosition );
    virtual int print();

    DISCOVERY_MON* data()
    {
        return &mData;
    }
    
private:
    DISCOVERY_MON mData;
};

#endif
