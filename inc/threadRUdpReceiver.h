#ifndef __THREAD_RUDP_RECEIVER_H
#define __THREAD_RUDP_RECEIVER_H

#include "udt.h"
#include "threadBaseReceiver.h"

class ThreadRUdpReceiver : public ThreadBaseReceiver
{
public:
    ThreadRUdpReceiver( MsgQueue<QItem>* incomingQueue, UDTSOCKET clientfd );
    
    virtual void* run( void* arg );

    
private:
    UDTSOCKET mClientfd;
};

#endif
