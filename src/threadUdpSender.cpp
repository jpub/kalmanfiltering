#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>

#include "byteBuffer.h"
#include "threadUdpSender.h"
#include "msgQueue.h"
#include "logger.h"


ThreadUdpSender::ThreadUdpSender(
    MsgQueue<QItem>* outgoingQueue,
    int destinationId,
    const char* destIp,
    int destPort,
    const char* localBindIp,
    int localBindPort )
    : ThreadBaseSender(outgoingQueue, destinationId),
      mDestIp(destIp),
      mDestPort(destPort),
      mLocalBindIp(localBindIp),
      mLocalBindPort(localBindPort)
{
    LOG(DEBUG) << "Broadcasting to " << destIp << ":" << destPort;
    
    mSlen = sizeof( mDestAddr );

    memset( &mHints, 0, sizeof(struct addrinfo) );    
    mHints.ai_flags = AI_PASSIVE;
    mHints.ai_family = AF_INET;
    mHints.ai_socktype = SOCK_DGRAM;

    if ( getaddrinfo(NULL, "0", &mHints, &mOwnAddr) != 0 )
    {
        LOG(FATAL) << "unable to get local address. Error:" << errno;
    }

    mSockfd = socket( mOwnAddr->ai_family, mOwnAddr->ai_socktype, mOwnAddr->ai_protocol );
    if ( mSockfd == -1 )
    {
        LOG(FATAL) << "unable to create socket. Error:" << errno;
    }

    // set the socket to be able to broadcast
    int broadcast = 1;
    if ( setsockopt(mSockfd, SOL_SOCKET, SO_BROADCAST, &broadcast, sizeof(broadcast)) )
    {
        LOG(FATAL) << "Unable to set socket to broadcast. Error:" << errno;
    }

    int reuseAddr = 1;
    if ( setsockopt(mSockfd, SOL_SOCKET, SO_REUSEADDR, &reuseAddr, sizeof(reuseAddr)) != 0 )
    {
        LOG(FATAL) << "Unable to set socket to reuse address. Error:" << errno;
    }
    
    memset( (char*)&mDestAddr, 0, sizeof(mDestAddr) );
    mDestAddr.sin_family = AF_INET;
    mDestAddr.sin_port = htons( mDestPort );
    if ( inet_aton(mDestIp, &mDestAddr.sin_addr) == 0 )
    {
        LOG(FATAL) << "inet_aton fail";
    }
}

bool ThreadUdpSender::sendMessage( MsgBase& msg )
{
    int position = 0;
    ByteBuffer* buffer = msg.pack( position );

    int totalSend = 0;
    int totalLength = buffer->getSize();
    int bytesLeft = totalLength;
    int bytesSend;
    char* data = buffer->getDataPtr();

    while ( totalSend < totalLength )
    {
        bytesSend =
            sendto(
                mSockfd,
                data+totalSend,
                bytesLeft,
                0,
                (struct sockaddr*)&mDestAddr,
                mSlen );

        if ( bytesSend == -1 )
            break;

        totalSend += bytesSend;
        bytesLeft -= bytesSend;        
    }    
    
    return ( bytesSend == -1 ? false : true );
}
