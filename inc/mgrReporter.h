#ifndef __MGR_REPORTER_H
#define __MGR_REPORTER_H

#include <map>
#include <vector>
#include <string>
#include "threadBase.h"

/****************************************************************************************************/
/* std::map needs to be locked as it can be access by multiple threads  							*/
/* concurrently. The program crash due to the following reason:										*/
/* 1. Tracker rudp broadcast RTT request to all AP 													*/
/* 2. AP7 is somehow disconnected																	*/
/* 3. Tracker is looping through the map to put RTT request											*/
/* 4. ThreadRudpTrkToApSender attempt to send and realise AP7 is down								*/
/* 5. HandleDisconnection function will attemp to remove q from map 								*/
/* 6. There is a chance that the broadcasting function has not finish iterating through the map 	*/
/* 7. It is not a valid operation to remove an element from the map and while iterating it 			*/
/* 8. Solution: add mutext to guard all the map operation											*/
/****************************************************************************************************/
#include "mutexLocker.h"

using std::string;

class MgrReporter
{
public:
	static MgrReporter& getInstance();
	void init( MsgQueue<QItem>* udpBcQueue, MsgQueue<QItem>* mltQueue, MsgQueue<QItem>* exfQueue, MsgQueue<QItem>* internalQueue );
	void udpBroadcastToAp( QItem qitem );
	void sendToMlt( QItem qitem );
	void sendToExf( QItem qitem );
	void sendToInternal( QItem qitem );

	bool isApRudpConnected( string destIp );
	bool addRudpApSendingQueue( string destIp, MsgQueue<QItem>* rdupOutoingQueue );
	bool removeRdupApSendingQueue( string destIp );
	bool rudpBroadcastToAp( QItem qitem );
	bool rudpUnicastToAp( string destIp, QItem qitem );

	bool isTrkRudpConnected( string destIp );
	bool addRudpTrkSendingQueue( string destIp, MsgQueue<QItem>* rdupOutoingQueue );
	bool removeRdupTrkSendingQueue( string destIp );
	bool rudpUnicastToTrk( QItem qitem );

	bool isMonRudpConnected();
	bool addRudpMonSendingQueue( string destIp, MsgQueue<QItem>* rdupOutoingQueue );
	bool removeRdupMonSendingQueue( string destIp );
	bool rudpUnicastToMon( QItem qitem );
	void updateFilterList( std::vector<int> filterCodeVec );

	
private:
	MgrReporter();

// protect the map
private:
	pthread_mutex_t mMutex;

private:
	static MgrReporter* msInstance;
	MsgQueue<QItem>* mUdpBcQueue;
	MsgQueue<QItem>* mMltQueue;
	MsgQueue<QItem>* mExfQueue;
	MsgQueue<QItem>* mInternalQueue;
	std::map< string, MsgQueue<QItem>* > mRUdpBcQueue;
	std::map< string, MsgQueue<QItem>* > mRUdpTrkQueue;
	std::map< string, MsgQueue<QItem>* > mRUdpMonQueue;

	std::map< int, int > mFilterCodeMap;
};

#endif