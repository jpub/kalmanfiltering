#!/bin/bash
COMMS_CHANNEL="eth0"

#COMMS_CHANNEL="wlan0"

SRC_ADDRESS=`ifconfig | grep $COMMS_CHANNEL -A 1 | grep inet | awk '{print $2}' | sed 's/inet//' | sed 's/addr://'`
BROADCAST_ADDRESS=`ifconfig | grep $COMMS_CHANNEL -A 1 | grep Bcast | awk '{print $3}' | sed 's/Bcast://'`

if [ -z $SRC_ADDRESS ]; then
	echo "unable to get IP address"
	exit
fi

if [ -z $BROADCAST_ADDRESS ]; then
	echo "unable to get Broadcast address"
	exit
fi

echo "SRC_ADDRESS:$SRC_ADDRESS"
echo "BROADCAST_ADDRESS:$BROADCAST_ADDRESS"

./tracker 9000 $SRC_ADDRESS $BROADCAST_ADDRESS  
