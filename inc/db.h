#ifndef DB_H
#define DB_H

#include <map>
#include <string>
#include "logger.h"

using std::string;
using std::map;

template <typename T>
class Db
{
public:
	typedef map< string, T > MapT;

public:
	static Db& getInstance();

public:
	bool get( const string& index, T& out );
	vector<T> getAll();
	bool update( const string& index, const T& in );
	bool remove( const string& index );
	bool removeAll();
	bool exist( const string& index );
	int size();


private:
	Db();

private:
	static Db* msInstance;

private:	
	MapT mMap;	
    typename MapT::iterator mIter;
};

template <typename T>
Db<T>* Db<T>::msInstance = 0;

template <typename T>
Db<T>::Db()
{
}

template <typename T>
Db<T>& Db<T>::getInstance()
{
	if ( msInstance == 0 )
	{
		msInstance = new Db<T>();
	}
	return *msInstance;
}

template <typename T>
bool Db<T>::get( const string& index, T& out )
{

    mIter = mMap.find( index );
	if ( mIter == mMap.end() )
		return false;

	out = mIter->second;
	return true;
}

template <typename T>
vector<T> Db<T>::getAll()
{
	vector<T> outVec;
	for ( mIter=mMap.begin(); mIter!=mMap.end(); ++mIter )
	{
		outVec.push_back( mIter->second );
	}
	return outVec;
}

template <typename T>
bool Db<T>::update( const string& index, const T& in )
{
	mMap[index] = in;
	return true;
}

template <typename T>
bool Db<T>::remove( const string& index )
{
    mIter = mMap.find( index );
	if ( mIter == mMap.end() )
		return false;

	mMap.erase( mIter );
	return true;
}

template <typename T>
bool Db<T>::removeAll()
{
	mMap.clear();	
	return true;
}

template <typename T>
bool Db<T>::exist( const string& index )
{
	return (mMap.find(index) != mMap.end());
}

template <typename T>
int Db<T>::size()
{
	return mMap.size();
}

#endif
