#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>



int main( int argc, char** argv )
{
    int sockfd;
    struct sockaddr destAddr;
    socklen_t slen;
    char buf[512];
    
    if ( argc != 2 )
    {
        printf( "usage: ./receiver <PORT>\n" );
        return -1;
    }
    
    struct addrinfo hints, *local;
    memset(&hints, 0, sizeof(struct addrinfo));    
    hints.ai_flags = AI_PASSIVE;
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_DGRAM;
    
    if ( getaddrinfo(NULL, argv[1], &hints, &local) != 0 )
    {
        printf( "unable to get local address\n" );
        return -1;
    }

    sockfd = socket( local->ai_family, local->ai_socktype, local->ai_protocol );
    if ( sockfd == -1 )
    {
        printf( "unable to create socket\n" );
        return -1;
    }


    // if want to do broadcast
    int broadcast = 1;
    if ( setsockopt( sockfd, SOL_SOCKET, SO_BROADCAST, &broadcast, sizeof(broadcast) ) != 0 )
    {
        printf( "unable to set broadcast\n" );
        return -1;
    }
    
    if ( bind(sockfd, local->ai_addr, local->ai_addrlen) != 0 )
    {
        printf( "unable to bind\n" );
        return -1;
    }

    int count = 0;
    while ( true )
    {
        ++count;
        memset( buf, 0, 512 );
        int bytesRecv = recvfrom( sockfd, buf, 512, 0, &destAddr, &slen );
        printf( "%d -- len [%d]\n", count, bytesRecv );
    }
    return 0;
}
