#include <stdio.h>
#include <iostream>
#include <sstream>
#include "mgrConfigStatic.h"
#include "logger.h"

MgrConfigStatic* MgrConfigStatic::msInstance = 0;

using namespace std;

MgrConfigStatic::MgrConfigStatic()
{
	pthread_mutex_init( &mMutex, NULL );
	mSrcMac = "";
	mSrcAddress = "";
}

MgrConfigStatic& MgrConfigStatic::getInstance()
{
	if ( msInstance == 0 )
	{
		msInstance = new MgrConfigStatic();
	}
	return *msInstance;
}

bool MgrConfigStatic::init( const char* filename )
{
	mTrackerId = 0;
	mTrackerAddress = "";
	mMonitorId = 0;
	mMonitorAddress = "";

	if ( mXmlDoc.LoadFile(filename) )
	{
		LOG(FATAL) << "Fail to load config file:" << filename;		
		return false;
	}
	mFilename = filename;

	mRoot = mXmlDoc.FirstChildElement( "Settings" );
	mUdsSnf = getElementText( "UNIX_DSOCKET_SNF" );
	mUdsMlt = getElementText( "UNIX_DSOCKET_MLT" );
	mUdsTrk = getElementText( "UNIX_DSOCKET_TRK" );
	mUdsExf = getElementText( "UNIX_DSOCKET_EXF" );
	mUdsExfDbg = getElementText( "UNIX_DSOCKET_EXF_DBG" );
	mBcPort = atoi( getElementText("BC_PORT") );
	mApApRudpPort = atoi( getElementText("AP_AP_RUDP_PORT") );
	mTrackerPort = atoi( getElementText("TRACKER_PORT") );
	mMonitorPort = atoi( getElementText("MONITOR_PORT") );
	mTtl = atoi( getElementText("TTL") );
	mMaintenanceTimer = atoi( getElementText("MAINTENANCE_TIMER") );
	mCycleDiscovery = atoi( getElementText("CYCLE_DISCOVERY") );
	mCycleApStatus = atoi( getElementText("CYCLE_AP_STATUS") );
	mCycleAreaStatus = atoi( getElementText("CYCLE_AREA_STATUS") );
	mCycleLinkLatencyRequest = atoi( getElementText("CYCLE_LINK_LATENCY_REQUEST") );
	mCycleTrackMaintenance = atoi( getElementText("CYCLE_TRACK_MAINTENANCE") );
	mCycleSensorRegulatorMaintenance = atoi( getElementText("CYCLE_SENSOR_REGULATOR_MAINTENANCE") );
	mLinkQualityCounter = atoi( getElementText("LINK_QUALITY_COUNTER") );
	mActivateAngleTracker = atoi(getElementText("ACTIVATE_ANGLE_TRACKER")) == 0 ? false : true;
	mActivateDistanceTracker = atoi(getElementText("ACTIVATE_DISTANCE_TRACKER")) == 0 ? false : true;
	mMaxAngleAgreement = atof( getElementText("MAXIMUM_ANGLE_AGREEMENT") );
	mHumanWalkSpeed = atof( getElementText("HUMAN_WALK_SPEED") );
	mTrackAgeTime = atof( getElementText("TRACK_AGE_TIME") );
	mSensorRegulatorSec = atoi( getElementText("SENSOR_REGULATOR_SEC") );

	string filterCodeCsvStr = getElementText( "FILTER_CODE" );
	istringstream iss( filterCodeCsvStr );
	string token = "";
	while ( getline(iss, token, ',') )
	{
		if ( token.length() > 0 )
			mFilterCodeVec.push_back( atoi(token.c_str()) );
	}

	string ignoreMacCsvStr = getElementText( "IGNORE_MAC" );
	istringstream iss2( ignoreMacCsvStr );
	token = "";
	while ( getline(iss2, token, ',') )
	{
		if ( token.length() > 0 )
			mIgnoreMap[token] = token;
	}

	int areaCount = atoi( getElementText("AREA_COUNT") );
	for ( int i=1; i<=areaCount; ++i )
	{
		char tmpStr[80] = {0};
		sprintf( tmpStr, "AREA_%d", i );
		string areaCsvStr = getElementText( tmpStr );

		istringstream iss( areaCsvStr );
		string token = "";
		vector<string> csvStrVec;
		while ( getline(iss, token, ',') )
		{
			if ( token.length() > 0 )
				csvStrVec.push_back( token );
		}

		vector<POINTF_2D> pointVec;
		string areaName = "";
		for ( unsigned int j=0; j<csvStrVec.size(); ++j )
		{
			if ( j==0 )
			{
				areaName = csvStrVec[j];
				mAreaMap[areaName] = pointVec;
				continue;
			}

			POINTF_2D rectPoint;
			rectPoint.x = atoi( csvStrVec[j].c_str() );
			j += 1;
			rectPoint.y = atoi( csvStrVec[j].c_str() );
			mAreaMap[areaName].push_back( rectPoint );
		}
	}

	LOG(INFO) << "UNIX_DSOCKET_SNF:" << mUdsSnf;
	LOG(INFO) << "UNIX_DSOCKET_MLT:" << mUdsMlt;
	LOG(INFO) << "UNIX_DSOCKET_TRK:" << mUdsTrk;
	LOG(INFO) << "UNIX_DSOCKET_EXF:" << mUdsExf;
	LOG(INFO) << "UNIX_DSOCKET_EXF_DBG:" << mUdsExfDbg;
	LOG(INFO) << "BC_PORT:" << mBcPort;
	LOG(INFO) << "AP_AP_RUDP_PORT:" << mApApRudpPort;
	LOG(INFO) << "TRACKER_PORT:" << mTrackerPort;
	LOG(INFO) << "MONITOR_PORT:" << mMonitorPort;
	LOG(INFO) << "TTL:" << mTtl;
	LOG(INFO) << "MAINTENANCE_TIMER:" << mMaintenanceTimer;
	LOG(INFO) << "CYCLE_DISCOVERY:" << mCycleDiscovery;
	LOG(INFO) << "CYCLE_AP_STATUS:" << mCycleApStatus;
	LOG(INFO) << "CYCLE_AREA_STATUS:" << mCycleAreaStatus;
	LOG(INFO) << "CYCLE_LINK_LATENCY_REQUEST:" << mCycleLinkLatencyRequest;
	LOG(INFO) << "CYCLE_TRACK_MAINTENANCE:" << mCycleTrackMaintenance;
	LOG(INFO) << "CYCLE_SENSOR_REGULATOR_MAINTENANCE:" << mCycleSensorRegulatorMaintenance;
	LOG(INFO) << "LINK_QUALITY_COUNTER:" << mLinkQualityCounter;
	LOG(INFO) << "ACTIVATE_ANGLE_TRACKER:" << mActivateAngleTracker;
	LOG(INFO) << "ACTIVATE_DISTANCE_TRACKER:" << mActivateDistanceTracker;
	LOG(INFO) << "MAXIMUM_ANGLE_AGREEMENT:" << mMaxAngleAgreement;
	LOG(INFO) << "HUMAN_WALK_SPEED:" << mHumanWalkSpeed;
	LOG(INFO) << "TRACK_AGE_TIME:" << mTrackAgeTime;
	LOG(INFO) << "SENSOR_REGULATOR_SEC:" << mSensorRegulatorSec;

	for ( unsigned int i=0; i<mFilterCodeVec.size(); ++i )
	{
		LOG(INFO) << "FILTER_CODE:" << mFilterCodeVec[i];
	}

	map<string,string>::iterator itIgnoreMap;
	int counter =0;
	for ( itIgnoreMap=mIgnoreMap.begin(); itIgnoreMap!=mIgnoreMap.end(); ++itIgnoreMap )
	{
		++counter;
		LOG(INFO) << "IGNORE_MAC_" << counter << ":" << itIgnoreMap->first;
	}

	LOG(INFO) << "AREA_COUNT:" << areaCount;
	map< string, vector<POINTF_2D> >::iterator it;
	for ( it=mAreaMap.begin(); it!=mAreaMap.end(); ++it )
	{
		LOG(INFO) << "AREA_NAME:" << it->first;
		vector<POINTF_2D> pointVec = it->second;
		for ( unsigned int i=0; i<pointVec.size(); ++i )
			LOG(INFO) << "VERTEX:" << pointVec[i].x << ", " << pointVec[i].y;
	}

	return true;
}

const char* MgrConfigStatic::getElementText( const char* elementName )
{
	mElement = mRoot->FirstChildElement( elementName );
	return mElement->GetText();
}

int MgrConfigStatic::getSrcAid()
{
	return mSrcAid;
}

string MgrConfigStatic::getSrcMac()
{
	return mSrcMac;
}

string MgrConfigStatic::getSrcAddress()
{
	return mSrcAddress;
}

string MgrConfigStatic::getBcAddress()
{
	return mBcAddress;
}

int MgrConfigStatic::getTrackerId()
{
	MutexLocker mutexLock( &mMutex );
	return mTrackerId;
}

string MgrConfigStatic::getTrackerAddress()
{
	MutexLocker mutexLock( &mMutex );
	return mTrackerAddress;
}

int MgrConfigStatic::getMonitorId()
{
	MutexLocker mutexLock( &mMutex );
	return mMonitorId;
}

string MgrConfigStatic::getMonitorAddress()
{
	MutexLocker mutexLock( &mMutex );
	return mMonitorAddress;
}

string MgrConfigStatic::getUdsSnf()
{
	return mUdsSnf;
}

string MgrConfigStatic::getUdsMlt()
{
	return mUdsMlt;
}

string MgrConfigStatic::getUdsTrk()
{
	return mUdsTrk;
}

string MgrConfigStatic::getUdsExf()
{
	return mUdsExf;
}

string MgrConfigStatic::getUdsExfDbg()
{
	return mUdsExfDbg;
}

int MgrConfigStatic::getBcPort()
{
	return mBcPort;
}

int MgrConfigStatic::getApApRudpPort()
{
	return mApApRudpPort;
}

int MgrConfigStatic::getTrackerPort()
{
	return mTrackerPort;
}

int MgrConfigStatic::getMonitorPort()
{
	return mMonitorPort;
}

int MgrConfigStatic::getTtl()
{
	return mTtl;
}

int MgrConfigStatic::getMaintenanceTimer()
{
	return mMaintenanceTimer;
}

int MgrConfigStatic::getCycleDiscovery()
{
	return mCycleDiscovery;
}

int MgrConfigStatic::getCycleApStatus()
{
	return mCycleApStatus;
}

int MgrConfigStatic::getCycleAreaStatus()
{
	return mCycleAreaStatus;
}

int MgrConfigStatic::getCycleLinkLatencyRequest()
{
	return mCycleLinkLatencyRequest;
}

int MgrConfigStatic::getCycleTrackMaintenance()
{
	return mCycleTrackMaintenance;
}

int MgrConfigStatic::getCycleSensorRegulatorMaintenance()
{
	return mCycleSensorRegulatorMaintenance;
}

int MgrConfigStatic::getLinkQualityCounter()
{
	return mLinkQualityCounter;
}

bool MgrConfigStatic::getActivateAngleTracker()
{
	return mActivateAngleTracker;
}

bool MgrConfigStatic::getActivateDistanceTracker()
{
	return mActivateDistanceTracker;
}

double MgrConfigStatic::getMaximumAngleAgreement()
{
	return mMaxAngleAgreement;
}

double MgrConfigStatic::getHumanWalkSpeed()
{
	return mHumanWalkSpeed;
}

int MgrConfigStatic::getTrackAgeTime()
{
	return mTrackAgeTime;
}

int MgrConfigStatic::getSensorRegulatorSec()
{
	return mSensorRegulatorSec;
}

vector<int> MgrConfigStatic::getFilterCodeVec()
{
	return mFilterCodeVec;
}

map<string,string> MgrConfigStatic::getIgnoreMap()
{
	return mIgnoreMap;
}

map< string, vector<POINTF_2D> > MgrConfigStatic::getAreaMap()
{
	return mAreaMap;
}

void MgrConfigStatic::setSrcMac( string srcMac )
{
	mSrcMac = srcMac;
}

void MgrConfigStatic::setSrcAid( int srcAid )
{
	mSrcAid = srcAid;
}

void MgrConfigStatic::setSrcAddress( string srcAddress )
{
	mSrcAddress = srcAddress;
}

void MgrConfigStatic::setBcAddress( string bcAddress )
{
	mBcAddress = bcAddress;
}

void MgrConfigStatic::setTrackerId( int trackerId )
{
	MutexLocker mutexLock( &mMutex );
	mTrackerId = trackerId;
}

void MgrConfigStatic::setTrackerAddress( string trackerAddress )
{
	MutexLocker mutexLock( &mMutex );
	mTrackerAddress = trackerAddress;
}

void MgrConfigStatic::setMonitorId( int monitorId )
{
	MutexLocker mutexLock( &mMutex );
	mMonitorId = monitorId;
}

void MgrConfigStatic::setMonitorAddress( string monitorAddress )
{
	MutexLocker mutexLock( &mMutex );
	mMonitorAddress = monitorAddress;
}
