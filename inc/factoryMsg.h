#ifndef __FACTORY_MSG_H
#define __FACTORY_MSG_H

#include "threadBase.h"

class ByteBuffer;

class FactoryMsg
{
public:
    FactoryMsg();
    QItem get( int messageId, ByteBuffer* bb = 0 );
    QItem mDummy;
    
};

#endif
