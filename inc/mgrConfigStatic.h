#ifndef __MGR_CONFIG_STATIC_H
#define __MGR_CONFIG_STATIC_H

#include <string>
#include <vector>
#include <map>
#include "tinyxml2.h"
#include "__GenStructs.h"

/********************************************************************************************/
/* lock the Tracker and Monitor Addresses and ID 											*/
/* Those variables can be accessed from multiple threads during discovery and disconnection */
/********************************************************************************************/
#include "mutexLocker.h"


using std::string;
using std::vector;
using std::map;

class MgrConfigStatic
{
public:
	static MgrConfigStatic& getInstance();
	bool init( const char* filename );
	
	int getSrcAid();
	string getSrcMac();
	string getSrcAddress();
	string getBcAddress();
	int getTrackerId();
	string getTrackerAddress();
	int getMonitorId();
	string getMonitorAddress();
	string getUdsSnf();
	string getUdsMlt();
	string getUdsTrk();
	string getUdsExf();
	string getUdsExfDbg();
	int getBcPort();
	int getApApRudpPort();
	int getTrackerPort();
	int getMonitorPort();
	int getTtl();
	int getMaintenanceTimer();
	int getCycleDiscovery();
	int getCycleApStatus();
	int getCycleAreaStatus();
	int getCycleLinkLatencyRequest();
	int getCycleTrackMaintenance();
	int getCycleSensorRegulatorMaintenance();
	int getLinkQualityCounter();
	bool getActivateAngleTracker();
	bool getActivateDistanceTracker();
	double getMaximumAngleAgreement();
	double getHumanWalkSpeed();
	int getTrackAgeTime();
	int getSensorRegulatorSec();
	vector<int> getFilterCodeVec();
	map<string,string> getIgnoreMap();
	map< string, vector<POINTF_2D> > getAreaMap();

public: // parameters from command line
	void setSrcMac( string srcMac );
	void setSrcAid( int srcAid );
	void setSrcAddress( string srcAddress );
	void setBcAddress( string bcAddress );

public: // parameters from discovery msg
	void setTrackerId( int trackerId );
	void setTrackerAddress( string trackerIp );
	void setMonitorId( int monitorId );
	void setMonitorAddress( string monitorIp );


private:
	MgrConfigStatic();
	const char* getElementText( const char* elementName );

// protect the Tracker and Monitor address and id
private:
	pthread_mutex_t mMutex;

private:
	static MgrConfigStatic* msInstance;
	string mFilename;
	tinyxml2::XMLDocument mXmlDoc;
	tinyxml2::XMLElement* mRoot;
	tinyxml2::XMLElement* mElement;

private:
	int mSrcAid;
	string mSrcMac;
	string mSrcAddress;
	string mBcAddress;
	int mTrackerId;
	string mTrackerAddress;
	int mMonitorId;
	string mMonitorAddress;
	string mUdsSnf;
	string mUdsMlt;
	string mUdsTrk;
	string mUdsExf;
	string mUdsExfDbg;
	int mBcPort;
	int mApApRudpPort;
	int mTrackerPort;
	int mMonitorPort;
	int mTtl;
	int mMaintenanceTimer;
	int mCycleDiscovery;
	int mCycleApStatus;
	int mCycleAreaStatus;
	int mCycleLinkLatencyRequest;
	int mCycleTrackMaintenance;
	int mCycleSensorRegulatorMaintenance;
	int mLinkQualityCounter;
	bool mActivateAngleTracker;
	bool mActivateDistanceTracker;
	double mMaxAngleAgreement;
	double mHumanWalkSpeed;
	int mTrackAgeTime;
	int mSensorRegulatorSec;
	vector<int> mFilterCodeVec;
	map<string,string> mIgnoreMap;
	map< string, vector<POINTF_2D> > mAreaMap;
};

#endif