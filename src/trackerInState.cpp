#include "trackerInState.h"

string TrackerInState::getDeviceId()
{
	return mDeviceId;
}

double TrackerInState::getCurrentTrackerLongitude()
{
	return mLongitude;
}

double TrackerInState::getCurrentTrackerLatitude()
{
	return mLatitude;
}

double TrackerInState::getDetectedLongitude()
{
	return mDetectedLongitude;
}

double TrackerInState::getDetectedLatitude()
{
	return mDetectedLatitude;
}

unsigned int TrackerInState::getLastUpdateTime()
{
	return mLastUpdateTime;
}

unsigned int TrackerInState::getSecondsSinceTrackerLastUpdate()
{
	return mSecondsSinceTrackerLastUpdate;
}


void TrackerInState::setDeviceId( string deviceId )
{
	mDeviceId = deviceId;
}

void TrackerInState::setCurrentTrackerLongitude( double longitude )
{
	mLongitude = longitude;
}

void TrackerInState::setCurrentTrackerLatitude( double latitude )
{
	mLatitude = latitude;
}

void TrackerInState::setDetectedLongitude( double detectedLongitude )
{
	mDetectedLongitude = detectedLongitude;
}

void TrackerInState::setDetectedLatitude( double detectedLatitude )
{
	mDetectedLatitude = detectedLatitude;
}

void TrackerInState::setLastUpdateTime( unsigned int lastUpdateTime )
{
	mLastUpdateTime = lastUpdateTime;
}

void TrackerInState::setSecondsSinceTrackerLastUpdate( unsigned int secondsSinceTrackerLastUpdate )
{
	mSecondsSinceTrackerLastUpdate = secondsSinceTrackerLastUpdate;
}
