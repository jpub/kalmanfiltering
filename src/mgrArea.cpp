#include "mgrArea.h"
#include "mgrReporter.h"
#include "msgSimArea.h"
#include "logger.h"


MgrArea* MgrArea::msInstance = 0;


MgrArea::MgrArea()
{
}

MgrArea::~MgrArea()
{
}

MgrArea& MgrArea::getInstance()
{
	if ( msInstance == 0 )
	{
		msInstance = new MgrArea();
	}
	return *msInstance;
}

void MgrArea::init( map< string, vector<POINTF_2D> > areaMap )
{
    map< string, vector<POINTF_2D> >::iterator it;
    for ( it=areaMap.begin(); it!=areaMap.end(); ++it )
    {
        SymbolArea symbolArea( it->first, it->second );
        mSymbolAreaVec.push_back( symbolArea );
    }
}

void MgrArea::update( string deviceId, double longitude, double latitude )
{
    for ( unsigned int i=0; i<mSymbolAreaVec.size(); ++i )
    {
        mSymbolAreaVec[i].update( deviceId, longitude, latitude );
    }
}

void MgrArea::sendAreaUpdate()
{
    for ( unsigned int i=0; i<mSymbolAreaVec.size(); ++i )
    {
        SymbolArea symbolArea = mSymbolAreaVec[i];
        vector<POINTF_2D> pointVec = symbolArea.getPointVec();

        QItem qitem = mFactoryMsg.get( ID_SIM_AREA );
        MsgSimArea* msg = (MsgSimArea*)(qitem.getDataPointer());

        memcpy( msg->data()->areaName, symbolArea.getName().c_str(), symbolArea.getName().length() );
        msg->data()->count = symbolArea.getCount();
        msg->data()->x1 = pointVec[0].x;
        msg->data()->y1 = pointVec[0].y;
        msg->data()->x2 = pointVec[1].x;
        msg->data()->y2 = pointVec[1].y;
        msg->data()->x3 = pointVec[2].x;
        msg->data()->y3 = pointVec[2].y;
        msg->data()->x4 = pointVec[3].x;
        msg->data()->y4 = pointVec[3].y;

        // send to EXF
        MgrReporter::getInstance().sendToExf( qitem );
    }
}

void MgrArea::removeDevice( string deviceId )
{
    for ( unsigned int i=0; i<mSymbolAreaVec.size(); ++i )
    {
        mSymbolAreaVec[i].removeDevice( deviceId );
    }
}
