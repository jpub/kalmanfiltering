#include "byteBuffer.h"
#include "msgBase.h"
#include "msgLinkRtt.h"
#include "logger.h"
#include <stdio.h>


QItem MsgLinkRtt::clone()
{
    MsgLinkRtt* msg = new MsgLinkRtt();
    if ( this->mRawBuffer )
        msg->mRawBuffer = new ByteBuffer( *this->mRawBuffer );
    msg->mHeader = this->mHeader;
    msg->mData = this->mData;

    return QItem( msg );
}

MsgLinkRtt::MsgLinkRtt()
{
    memset( &mData, 0, sizeof(mData) );
    mHeader.messageCode = ID_LINK_RTT;
    mHeader.messageLength = getSize();
}

MsgLinkRtt::MsgLinkRtt( ByteBuffer* inBuffer )
    :MsgBase( inBuffer )
{
    int bytePosition = 0;
    unpack( bytePosition );
}

MsgLinkRtt::MsgLinkRtt( const MsgLinkRtt& rhs )
    :MsgBase( rhs )
{
    mData = rhs.mData;
}

MsgLinkRtt& MsgLinkRtt::operator=( const MsgLinkRtt& rhs )
{
    MsgBase::operator = (rhs);
    mData = rhs.mData;
    return *this;
}

int MsgLinkRtt::getSize()
{
    int totalSize = 0;
    totalSize += MsgBase::getSize();
    totalSize += sizeof( mData );
    return totalSize;
}

ByteBuffer* MsgLinkRtt::pack( int& bytePosition )
{
    MsgBase::pack( bytePosition );
    mRawBuffer->appendByteArray( (char*)&mData, sizeof(mData) );
    return mRawBuffer;
}

void MsgLinkRtt::unpack( int& bytePosition )
{
    MsgBase::unpack( bytePosition );
    mRawBuffer->popByteArray( (char*)&mData, sizeof(mData) );
}

int MsgLinkRtt::print()
{
    int charWritten = MsgBase::print();

    charWritten = snprintf( mPrintBuffer + charWritten, PRINT_BUFFER_SIZE,
        "ClientId:%d MilliRtt:%d Key:%d",
        mData.clientId,
        mData.milliRtt,
        mData.key );

    LOG(DEBUG) << mPrintBuffer;
    return charWritten;
}