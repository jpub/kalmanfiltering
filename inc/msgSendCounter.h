#ifndef __MSG_SEND_COUNTER_H
#define __MSG_SEND_COUNTER_H

#include "msgBase.h"

class ByteBuffer;

class MsgSendCounter : public MsgBase
{
public:
    ENABLE_EVENT_DISPATCH();

public:
    virtual QItem clone();

public:
    MsgSendCounter();
    MsgSendCounter( ByteBuffer* inBuffer );
    MsgSendCounter( const MsgSendCounter& rhs );
    MsgSendCounter& operator=( const MsgSendCounter& rhs );

    virtual int getSize();
    virtual ByteBuffer* pack( int& bytePosition );
    virtual void unpack( int& bytePosition );
    virtual int print();

    SEND_COUNTER* data()
    {
        return &mData;
    }
    
private:
    SEND_COUNTER mData;
};

#endif
