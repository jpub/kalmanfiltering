#ifndef __THREAD_UDP_RECEIVER_H
#define __THREAD_UDP_RECEIVER_H

#include <arpa/inet.h>
#include <netdb.h>
#include "threadBaseReceiver.h"

class ThreadUdpReceiver : public ThreadBaseReceiver
{
public:
    ThreadUdpReceiver( MsgQueue<QItem>* incomingQueue,
                       int incomingPort,
                       const char* localBindIp );
    
    virtual void* run( void* arg );

private:
    int mIncomingPort;
    const char* mLocalBindIp;

    int mSockfd;
    struct addrinfo mHints;
    struct addrinfo* mOwnAddr;
    struct sockaddr mDestAddr;
    socklen_t mSlen;
};

#endif
