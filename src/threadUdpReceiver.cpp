#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>

#include "threadUdpReceiver.h"
#include "msgQueue.h"
#include "logger.h"



ThreadUdpReceiver::ThreadUdpReceiver(
    MsgQueue<QItem>* incomingQueue,
    int incomingPort,
    const char* localBindIp )
    : ThreadBaseReceiver(incomingQueue),
      mIncomingPort(incomingPort),
      mLocalBindIp(localBindIp)
{
    memset( &mHints, 0, sizeof(struct addrinfo) );
    mHints.ai_flags = AI_PASSIVE;
    mHints.ai_family = AF_INET;
    mHints.ai_socktype = SOCK_DGRAM;

    char incomingPortStr[PORT_STRING_LENGTH] = {0};
    sprintf( incomingPortStr, "%d", mIncomingPort );
    if ( getaddrinfo(NULL, incomingPortStr, &mHints, &mOwnAddr) != 0 )
    {
        LOG(FATAL) << "Unable to get own address. Error:" << errno;
    }

    mSockfd = socket( mOwnAddr->ai_family, mOwnAddr->ai_socktype, mOwnAddr->ai_protocol );
    if ( mSockfd == -1 )
    {
        LOG(FATAL) << "Create socket fail. Error:" << errno;
    }

    // set the socket to be able to broadcast
    int broadcast = 1;
    if ( setsockopt(mSockfd, SOL_SOCKET, SO_BROADCAST, &broadcast, sizeof(broadcast)) != 0 )
    {
        LOG(FATAL) << "Unable to set socket to broadcast. Error:" << errno;
    }

    int reuseAddr = 1;
    if ( setsockopt(mSockfd, SOL_SOCKET, SO_REUSEADDR, &reuseAddr, sizeof(reuseAddr)) != 0 )
    {
        LOG(FATAL) << "Unable to set socket to reuse address. Error:" << errno;
    }

    if ( bind(mSockfd, mOwnAddr->ai_addr, mOwnAddr->ai_addrlen) != 0 )
    {
        LOG(FATAL) << "Unable to bind receiving socket. Error:" << errno;
    }
}

void* ThreadUdpReceiver::run( void* arg )
{
    char socketData[RECV_BUFFER_SIZE];
    int bytesRecv = 0;
    char* nullArg = 0;
    
    while ( true )
    {
        // must always fill in the size! if not it will cause random crashes
        mSlen = sizeof( mDestAddr );

        bytesRecv =
            recvfrom(
                mSockfd,
                socketData,
                RECV_BUFFER_SIZE,
                0,
                &mDestAddr,
                &mSlen );

        if ( bytesRecv == -1 )
        {
            LOG(FATAL) << "Receive fail!!! Error:" << errno;
            return nullArg;
        }

        if ( bytesRecv == 0 )
        {
            LOG(WARNING) << "Receive empty data..continue";
            continue;
        }

        if ( !mRingBuffer.write(socketData, bytesRecv) )
        {
            LOG(WARNING) << "Message not written to buffer";
            continue;
        }

        // process the data in the ring buffer
        // If this is too time consuming..may have to shift to another thread
        processRingBuffer();
    }
}
