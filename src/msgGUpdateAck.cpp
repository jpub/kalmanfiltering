#include "byteBuffer.h"
#include "msgBase.h"
#include "msgGUpdateAck.h"
#include <stdio.h>


QItem MsgGUpdateAck::clone()
{
    MsgGUpdateAck* msg = new MsgGUpdateAck();
    if ( this->mRawBuffer )
        msg->mRawBuffer = new ByteBuffer( *this->mRawBuffer );
    msg->mHeader = this->mHeader;
    msg->mData = this->mData;

    return QItem( msg );
}

MsgGUpdateAck::MsgGUpdateAck()
{
    memset( &mData, 0, sizeof(mData) );
    mHeader.messageCode = ID_G_UPDATE_ACK;
    mHeader.messageLength = getSize();
}

MsgGUpdateAck::MsgGUpdateAck( ByteBuffer* inBuffer )
    :MsgBase( inBuffer )
{
    int bytePosition = 0;
    unpack( bytePosition );
}

MsgGUpdateAck::MsgGUpdateAck( const MsgGUpdateAck& rhs )
    :MsgBase( rhs )
{
    mData = rhs.mData;
}

MsgGUpdateAck& MsgGUpdateAck::operator=( const MsgGUpdateAck& rhs )
{
    MsgBase::operator = (rhs);
    mData = rhs.mData;
    return *this;
}

int MsgGUpdateAck::getSize()
{
    int totalSize = 0;
    totalSize += MsgBase::getSize();
    totalSize += mData.numberOfBytes;

    return totalSize;
}

ByteBuffer* MsgGUpdateAck::pack( int& bytePosition )
{
    MsgBase::pack( bytePosition );
    int variableSize = getSize() - sizeof(MSG_HEADER);
    mRawBuffer->appendByteArray( (char*)mData.payload, variableSize );
    return mRawBuffer;
}

void MsgGUpdateAck::unpack( int& bytePosition )
{
    MsgBase::unpack( bytePosition );
    int variableSize = mRawBuffer->getSize() - sizeof(MSG_HEADER);
    mData.numberOfBytes = variableSize;
    mRawBuffer->popByteArray( (char*)mData.payload, variableSize );
}

int MsgGUpdateAck::print()
{
    int charWritten = MsgBase::print();

    charWritten = snprintf( mPrintBuffer + charWritten, PRINT_BUFFER_SIZE,
        "PIPE_THROUGH_BYTES:%d",
        mData.numberOfBytes );

    LOG(DEBUG) << mPrintBuffer;
    return charWritten;
}
