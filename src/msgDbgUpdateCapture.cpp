#include "byteBuffer.h"
#include "msgBase.h"
#include "msgDbgUpdateCapture.h"
#include <stdio.h>


QItem MsgDbgUpdateCapture::clone()
{
    MsgDbgUpdateCapture* msg = new MsgDbgUpdateCapture();
    if ( this->mRawBuffer )
        msg->mRawBuffer = new ByteBuffer( *this->mRawBuffer );
    msg->mHeader = this->mHeader;
    msg->mData = this->mData;

    return QItem( msg );
}

MsgDbgUpdateCapture::MsgDbgUpdateCapture()
{
    memset( &mData, 0, sizeof(mData) );
    mHeader.messageCode = ID_DBG_UPDATE_CAPTURE;
    mHeader.messageLength = getSize();
}

MsgDbgUpdateCapture::MsgDbgUpdateCapture( ByteBuffer* inBuffer )
    :MsgBase( inBuffer )
{
    int bytePosition = 0;
    unpack( bytePosition );
}

MsgDbgUpdateCapture::MsgDbgUpdateCapture( const MsgDbgUpdateCapture& rhs )
    :MsgBase( rhs )
{
    mData = rhs.mData;
}

MsgDbgUpdateCapture& MsgDbgUpdateCapture::operator=( const MsgDbgUpdateCapture& rhs )
{
    MsgBase::operator = (rhs);
    mData = rhs.mData;
    return *this;
}

int MsgDbgUpdateCapture::getSize()
{
    int totalSize = 0;
    totalSize += MsgBase::getSize();

    int numberOfUnusedCodeInfo = __ARRAY_50_LENGTH - mData.numberOfCode;
    totalSize += ( sizeof(mData) - numberOfUnusedCodeInfo * sizeof(CODE_INFO) );

    return totalSize;
}

ByteBuffer* MsgDbgUpdateCapture::pack( int& bytePosition )
{
    MsgBase::pack( bytePosition );
    int variableSize = getSize() - sizeof(MSG_HEADER);
    mRawBuffer->appendByteArray( (char*)&mData, variableSize );
    return mRawBuffer;
}

void MsgDbgUpdateCapture::unpack( int& bytePosition )
{
    MsgBase::unpack( bytePosition );
    int variableSize = mRawBuffer->getSize() - sizeof(MSG_HEADER);
    mRawBuffer->popByteArray( (char*)&mData, variableSize );
}

int MsgDbgUpdateCapture::print()
{
    int charWritten = MsgBase::print();

    charWritten = snprintf( mPrintBuffer + charWritten, PRINT_BUFFER_SIZE,
        "Mode:%d NumOfCode:%d",
        mData.mode,
        mData.numberOfCode );

    LOG(DEBUG) << mPrintBuffer;
    return charWritten;
}
