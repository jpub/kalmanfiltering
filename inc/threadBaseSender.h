#ifndef __THREAD_BASE_SENDER_H
#define __THREAD_BASE_SENDER_H

#include "threadBase.h"

class ThreadBaseSender : public ThreadBase
{
public:
    ThreadBaseSender( MsgQueue<QItem>* outgoingQueue, int destinationId );
    virtual void* run( void* arg );

protected:
    virtual bool sendMessage( MsgBase& ) = 0;
    virtual void handleSuccessfulSending( MsgBase& ) {}

protected:
	unsigned int mMessageNumber;
	int mSourceId;
	int mDestinationId;
};
    
#endif
