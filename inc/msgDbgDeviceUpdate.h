#ifndef __MSG_DBG_DEVICE_UPDATE_H
#define __MSG_DBG_DEVICE_UPDATE_H

#include "msgBase.h"

class ByteBuffer;

class MsgDbgDeviceUpdate : public MsgBase
{
public:
    ENABLE_EVENT_DISPATCH();

public:
    virtual QItem clone();

public:
    MsgDbgDeviceUpdate();
    MsgDbgDeviceUpdate( ByteBuffer* inBuffer );
    MsgDbgDeviceUpdate( const MsgDbgDeviceUpdate& rhs );
    MsgDbgDeviceUpdate& operator=( const MsgDbgDeviceUpdate& rhs );

    virtual int getSize();
    virtual ByteBuffer* pack( int& bytePosition );
    virtual void unpack( int& bytePosition );
    virtual int print();

    DBG_DEVICE_UPDATE* data()
    {
        return &mData;
    }
    
private:
    DBG_DEVICE_UPDATE mData;
};

#endif
