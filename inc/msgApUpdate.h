#ifndef __MSG_AP_UPDATE_H
#define __MSG_AP_UPDATE_H

#include "msgBase.h"

class ByteBuffer;

class MsgApUpdate : public MsgBase
{
public:
    ENABLE_EVENT_DISPATCH();

public:
    virtual QItem clone();

public:
    MsgApUpdate();
    MsgApUpdate( ByteBuffer* inBuffer );
    MsgApUpdate( const MsgApUpdate& rhs );
    MsgApUpdate& operator=( const MsgApUpdate& rhs );

    virtual int getSize();
    virtual ByteBuffer* pack( int& bytePosition );
    virtual void unpack( int& bytePosition );
    virtual int print();

    AP_UPDATE* data()
    {
        return &mData;
    }
    
private:
    AP_UPDATE mData;
};

#endif
